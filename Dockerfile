FROM node:16 as build
ADD . /app
WORKDIR /app
RUN yarn install --immutable --immutable-cache --check-cache
ARG VITE_CRISP_ID
ENV VITE_CRISP_ID=$VITE_CRISP_ID
RUN yarn build

FROM nginx:1.21-alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/dist /usr/share/nginx/html
