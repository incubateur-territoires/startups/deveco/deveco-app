module Pages.Parametres.Zonages exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h1, h2, hr, label, p, span, text)
import Api
import Api.Auth
import DSFR.Button
import DSFR.Grid
import DSFR.Icons.System exposing (deleteFill)
import DSFR.Input
import DSFR.Modal
import DSFR.Typography as Typo
import Data.Zonage exposing (Zonage, decodeZonage)
import Effect
import File
import File.Select
import Html.Attributes exposing (class, title)
import Html.Extra exposing (viewIf, viewMaybe)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import Task
import UI.Layout
import View exposing (View)


type alias Model =
    { zonages : WebData (List Zonage)
    , nouveauZonage : Maybe NouveauZonage
    , deleteRequest : Maybe DeleteRequest
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | ReceivedZonages (WebData (List Zonage))
    | ClickedAddZonage
    | CanceledAddZonage
    | UpdatedNomZonage String
    | ClickedUploadFichierZonage
    | UploadedFichierZonage File.File
    | ReadFichierZonage String
    | ClickedRemovedZonageFile
    | ConfirmedAddZonage
    | ReceivedAddZonage (WebData (List Zonage))
    | ClickedDelete Zonage
    | CanceledDelete
    | ConfirmedDelete
    | ReceivedDeleteZonage (WebData (List Zonage))


type alias DeleteRequest =
    { request : WebData (List Zonage)
    , zonage : Zonage
    }


type alias NouveauZonage =
    { nom : String
    , fichier : Maybe String
    , contenu : Maybe String
    , request : WebData (List Zonage)
    }


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ user =
    Spa.Page.onNewFlags (\_ -> NoOp) <|
        Spa.Page.element
            { view = view
            , init = init
            , update = update user
            , subscriptions = \_ -> Sub.none
            }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { zonages = RD.Loading
      , nouveauZonage = Nothing
      , deleteRequest = Nothing
      }
    , getZonages
    )
        |> Shared.pageChangeEffects


getZonages : Effect.Effect Shared.Msg Msg
getZonages =
    Effect.fromCmd <|
        Api.Auth.get
            { url = Api.getZonages
            }
            SharedMsg
            ReceivedZonages
            (Decode.list decodeZonage)


addZonage : NouveauZonage -> Effect.Effect Shared.Msg Msg
addZonage nouveauZonage =
    case nouveauZonage.contenu of
        Nothing ->
            Effect.none

        Just contenu ->
            Effect.fromCmd <|
                Api.Auth.post
                    { url = Api.getZonages
                    , body =
                        [ ( "nom", Encode.string nouveauZonage.nom )
                        , ( "contenu", Encode.string contenu )
                        ]
                            |> Encode.object
                            |> Http.jsonBody
                    }
                    SharedMsg
                    ReceivedAddZonage
                    (Decode.list decodeZonage)


update : User -> Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update _ msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedZonages zonages ->
            { model | zonages = zonages }
                |> Effect.withNone

        ClickedAddZonage ->
            { model
                | nouveauZonage =
                    Just
                        { nom = ""
                        , fichier = Nothing
                        , contenu = Nothing
                        , request = RD.NotAsked
                        }
            }
                |> Effect.withNone

        CanceledAddZonage ->
            { model | nouveauZonage = Nothing }
                |> Effect.withNone

        UpdatedNomZonage nom ->
            { model
                | nouveauZonage =
                    model.nouveauZonage
                        |> Maybe.map
                            (\z ->
                                { z
                                    | nom = nom
                                }
                            )
            }
                |> Effect.withNone

        ClickedUploadFichierZonage ->
            model
                |> Effect.withCmd (File.Select.file [] UploadedFichierZonage)

        UploadedFichierZonage fichier ->
            { model
                | nouveauZonage =
                    model.nouveauZonage
                        |> Maybe.map
                            (\z ->
                                { z
                                    | fichier = Just <| File.name fichier
                                }
                            )
            }
                |> Effect.withCmd (Task.perform ReadFichierZonage <| File.toString fichier)

        ReadFichierZonage contenu ->
            { model
                | nouveauZonage =
                    model.nouveauZonage
                        |> Maybe.map
                            (\z ->
                                { z
                                    | contenu = Just contenu
                                }
                            )
            }
                |> Effect.withNone

        ClickedRemovedZonageFile ->
            { model
                | nouveauZonage =
                    model.nouveauZonage
                        |> Maybe.map
                            (\z ->
                                { z
                                    | fichier = Nothing
                                    , contenu = Nothing
                                }
                            )
            }
                |> Effect.withNone

        ConfirmedAddZonage ->
            case model.nouveauZonage of
                Nothing ->
                    model
                        |> Effect.withNone

                Just nouveauZonage ->
                    ( { model
                        | nouveauZonage =
                            Just
                                { nouveauZonage
                                    | request = RD.Loading
                                }
                      }
                    , addZonage nouveauZonage
                    )

        ReceivedAddZonage response ->
            case model.nouveauZonage of
                Nothing ->
                    model
                        |> Effect.withNone

                Just nouveauZonage ->
                    { model
                        | nouveauZonage =
                            Just
                                { nouveauZonage
                                    | request = response
                                }
                        , zonages =
                            response
                                |> RD.map RD.Success
                                |> RD.withDefault model.zonages
                    }
                        |> Effect.withNone

        ClickedDelete zonage ->
            ( { model
                | deleteRequest =
                    Just <|
                        DeleteRequest RD.NotAsked zonage
              }
            , Effect.none
            )

        CanceledDelete ->
            { model
                | deleteRequest = Nothing
            }
                |> Effect.withNone

        ConfirmedDelete ->
            case model.deleteRequest of
                Nothing ->
                    model
                        |> Effect.withNone

                Just dr ->
                    ( { model | deleteRequest = Just { dr | request = RD.Loading } }
                    , deleteRequest dr.zonage
                    )

        ReceivedDeleteZonage response ->
            case model.deleteRequest of
                Nothing ->
                    model
                        |> Effect.withNone

                Just dr ->
                    { model
                        | deleteRequest =
                            Just
                                { dr
                                    | request = response
                                }
                        , zonages =
                            response
                                |> RD.map RD.Success
                                |> RD.withDefault model.zonages
                    }
                        |> Effect.withNone


deleteRequest : Zonage -> Effect.Effect Shared.Msg Msg
deleteRequest zonage =
    Effect.fromCmd <|
        Api.Auth.delete
            { url = Api.getZonage zonage.id
            , body = Http.emptyBody
            }
            SharedMsg
            ReceivedDeleteZonage
            (Decode.list decodeZonage)


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "Zonages personnalisés"
    , body = [ body model ]
    , route = Route.ParamsZonages
    }


body : Model -> Html Msg
body model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ viewMaybe (viewNouveauZonageModal (model.zonages |> RD.withDefault [])) model.nouveauZonage
        , viewMaybe viewDeleteModal model.deleteRequest
        , div [ DSFR.Grid.col3 ] [ UI.Layout.parametresSideMenu Route.ParamsZonages ]
        , div [ DSFR.Grid.col9 ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ viewZonages model.zonages ]
            ]
        ]


viewNouveauZonageModal : List Zonage -> NouveauZonage -> Html Msg
viewNouveauZonageModal zonages nouveauZonage =
    DSFR.Modal.view
        { id = "nouveau-zonage"
        , label = "nouveau-zonage"
        , openMsg = NoOp
        , closeMsg = Just CanceledAddZonage
        , title = text "Téléverser un nouveau zonage"
        , opened = True
        }
        (viewNouveauZonageBody zonages nouveauZonage)
        Nothing
        |> Tuple.first


viewNouveauZonageBody : List Zonage -> NouveauZonage -> Html Msg
viewNouveauZonageBody zonages nouveauZonage =
    let
        alreadyUsed =
            zonages
                |> List.map (.nom >> String.toLower)
                |> List.member (nouveauZonage |> .nom |> String.toLower)
    in
    div [ class "p-4" ]
        [ div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col12 ]
                [ case nouveauZonage.request of
                    RD.Loading ->
                        div []
                            [ text <|
                                "Le zonage "
                                    ++ nouveauZonage.nom
                                    ++ " est en cours de création."
                            ]

                    RD.Success _ ->
                        div []
                            [ p []
                                [ text <|
                                    "Le zonage "
                                        ++ nouveauZonage.nom
                                        ++ " a été créé avec succès\u{00A0}!"
                                ]
                            , p []
                                [ text <|
                                    "Il peut se passer plusieurs minutes avant que la recherche d'Établissements par zonage soit mise à jour."
                                ]
                            ]

                    _ ->
                        div []
                            [ DSFR.Input.new
                                { value = nouveauZonage.nom
                                , label = text "Nom du zonage"
                                , onInput = UpdatedNomZonage
                                , name = "nom-zonage"
                                }
                                |> DSFR.Input.withError
                                    (if alreadyUsed then
                                        Just [ text "Ce nom est déjà utilisé" ]

                                     else
                                        Nothing
                                    )
                                |> DSFR.Input.view
                            , div
                                [ class "fr-upload-group"
                                ]
                                [ label
                                    [ class "fr-label"
                                    , Html.Attributes.for "file-upload"
                                    ]
                                    [ text "Plan du zonage"
                                    , viewIf (nouveauZonage.fichier == Nothing) <|
                                        span
                                            [ class "fr-hint-text"
                                            ]
                                            [ text "Format EPSG:3949" ]
                                    , case nouveauZonage.fichier of
                                        Just fichier ->
                                            div [ class "flex flex-row gap-2 items-center" ]
                                                [ text fichier
                                                , DSFR.Button.new { label = "", onClick = Just ClickedRemovedZonageFile }
                                                    |> DSFR.Button.onlyIcon DSFR.Icons.System.closeLine
                                                    |> DSFR.Button.tertiaryNoOutline
                                                    |> DSFR.Button.view
                                                ]

                                        Nothing ->
                                            DSFR.Button.new { label = "Choisir...", onClick = Just ClickedUploadFichierZonage }
                                                |> DSFR.Button.tertiaryNoOutline
                                                |> DSFR.Button.view
                                    ]
                                ]
                            , viewIf (RD.isFailure nouveauZonage.request) (text "Une erreur s'est produite, veuillez réessayer.")
                            ]
                ]
            , div [ DSFR.Grid.col12 ]
                [ let
                    valid =
                        (nouveauZonage.nom /= "")
                            && (nouveauZonage.fichier /= Nothing)
                            && not alreadyUsed

                    ( isButtonDisabled, title_ ) =
                        case ( nouveauZonage.request, valid ) of
                            ( RD.Loading, _ ) ->
                                ( True, "Enregistrer" )

                            ( _, False ) ->
                                ( True, "Enregistrer" )

                            _ ->
                                ( False, "Enregistrer" )

                    buttons =
                        case nouveauZonage.request of
                            RD.Success _ ->
                                [ DSFR.Button.new { onClick = Just CanceledAddZonage, label = "OK" }
                                ]

                            _ ->
                                [ DSFR.Button.new { onClick = Just ConfirmedAddZonage, label = "Enregistrer" }
                                    |> DSFR.Button.withAttrs [ title title_, Html.Attributes.name "ajout-nouveau-zonage" ]
                                    |> DSFR.Button.withDisabled isButtonDisabled
                                , DSFR.Button.new { onClick = Just CanceledAddZonage, label = "Annuler" }
                                    |> DSFR.Button.secondary
                                ]
                  in
                  DSFR.Button.group buttons
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            ]
        ]


viewZonage : Zonage -> Html Msg
viewZonage zonage =
    div [ class "flex flex-row gap-4 items-center" ]
        [ div []
            [ text "Nom\u{00A0}: "
            , text zonage.nom
            ]
        , DSFR.Button.new
            { label = "Supprimer"
            , onClick =
                Just <|
                    ClickedDelete zonage
            }
            |> DSFR.Button.onlyIcon deleteFill
            |> DSFR.Button.view
        ]


viewZonages : WebData (List Zonage) -> Html Msg
viewZonages zonages =
    div [ DSFR.Grid.col12, class "p-4 sm:p-8" ]
        [ div [ class "flex flex-col gap-4 fr-card--white p-4" ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col, class "flex flex-row gap-4 justify-between" ]
                    [ h1 [ Typo.fr_h4, class "!my-0" ] [ text "Zonages personnalisés" ]
                    , DSFR.Button.new { label = "Ajouter un zonage", onClick = Just ClickedAddZonage }
                        |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                        |> DSFR.Button.view
                    ]
                ]
            , div [ class "flex flex-col gap-4" ] <|
                case RD.withDefault [] <| zonages of
                    [] ->
                        [ text "Aucun zonage personnalisé pour l'instant." ]

                    zs ->
                        List.intersperse (hr [ class "fr-hr !p-1" ] []) <|
                            List.map viewZonage <|
                                zs
            ]
        ]


viewDeleteModal : DeleteRequest -> Html Msg
viewDeleteModal dr =
    DSFR.Modal.view
        { id = "delete-zonage-modal"
        , label = "delete-zonage-modal"
        , openMsg = NoOp
        , closeMsg = Just CanceledDelete
        , title = div [] [ h2 [] [ text "Suppression de «\u{00A0}", text dr.zonage.nom, text "\u{00A0}»" ] ]
        , opened = True
        }
        (div [ class "flex flex-col gap-4" ]
            [ div [ DSFR.Grid.gridRow ]
                [ div [ DSFR.Grid.col12 ]
                    [ text <|
                        case dr.request of
                            RD.NotAsked ->
                                "Voulez-vous vraiment supprimer ce zonage\u{00A0}?"

                            RD.Loading ->
                                "Suppression en cours..."

                            RD.Failure _ ->
                                "Une erreur s'est produite, veuillez réessayer."

                            RD.Success _ ->
                                "Suppression effectuée avec succès\u{00A0}!"
                    ]
                ]
            , div [ DSFR.Grid.gridRow ]
                [ div [ DSFR.Grid.col12 ]
                    [ let
                        buttons =
                            case dr.request of
                                RD.Success _ ->
                                    [ DSFR.Button.new { onClick = Just CanceledDelete, label = "OK" }
                                    ]

                                _ ->
                                    [ DSFR.Button.new
                                        { onClick = Just ConfirmedDelete
                                        , label = "Confirmer la suppression"
                                        }
                                        |> DSFR.Button.withAttrs [ title "Supprimer", Html.Attributes.name "supprimer-zonage" ]
                                        |> DSFR.Button.withDisabled (dr.request == RD.Loading)
                                    , DSFR.Button.new { onClick = Just CanceledDelete, label = "Annuler" }
                                        |> DSFR.Button.secondary
                                    ]
                      in
                      DSFR.Button.group buttons
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRightInverted
                        |> DSFR.Button.viewGroup
                    ]
                ]
            ]
        )
        Nothing
        |> Tuple.first
