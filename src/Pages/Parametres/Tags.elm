module Pages.Parametres.Tags exposing (Model, Msg, page)

import Accessibility exposing (Html, br, div, h1, h2, p, span, text)
import Api
import Api.Auth
import Api.EntityId exposing (EntityId, decodeEntityId, entityIdToString)
import DSFR.Accordion
import DSFR.Button
import DSFR.CallOut exposing (callout)
import DSFR.Footer exposing (lienTutoriel)
import DSFR.Grid
import DSFR.Icons.System exposing (deleteFill)
import DSFR.Modal
import DSFR.Typography as Typo
import Data.Role
import Data.Territoire exposing (TerritoireId)
import Effect
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.UI exposing (plural)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Layout
import User
import View exposing (View)


type alias Model =
    { tags : WebData Tags
    , deleteRequest : Maybe DeleteRequest
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | ReceivedCurrentTags (WebData Tags)
    | ClickedDelete (EntityId TerritoireId) TagType Tag
    | CanceledDelete
    | ConfirmedDelete
    | ReceivedDelete (WebData Tags)


type alias DeleteRequest =
    { request : WebData Tags
    , territoireId : EntityId TerritoireId
    , tagType : TagType
    , tag : Tag
    }


type alias Tags =
    { activites : List Tag
    , zones : List Tag
    , mots : List Tag
    }


type TagType
    = Activites
    | Zones
    | Mots


tagTypeToString : TagType -> String
tagTypeToString tagType =
    case tagType of
        Activites ->
            "activites_reelles"

        Zones ->
            "entreprise_localisations"

        Mots ->
            "mots_cles"


tagTypeToDisplay : TagType -> String
tagTypeToDisplay tagType =
    case tagType of
        Activites ->
            "Activités réelles"

        Zones ->
            "Zones géographiques"

        Mots ->
            "Mots-clés"


type alias Tag =
    { id : EntityId TagId
    , label : String
    , usages : Int
    }


type TagId
    = TagId


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ user =
    Spa.Page.onNewFlags (\_ -> NoOp) <|
        Spa.Page.element
            { view = view user
            , init = init
            , update = update
            , subscriptions = \_ -> Sub.none
            }


view : User -> Model -> View Msg
view user model =
    { title = UI.Layout.pageTitle "Gestion des étiquettes"
    , body = [ body user model ]
    , route = Route.ParamsTags
    }


body : User -> Model -> Html Msg
body user model =
    let
        territoryId =
            user
                |> User.role
                |> Data.Role.territoire
                |> Maybe.map Data.Territoire.id
                |> Maybe.withDefault Api.EntityId.newId
    in
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ viewDeleteModal model.deleteRequest
        , div [ DSFR.Grid.col3 ] [ UI.Layout.parametresSideMenu Route.ParamsTags ]
        , div [ DSFR.Grid.col9 ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col12, class "p-4 sm:p-8" ]
                    [ div [ class "fr-card--white p-4" ]
                        [ h1 [ Typo.fr_h2 ] [ text "Gestion des étiquettes de qualification" ]
                        , callout Nothing Nothing <|
                            div []
                                [ p []
                                    [ text "Les modifications de qualification impactent l'ensemble des utilisateurs de votre territoire de référence. Nous recommandons de les évoquer en réunion d'équipe avant toute modification individuelle."
                                    ]
                                , br []
                                , p []
                                    [ text "Pourquoi utiliser des étiquettes de qualification dans Deveco\u{00A0}?"
                                    , text " "
                                    , text "Retrouvez plus d'explications sur le "
                                    , lienTutoriel False
                                    , text "."
                                    ]
                                ]
                        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ] <|
                            case model.tags of
                                RD.Success { activites, zones, mots } ->
                                    [ div [ DSFR.Grid.col12 ] <|
                                        viewTags territoryId Activites <|
                                            activites
                                    , div [ DSFR.Grid.col12 ] <|
                                        viewTags territoryId Zones <|
                                            zones
                                    , div [ DSFR.Grid.col12 ] <|
                                        viewTags territoryId Mots <|
                                            mots
                                    ]

                                RD.Loading ->
                                    [ text "Chargement en cours..." ]

                                RD.Failure _ ->
                                    [ text "Erreur, veuillez contacter l'équipe de développement." ]

                                _ ->
                                    [ text "Quelque chose aurait dû se passer..." ]
                        ]
                    ]
                ]
            ]
        ]


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { tags = RD.NotAsked
      , deleteRequest = Nothing
      }
    , getCurrentTags
    )
        |> Shared.pageChangeEffects


getCurrentTags : Effect.Effect sharedMsg Msg
getCurrentTags =
    Effect.fromCmd <|
        Api.Auth.get
            { url = Api.getTerritoryTags
            }
            SharedMsg
            ReceivedCurrentTags
            decodeTags


decodeTags : Decoder Tags
decodeTags =
    Decode.succeed Tags
        |> andMap (Decode.field "activites" <| Decode.list decodeTag)
        |> andMap (Decode.field "zones" <| Decode.list decodeTag)
        |> andMap (Decode.field "mots" <| Decode.list decodeTag)


decodeTag : Decoder Tag
decodeTag =
    Decode.succeed Tag
        |> andMap (Decode.field "id" <| decodeEntityId)
        |> andMap (Decode.field "label" <| Decode.string)
        |> andMap (Decode.field "usages" <| Decode.int)


deleteTag : TagType -> Tag -> Effect.Effect sharedMsg Msg
deleteTag tagType tag =
    Effect.fromCmd <|
        Api.Auth.delete
            { url = Api.getTerritoryTags
            , body =
                [ ( "tagType", Encode.string <| tagTypeToString tagType )
                , ( "id", Encode.string <| entityIdToString tag.id )
                , ( "tagLabel", Encode.string tag.label )
                ]
                    |> Encode.object
                    |> Http.jsonBody
            }
            SharedMsg
            ReceivedDelete
            decodeTags


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedCurrentTags tags ->
            { model | tags = tags }
                |> Effect.withNone

        ClickedDelete territoryId tagType tag ->
            ( { model
                | deleteRequest =
                    Just <|
                        DeleteRequest RD.NotAsked territoryId tagType tag
              }
            , Effect.none
            )

        CanceledDelete ->
            ( { model | deleteRequest = Nothing }
            , Effect.none
            )

        ConfirmedDelete ->
            case model.deleteRequest of
                Nothing ->
                    model |> Effect.withNone

                Just dr ->
                    ( { model | deleteRequest = Just <| { dr | request = RD.Loading } }, deleteTag dr.tagType dr.tag )

        ReceivedDelete response ->
            case response of
                RD.Success _ ->
                    { model
                        | deleteRequest = Nothing
                        , tags = response
                    }
                        |> Effect.withNone

                _ ->
                    { model
                        | deleteRequest =
                            model.deleteRequest
                                |> Maybe.map (\dr -> { dr | request = response })
                    }
                        |> Effect.withNone


viewTags : EntityId TerritoireId -> TagType -> List Tag -> List (Html Msg)
viewTags territoryId tagType tags =
    List.singleton <|
        div [ class "p-2" ] <|
            [ h2 [ Typo.fr_h4 ] [ text <| tagTypeToDisplay tagType ]
            , case tags of
                [] ->
                    span [ class "italic" ] [ text "Aucune étiquette." ]

                _ ->
                    DSFR.Accordion.raw
                        { id = "accordion-" ++ tagTypeToString tagType
                        , title =
                            text "voir "
                                :: (if List.length tags > 1 then
                                        [ text "les ", text <| String.fromInt <| List.length tags, text " étiquettes" ]

                                    else
                                        [ text "l'étiquette"
                                        ]
                                   )
                        , content =
                            List.singleton <|
                                div [ class "flex flex-col gap-2" ] <|
                                    List.map (viewTag territoryId tagType) <|
                                        List.sortWith
                                            (\t1 t2 ->
                                                compare t2.usages t1.usages
                                            )
                                        <|
                                            tags
                        , borderless = False
                        }
            ]


viewTag : EntityId TerritoireId -> TagType -> Tag -> Html Msg
viewTag territoryId tagType ({ label, usages } as tag) =
    div [ class "flex flex-row gap-4 items-center" ]
        [ div []
            [ span [ Typo.textBold ] [ text label ]
            , text " "
            , text "(utilisé "
            , text <| String.fromInt <| usages
            , text " fois)"
            ]
        , DSFR.Button.new
            { label = "Supprimer"
            , onClick =
                Just <|
                    ClickedDelete territoryId tagType tag
            }
            |> DSFR.Button.onlyIcon deleteFill
            |> DSFR.Button.view
        ]


viewDeleteModal : Maybe DeleteRequest -> Html Msg
viewDeleteModal deleteRequest =
    case deleteRequest of
        Nothing ->
            nothing

        Just dr ->
            DSFR.Modal.view
                { id = "delete-tag-modal"
                , label = "delete-tag-modal"
                , openMsg = NoOp
                , closeMsg = Just CanceledDelete
                , title = div [] [ h2 [] [ text "Suppression de «\u{00A0}", text dr.tag.label, text "\u{00A0}»" ] ]
                , opened = True
                }
                (div [ class "flex flex-col gap-4" ]
                    [ if dr.tag.usages > 0 then
                        div []
                            [ p []
                                [ text "Cette qualification est utilisée par "
                                , text <| String.fromInt <| dr.tag.usages
                                , text " entreprise"
                                , text <| plural dr.tag.usages
                                , text " ou créateur"
                                , text <| plural dr.tag.usages
                                , text " d'entreprises."
                                ]
                            , p [] [ text "Confirmez-vous la suppression\u{00A0}?" ]
                            ]

                      else
                        text "Cette qualification n'est pas utilisée."
                    , DSFR.Button.group
                        [ DSFR.Button.new
                            { onClick = Just ConfirmedDelete
                            , label = "Confirmer la suppression"
                            }
                            |> DSFR.Button.withDisabled (dr.request == RD.Loading)
                        , DSFR.Button.new { onClick = Just CanceledDelete, label = "Annuler" }
                            |> DSFR.Button.secondary
                        ]
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRightInverted
                        |> DSFR.Button.viewGroup
                    ]
                )
                Nothing
                |> Tuple.first
