module Pages.Etablissement.New exposing (Model, Msg, page)

import Accessibility exposing (Html, div, formWithListeners, h1, h2, span, text)
import Api
import Api.Auth
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.SearchBar
import DSFR.Typography as Typo exposing (link)
import Data.Etablissement exposing (Etablissement)
import Dict exposing (Dict)
import Effect
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import RemoteData as RD
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Entite
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page () Shared.Msg (View Msg) Model Msg
page shared _ =
    Spa.Page.element <|
        { init = init shared
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        }


type alias Model =
    { recherche : String
    , resultEtablissements : RD.WebData ResultEtablissements
    , etablissement : Maybe Etablissement
    , validationFiche : RD.RemoteData FormErrors ()
    , enregistrementFiche : RD.WebData String
    }


init : Shared.Shared -> () -> ( Model, Effect.Effect Shared.Msg Msg )
init _ _ =
    ( { recherche = ""
      , resultEtablissements = RD.NotAsked
      , etablissement = Nothing
      , validationFiche = RD.NotAsked
      , enregistrementFiche = RD.NotAsked
      }
    , Effect.none
    )
        |> Shared.pageChangeEffects


type Msg
    = UpdatedRecherche String
    | RequestedRecherche
    | ReceivedRecherche (RD.WebData ResultEtablissements)
    | RequestedCreationFiche
    | ReceivedCreationFiche (RD.WebData String)
    | SharedMsg Shared.Msg


type ResultEtablissements
    = New Etablissement
    | Existing String


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        UpdatedRecherche recherche ->
            ( { model | recherche = recherche }, Effect.none )

        RequestedRecherche ->
            ( { model | resultEtablissements = RD.Loading, etablissement = Nothing }, searchEtablissement model.recherche )

        ReceivedRecherche resultEtablissements ->
            let
                etablissement =
                    resultEtablissements
                        |> RD.toMaybe
                        |> Maybe.andThen
                            (\res ->
                                case res of
                                    New e ->
                                        Just e

                                    Existing _ ->
                                        Nothing
                            )
            in
            ( { model | resultEtablissements = resultEtablissements, etablissement = etablissement }, Effect.none )

        ReceivedCreationFiche ((RD.Success siret) as enregistrementFiche) ->
            ( { model | enregistrementFiche = enregistrementFiche }
            , Effect.fromShared <|
                Shared.Navigate <|
                    Route.Etablissement <|
                        siret
            )

        ReceivedCreationFiche enregistrementFiche ->
            ( { model | enregistrementFiche = enregistrementFiche }, Effect.none )

        RequestedCreationFiche ->
            let
                parsedFiche =
                    validateFiche model

                ( enregistrementFiche, effect ) =
                    case parsedFiche of
                        RD.Success () ->
                            ( RD.Loading, createFiche model )

                        _ ->
                            ( RD.NotAsked, Effect.none )
            in
            ( { model | validationFiche = parsedFiche, enregistrementFiche = enregistrementFiche }, effect )


type alias FormErrors =
    { global : Maybe String
    , fields : Dict String (List String)
    }


validateFiche : Model -> RD.RemoteData FormErrors ()
validateFiche { etablissement } =
    case etablissement of
        Nothing ->
            RD.Failure { global = Just "Aucun établissement sélectionné", fields = Dict.empty }

        Just _ ->
            RD.Success ()


createFiche : Model -> Effect.Effect Shared.Msg Msg
createFiche { etablissement } =
    let
        encodedFiche =
            etablissement
                |> Maybe.map
                    (Http.jsonBody
                        << Encode.object
                        << (\{ siret } ->
                                [ ( "siret", Encode.string siret ) ]
                           )
                    )
    in
    case encodedFiche of
        Nothing ->
            Effect.none

        Just jsonBody ->
            Api.Auth.post
                { url = Api.createEtablissement
                , body = jsonBody
                }
                SharedMsg
                ReceivedCreationFiche
                (Decode.field "siret" Decode.string)
                |> Effect.fromCmd


searchEtablissement : String -> Effect.Effect Shared.Msg Msg
searchEtablissement recherche =
    Effect.fromCmd <|
        Api.Auth.get
            { url = Api.searchInseeSiret recherche
            }
            SharedMsg
            ReceivedRecherche
            decodeResultEtablissements


decodeResultEtablissements : Decoder ResultEtablissements
decodeResultEtablissements =
    Decode.field "existing" Decode.bool
        |> Decode.andThen
            (\existing ->
                Decode.field "entreprise" Data.Etablissement.decodeEtablissement
                    |> Decode.map
                        (if existing then
                            .siret >> Existing

                         else
                            New
                        )
            )


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Ajout d'un Établissement exogène"
    , body = UI.Layout.lazyBody body model
    , route = Route.EtablissementNew
    }


ficheTypeForm : Model -> Html Msg
ficheTypeForm model =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex font-bold" ] [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ] [ text "Rechercher l'établissement" ] ]
        , div
            [ class "flex flex-col gap-8" ]
            [ div
                [ class "flex flex-col", DSFR.Grid.colSm7, DSFR.Grid.col12 ]
                [ DSFR.SearchBar.searchBar
                    { submitMsg = RequestedRecherche
                    , buttonLabel = "Rechercher"
                    , inputMsg = UpdatedRecherche
                    , inputLabel = ""
                    , inputPlaceholder = Nothing
                    , inputId = "etablissement-exogene-siret-search"
                    , inputValue = model.recherche
                    , hints =
                        "Un SIRET (14 chiffres)"
                            |> text
                            |> List.singleton
                    , fullLabel = Nothing
                    }
                ]
            , div []
                [ text "Vous ne connaissez pas le SIRET de l'établissement, recherchez-le\u{00A0}: "
                , Typo.externalLink "https://annuaire-entreprises.data.gouv.fr/" [] [ text "https://annuaire-entreprises.data.gouv.fr/" ]
                ]
            , div [ DSFR.Grid.col7 ]
                [ viewEtablissementsList model
                ]
            ]
        ]


body : Model -> Html Msg
body model =
    div [ class "fr-card--white p-4 sm:p-8 flex flex-col gap-8" ]
        [ h1 [] [ text "Ajouter un Établissement exogène au territoire" ]
        , formWithListeners [ Events.onSubmit <| RequestedCreationFiche, class "flex flex-col gap-8" ]
            [ ficheTypeForm model
            , footer model
            ]
        ]


viewEtablissementsList : Model -> Html Msg
viewEtablissementsList { resultEtablissements, etablissement } =
    case resultEtablissements of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            text "Recherche en cours..."

        RD.Failure _ ->
            div [ class "flex flex-col gap-2" ]
                [ div []
                    [ text "Résultat\u{00A0}:"
                    ]
                , div []
                    [ text "Une erreur s'est produite"
                    ]
                ]

        RD.Success (Existing siret) ->
            div [ class "flex flex-col gap-2" ]
                [ div []
                    [ text "Résultat\u{00A0}:"
                    ]
                , span [ class "font-bold" ]
                    [ text "Cet établissement est déjà présent sur votre territoire."
                    , text " "
                    , link
                        (Route.toUrl <|
                            Route.Etablissement <|
                                siret
                        )
                        []
                        [ text "Voir l'établissement." ]
                    ]
                ]

        RD.Success _ ->
            div [ class "flex flex-col gap-2" ]
                [ div []
                    [ text "Résultat\u{00A0}:"
                    ]
                , etablissement
                    |> Maybe.map viewEtablissementCard
                    |> Maybe.withDefault (text "Aucun résultat")
                ]


viewEtablissementCard : Etablissement -> Html msg
viewEtablissementCard { nom, siren, siret, adresse, etatAdministratif, exogene } =
    div [ class "flex flex-col" ]
        [ div [ class "font-bold flex flex-row gap-4 items-baseline" ]
            [ text nom
            , etatAdministratif
                |> Just
                |> UI.Entite.badgeInactif
            , UI.Entite.badgeExogene <|
                exogene
            ]
        , div []
            [ text "SIREN\u{00A0}: "
            , span [ class "font-bold" ] [ text siren ]
            ]
        , div []
            [ text "SIRET\u{00A0}: "
            , span [ class "font-bold" ] [ text siret ]
            ]
        , div []
            [ text "Adresse\u{00A0}: "
            , span [ class "font-bold" ] [ text adresse ]
            ]
        ]


footer : Model -> Html Msg
footer model =
    let
        label =
            case model.enregistrementFiche of
                RD.NotAsked ->
                    "Ajouter l'établissement"

                RD.Loading ->
                    "Ajout en cours..."

                RD.Failure _ ->
                    "Ajouter l'établissement"

                RD.Success _ ->
                    "Ajouter l'établissement"

        isButtonDisabled =
            model.enregistrementFiche == RD.Loading || model.etablissement == Nothing
    in
    div [ class "flex flex-col gap-2" ]
        [ div [ class "flex flex-row justify-end" ]
            [ case model.validationFiche of
                RD.Failure { global } ->
                    case global of
                        Nothing ->
                            nothing

                        Just error ->
                            DSFR.Alert.small { title = Nothing, description = text error }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.info

                _ ->
                    nothing
            , case model.enregistrementFiche of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite, veuillez réessayer." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        , DSFR.Button.group
            [ DSFR.Button.new { onClick = Nothing, label = label }
                |> DSFR.Button.submit
                |> DSFR.Button.withDisabled isButtonDisabled
            , DSFR.Button.new { onClick = Nothing, label = "Précédent" }
                |> DSFR.Button.secondary
                |> DSFR.Button.withDisabled isButtonDisabled
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Etablissements Nothing)
            ]
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]
