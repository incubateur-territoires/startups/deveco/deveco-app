module Pages.Fiches exposing (Model, Msg, page)

import Accessibility exposing (div)
import Effect
import Html exposing (Html)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element <|
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Msg =
    ()


type alias Model =
    ()


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init _ =
    ( ()
    , Effect.fromShared <| Shared.ReplaceUrl <| Route.Dashboard
    )


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update _ model =
    model
        |> Effect.withNone


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Redirection"
    , body = UI.Layout.lazyBody body model
    , route = Route.Fiches
    }


body : Model -> Html Msg
body _ =
    div [] []
