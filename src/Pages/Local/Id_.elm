module Pages.Local.Id_ exposing (Model, Msg(..), page)

import Accessibility exposing (Html, a, div, h1, h3, hr, span, text)
import Api
import Api.Auth
import Api.EntityId exposing (EntityId, unsafeConvert)
import DSFR.Alert
import DSFR.Button exposing (ButtonConfig)
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.Buildings
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Input
import DSFR.Modal
import DSFR.Radio
import DSFR.SearchBar
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Entite exposing (Entite)
import Data.Etablissement exposing (Etablissement)
import Data.Fiche exposing (FicheId)
import Data.Local exposing (Local, LocalId, decodeLocal)
import Data.Particulier
import Data.Proprietaire exposing (Proprietaire)
import Date exposing (Date)
import Effect
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing, viewIf)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (withEmptyAs)
import QS
import RemoteData as RD exposing (RemoteData, WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import Time exposing (Posix, Zone)
import UI.Entite
import UI.Layout
import UI.Local
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (EntityId LocalId) Shared.Msg (View Msg) Model Msg
page shared _ =
    Spa.Page.element
        { view = view shared
        , init = init shared
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { local : WebData Local
    , localId : EntityId LocalId
    , today : Date
    , deleteLocal : Maybe (WebData ())
    , newProprietaire : Maybe NewProprietaire
    , deleteProprietaire : Maybe { proprietaire : Proprietaire, request : WebData Local }
    }


type alias NewProprietaire =
    { type_ : ProprietaireType
    , nom : String
    , prenom : String
    , email : String
    , telephone : String
    , siretRecherche : String
    , siretResultats : RemoteData String (Maybe Etablissement)
    , siretChoix : Maybe Etablissement
    , request : WebData Local
    }


init : Shared.Shared -> EntityId LocalId -> ( Model, Effect.Effect Shared.Msg Msg )
init shared id =
    ( { local = RD.Loading
      , localId = id
      , today = Date.fromPosix shared.timezone shared.now
      , deleteLocal = Nothing
      , newProprietaire = Nothing
      , deleteProprietaire = Nothing
      }
    , Effect.fromCmd <| fetchLocal id
    )
        |> Shared.pageChangeEffects


fetchLocal : EntityId LocalId -> Cmd Msg
fetchLocal id =
    Api.Auth.get
        { url = Api.getLocal id
        }
        SharedMsg
        ReceivedLocal
        decodeLocal


type Msg
    = ReceivedLocal (WebData Local)
    | ClickedDelete
    | CanceledDelete
    | ConfirmedDelete
    | ReceivedDelete (WebData ())
    | AddProprietaire
    | CanceledAddProprietaire
    | SelectProprietaireType ProprietaireType
    | UpdatedProprietaire PField String
    | UpdatedSiret String
    | ConfirmedSiret
    | ReceivedEntrepriseResults (RemoteData String (Maybe Etablissement))
    | ConfirmedAddProprietaire
    | ReceivedAddProprietaire (WebData Local)
    | ClickedDeleteProprietaire Proprietaire
    | CanceledDeleteProprietaire
    | ConfirmedDeleteProprietaire
    | ReceivedDeleteProprietaireResponse (WebData Local)
    | NoOp
    | SharedMsg Shared.Msg


type ProprietaireType
    = Etablissement
    | Manual


type PField
    = Nom
    | Prenom
    | Email
    | Telephone


updateProprietaire : PField -> String -> NewProprietaire -> NewProprietaire
updateProprietaire field value prop =
    case field of
        Nom ->
            { prop | nom = value }

        Prenom ->
            { prop | prenom = value }

        Email ->
            { prop | email = value }

        Telephone ->
            { prop | telephone = value }


defaultNewProprietaire : NewProprietaire
defaultNewProprietaire =
    { type_ = Etablissement
    , nom = ""
    , prenom = ""
    , email = ""
    , telephone = ""
    , siretRecherche = ""
    , siretResultats = RD.NotAsked
    , siretChoix = Nothing
    , request = RD.NotAsked
    }


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedLocal response ->
            ( { model | local = response }, Effect.none )

        ClickedDelete ->
            { model | deleteLocal = Just RD.NotAsked }
                |> Effect.withNone

        CanceledDelete ->
            { model | deleteLocal = Nothing }
                |> Effect.withNone

        ConfirmedDelete ->
            case model.local of
                RD.Success local ->
                    { model | deleteLocal = Just RD.Loading }
                        |> Effect.withCmd (deleteLocal local)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedDelete response ->
            { model | deleteLocal = Just response }
                |> Effect.withNone

        AddProprietaire ->
            { model | newProprietaire = Just defaultNewProprietaire }
                |> Effect.withNone

        CanceledAddProprietaire ->
            { model | newProprietaire = Nothing }
                |> Effect.withNone

        SelectProprietaireType proprietaire ->
            { model
                | newProprietaire =
                    model.newProprietaire
                        |> Maybe.map
                            (\m ->
                                { m
                                    | type_ = proprietaire
                                }
                            )
            }
                |> Effect.withNone

        UpdatedProprietaire field value ->
            { model
                | newProprietaire =
                    model.newProprietaire
                        |> Maybe.map (updateProprietaire field value)
            }
                |> Effect.withNone

        UpdatedSiret siret ->
            { model
                | newProprietaire =
                    model.newProprietaire
                        |> Maybe.map (\p -> { p | siretRecherche = siret, siretResultats = RD.NotAsked })
            }
                |> Effect.withNone

        ConfirmedSiret ->
            let
                ( newResultats, cmd ) =
                    if model.newProprietaire |> Maybe.map (.siretRecherche >> (==) "") |> Maybe.withDefault True then
                        ( RD.Failure "Veuillez saisir un SIRET valide", Cmd.none )

                    else
                        ( RD.Loading
                        , getEntreprises model.local model.newProprietaire
                        )

                newModel =
                    { model
                        | newProprietaire =
                            model.newProprietaire
                                |> Maybe.map (\p -> { p | siretResultats = newResultats })
                    }
            in
            newModel
                |> Effect.withCmd cmd

        ReceivedEntrepriseResults results ->
            { model
                | newProprietaire =
                    model.newProprietaire
                        |> Maybe.map
                            (\p ->
                                { p
                                    | siretResultats = results
                                    , siretChoix =
                                        case results of
                                            RD.Success e ->
                                                e

                                            _ ->
                                                Nothing
                                }
                            )
            }
                |> Effect.withNone

        ConfirmedAddProprietaire ->
            case ( model.local, model.newProprietaire ) of
                ( RD.Success local, Just prop ) ->
                    case prop.request of
                        RD.Loading ->
                            model
                                |> Effect.withNone

                        _ ->
                            { model
                                | newProprietaire =
                                    model.newProprietaire
                                        |> Maybe.map
                                            (\m ->
                                                { m
                                                    | request = RD.Loading
                                                }
                                            )
                            }
                                |> Effect.withCmd (saveProprietaire local prop)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedAddProprietaire (RD.Success local) ->
            { model
                | newProprietaire = Nothing
                , local = RD.Success local
            }
                |> Effect.withNone

        ReceivedAddProprietaire result ->
            { model
                | newProprietaire =
                    model.newProprietaire
                        |> Maybe.map
                            (\m ->
                                { m
                                    | request = result
                                }
                            )
            }
                |> Effect.withNone

        ClickedDeleteProprietaire proprietaire ->
            { model | deleteProprietaire = Just { proprietaire = proprietaire, request = RD.NotAsked } }
                |> Effect.withNone

        CanceledDeleteProprietaire ->
            { model | deleteProprietaire = Nothing }
                |> Effect.withNone

        ConfirmedDeleteProprietaire ->
            case ( model.local, model.deleteProprietaire ) of
                ( RD.Success local, Just del ) ->
                    { model | deleteProprietaire = Just { proprietaire = del.proprietaire, request = RD.Loading } }
                        |> Effect.withCmd (deleteProprietaire del.proprietaire local)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedDeleteProprietaireResponse response ->
            case response of
                RD.Success _ ->
                    { model | deleteProprietaire = Nothing, local = response }
                        |> Effect.withNone

                _ ->
                    { model
                        | deleteProprietaire =
                            model.deleteProprietaire
                                |> Maybe.map (\d -> { d | request = response })
                    }
                        |> Effect.withNone


deleteProprietaire : Proprietaire -> Local -> Cmd Msg
deleteProprietaire proprietaire local =
    let
        jsonBody =
            [ ( "proprietaireId", Api.EntityId.encodeEntityId proprietaire.id )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    Api.Auth.post
        { url = Api.deleteProprietaire local.id
        , body = jsonBody
        }
        SharedMsg
        ReceivedDeleteProprietaireResponse
        decodeLocal


serializeSearchQuery : Local -> String -> String
serializeSearchQuery local recherche =
    QS.empty
        |> QS.setStr "local" (Api.EntityId.entityIdToString local.id)
        |> QS.setStr "recherche" recherche
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


getEntreprises : WebData Local -> Maybe NewProprietaire -> Cmd Msg
getEntreprises local newProprietaire =
    case ( local, newProprietaire ) of
        ( RD.Success l, Just newP ) ->
            case newP.type_ of
                Etablissement ->
                    Api.Auth.get
                        { url =
                            Api.getEtablissements <|
                                serializeSearchQuery l newP.siretRecherche
                        }
                        SharedMsg
                        (RD.mapError (\_ -> "Erreur réseau, veuillez réessayer et nous contacter si le problème persiste.") >> ReceivedEntrepriseResults)
                        decodeSiret

                _ ->
                    Cmd.none

        _ ->
            Cmd.none


{-| TODO gérer correctement les erreurs
-}
decodeSiret : Decoder (Maybe Etablissement)
decodeSiret =
    Decode.map Just <| Decode.field "entreprise" Data.Etablissement.decodeEtablissement


saveProprietaire : Local -> NewProprietaire -> Cmd Msg
saveProprietaire local prop =
    let
        payload =
            Http.jsonBody <|
                Encode.object <|
                    case prop.type_ of
                        Manual ->
                            [ ( "type", "manual" |> Encode.string )
                            , ( "nom", prop.nom |> Encode.string )
                            , ( "prenom", prop.prenom |> Encode.string )
                            , ( "email", prop.email |> Encode.string )
                            , ( "telephone", prop.telephone |> Encode.string )
                            ]

                        Etablissement ->
                            [ ( "type", "siret" |> Encode.string )
                            , ( "siret"
                              , prop.siretChoix
                                    |> Maybe.map (.siret >> Encode.string)
                                    |> Maybe.withDefault Encode.null
                              )
                            ]
    in
    Api.Auth.post
        { url = Api.addProprietaire local.id
        , body = payload
        }
        SharedMsg
        ReceivedAddProprietaire
        decodeLocal


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


deleteLocal : Local -> Cmd Msg
deleteLocal local =
    Api.Auth.delete
        { url = Api.getLocal <| .id local
        , body = Http.emptyBody
        }
        SharedMsg
        ReceivedDelete
        (Decode.succeed ())


view : Shared.Shared -> Model -> View Msg
view { now, timezone } model =
    let
        title =
            case model.local of
                RD.Success local ->
                    "Local de " ++ .titre local

                _ ->
                    "Local"
    in
    { title = UI.Layout.pageTitle <| title
    , body = body timezone now model
    , route = Route.Local <| model.localId
    }


body : Zone -> Posix -> Model -> List (Html Msg)
body zone now model =
    [ div [ class "flex flex-col gap-4" ]
        [ div [ class "flex flex-row justify-between items-center" ]
            [ DSFR.Button.new
                { label = "Retour"
                , onClick = Nothing
                }
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Locaux Nothing)
                |> DSFR.Button.leftIcon DSFR.Icons.System.arrowLeftLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withAttrs [ class "text-underline" ]
                |> DSFR.Button.view
            , Maybe.withDefault nothing <|
                Maybe.map
                    (\( author, date ) ->
                        text <|
                            (\d -> "Dernière modification " ++ d ++ " par " ++ author) <|
                                if Date.fromPosix zone now == Date.fromPosix zone date then
                                    " " ++ (String.toLower <| Lib.Date.formatRelative now date)

                                else
                                    " le " ++ Lib.Date.dateTimeToShortFrenchString zone date
                    )
                <|
                    case model.local of
                        RD.Success local ->
                            Just ( local.auteurModification, local.dateModification )

                        _ ->
                            Nothing
            ]
        , case model.local of
            RD.Loading ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ DSFR.Icons.System.refreshFill |> DSFR.Icons.iconLG
                    , "Chargement en cours..."
                        |> text
                    ]

            RD.Success local ->
                div [ class "flex flex-col gap-4" ]
                    [ viewDeleteModal local model.deleteLocal
                    , viewAddProprietaireModal local model.newProprietaire
                    , viewDeleteProprietaireModal local model.deleteProprietaire
                    , viewLocal local
                    , viewOccupation local
                    , viewSuiviLocal local
                    ]

            _ ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ "Une erreur s'est produite, veuillez recharger la page."
                        |> text
                    ]
        ]
    ]


editButton : EntityId LocalId -> ButtonConfig msg
editButton id =
    DSFR.Button.new { label = "", onClick = Nothing }
        |> DSFR.Button.linkButton (Route.toUrl <| Route.LocalEdit <| id)
        |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
        |> DSFR.Button.withAttrs [ class "!mb-0" ]
        |> DSFR.Button.tertiaryNoOutline


deleteButton : ButtonConfig Msg
deleteButton =
    DSFR.Button.new { label = "", onClick = Just <| ClickedDelete }
        |> DSFR.Button.onlyIcon DSFR.Icons.System.deleteFill
        |> DSFR.Button.withAttrs [ Html.Attributes.id "local-delete-btn", class "!mb-0" ]
        |> DSFR.Button.tertiaryNoOutline


viewLocal : Local -> Html Msg
viewLocal ({ id, titre, adresse, types, statut } as local) =
    div [ class "p-4 sm:p-8 fr-card--white" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col ]
                [ div [ DSFR.Grid.gridRow, class "p-2" ]
                    [ div [ DSFR.Grid.col ]
                        [ div [ class "flex flex-row gap-4 items-center justify-between" ]
                            [ types
                                |> UI.Local.localTypeTags
                            , div [ class "flex flex-row items-center gap-4" ]
                                [ statut
                                    |> UI.Local.localVacantBadge
                                , [ editButton id
                                  , deleteButton
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.viewGroup
                                ]
                            ]
                        ]
                    ]
                , div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col ]
                        [ div [ class "flex flex-col p-2" ]
                            [ h1 [ Typo.textBold, Typo.fr_h3, class "!mb-0" ] [ text titre ]
                            , div [ Typo.textBold, class "!mb-0" ] [ text adresse ]
                            ]
                        ]
                    ]
                , div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col9 ] [ viewLocalInfos local ]
                    , div [ DSFR.Grid.col3 ] [ viewSuivi local ]
                    ]
                ]
            ]
        ]


viewLocalInfos : Local -> Html Msg
viewLocalInfos local =
    div [ class "p-2" ]
        [ div [ class "fr-card--grey p-2" ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col4 ]
                    [ div [ class "flex flex-col gap-4" ]
                        [ div [ class "p-2" ]
                            [ div [ Typo.textBold ] [ text "Descriptif" ]
                            , hr [ class "!pb-4" ] []
                            , div [ Typo.textSm, class "!mb-0 min-h-[2rem]" ] <|
                                [ Lib.UI.infoLine Nothing "Surface totale" <|
                                    case local.surface of
                                        "" ->
                                            "-"

                                        surface ->
                                            surface ++ "\u{00A0}m²"
                                ]
                            , div [ Typo.textSm, class "!mb-0 min-h-[2rem]" ] <|
                                [ Lib.UI.infoLine Nothing "Loyer annuel" <|
                                    case local.loyer of
                                        "" ->
                                            "-"

                                        loyer ->
                                            loyer ++ "\u{00A0}€"
                                ]
                            ]
                        , div [ class "p-2" ]
                            [ div [ Typo.textBold ] [ text "Zone géographique" ]
                            , hr [ class "!pb-4" ] []
                            , case local.localisations of
                                [] ->
                                    text "-"

                                localisations ->
                                    div []
                                        [ text <|
                                            String.join "\u{00A0}— " <|
                                                localisations
                                        ]
                            ]
                        ]
                    ]
                , div [ DSFR.Grid.col8 ]
                    [ div [ class "p-2" ]
                        [ div [ Typo.textBold ] [ text "Commentaires" ]
                        , hr [ class "!pb-4" ] []
                        , div [ class "whitespace-pre-wrap" ]
                            [ text <|
                                withEmptyAs "-" <|
                                    local.commentaire
                            ]
                        ]
                    ]
                ]
            ]
        ]


viewOccupation : Local -> Html Msg
viewOccupation local =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col6 ]
            [ div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
                [ div [ class "flex flex-row justify-between items-center" ]
                    [ h3 [ Typo.fr_h6, class "!mb-0" ]
                        [ text "Propriétaire"
                        , text <|
                            if List.length local.proprietaires > 1 then
                                "s"

                            else
                                ""
                        , text " du local"
                        ]
                    , DSFR.Button.new
                        { label = "Ajouter un propriétaire"
                        , onClick = Just AddProprietaire
                        }
                        |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.view
                    ]
                , div [ class "flex flex-col gap-4" ] <|
                    case local.proprietaires of
                        [] ->
                            [ span [ class "italic" ] [ text "Aucun propriétaire enregistré." ] ]

                        _ ->
                            List.intersperse (div [ class "border-[0.1rem]" ] []) <|
                                List.map
                                    (\proprietaire ->
                                        div [ class "flex flex-row" ]
                                            [ viewEntite True <| proprietaire.entite
                                            , DSFR.Button.new
                                                { onClick = Just <| ClickedDeleteProprietaire proprietaire
                                                , label = "Supprimer"
                                                }
                                                |> DSFR.Button.onlyIcon DSFR.Icons.System.deleteLine
                                                |> DSFR.Button.tertiaryNoOutline
                                                |> DSFR.Button.withAttrs [ class "self-center" ]
                                                |> DSFR.Button.view
                                            ]
                                    )
                                <|
                                    .proprietaires <|
                                        local
                ]
            ]
        , div [ DSFR.Grid.col6 ]
            [ div [ class "flex flex-col gap-4 p-4 sm:p-8 fr-card--white" ]
                [ div [ class "flex flex-row justify-between items-center" ]
                    [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Établissement occupant" ]
                    , DSFR.Button.new
                        { label = "Ajouter un établissement"
                        , onClick = Nothing
                        }
                        |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.withDisabled True
                        |> DSFR.Button.view
                    ]
                , div [ class "flex flex-col gap-4" ] <|
                    [ span [ class "italic" ] [ text "Aucun occupant enregistré." ] ]
                ]
            ]
        ]


viewEntite : Bool -> Entite -> Html msg
viewEntite removeCurrent entite =
    case ( entite.etablissement, entite.createur ) of
        ( Just e, _ ) ->
            linkToFicheWrapper Nothing (Just e.siret) <|
                (viewEntreprise False e
                    ++ [ div []
                            [ text "Autres propriétés\u{00A0}: "
                            , span [ Typo.textBold ]
                                [ text <|
                                    String.fromInt <|
                                        (\l ->
                                            if removeCurrent then
                                                l - 1

                                            else
                                                l
                                        )
                                        <|
                                            List.length <|
                                                entite.proprietes
                                ]
                            ]
                       ]
                )

        ( _, Just p ) ->
            linkToFicheWrapper (Maybe.map unsafeConvert <| entite.ficheId) Nothing <|
                [ div [ class "flex flex-row justify-between" ]
                    [ UI.Entite.icon entite
                    ]
                , div [] <|
                    [ span [ Typo.textBold ]
                        [ text <|
                            Data.Particulier.displayNomCreateur <|
                                p
                        ]
                    , div []
                        [ text "Email\u{00A0}: "
                        , span [ Typo.textBold ]
                            [ text <|
                                withEmptyAs "-" <|
                                    p.email
                            ]
                        ]
                    , div []
                        [ text "Téléphone\u{00A0}: "
                        , span [ Typo.textBold ]
                            [ text <|
                                withEmptyAs "-" <|
                                    p.telephone
                            ]
                        ]
                    ]
                ]

        _ ->
            nothing


linkToFicheWrapper : Maybe (EntityId FicheId) -> Maybe String -> List (Html msg) -> Html msg
linkToFicheWrapper ficheId siret =
    let
        route =
            case ( ficheId, siret ) of
                ( _, Just s ) ->
                    Just <|
                        Route.Etablissement <|
                            s

                ( Just fid, _ ) ->
                    Just <|
                        Route.Createur <|
                            fid

                ( Nothing, Nothing ) ->
                    Nothing
    in
    case route of
        Nothing ->
            div [ class "p-2" ]

        Just r ->
            a
                [ class "custom-hover p-2 w-full"
                , Html.Attributes.style "background-image" "none"
                , Html.Attributes.href <| Route.toUrl <| r
                , Html.Attributes.title "Voir la fiche dans le portefeuille"
                ]


viewEntreprise : Bool -> Etablissement -> List (Html msg)
viewEntreprise withCategory entreprise =
    [ div [ class "flex flex-row justify-between" ]
        [ DSFR.Icons.Buildings.hotelLine
            |> DSFR.Icons.iconLG
        , div [] [ UI.Entite.badgeInactif <| Just entreprise.etatAdministratif ]
        ]
    , div []
        [ span [ Typo.textBold ]
            [ text <| Data.Etablissement.displayNomEnseigne <| entreprise
            ]
        ]
    , viewIf withCategory <|
        div []
            [ text "Catégorie\u{00A0}: "
            , span [ Typo.textBold ]
                [ text <|
                    withEmptyAs "-" <|
                        Maybe.withDefault "-" <|
                            entreprise.categorieActivitePrincipaleUniteLegale
                ]
            ]
    , div []
        [ text "Adresse\u{00A0}: "
        , span [ Typo.textBold ]
            [ text entreprise.adresse
            ]
        ]
    ]


viewSuivi : Local -> Html msg
viewSuivi _ =
    let
        suivis =
            []
    in
    div [ class "p-2" ]
        [ div [ Typo.textBold ]
            [ text "Élément"
            , text <|
                if List.length suivis > 1 then
                    "s"

                else
                    ""
            , text " de suivi"
            ]
        , hr [ class "!pb-4" ] []
        , case suivis of
            [] ->
                div [ class "italic" ] [ text "Aucun élément de suivi en cours." ]

            ds ->
                ds
                    |> List.map
                        ((\t -> { data = t, toString = identity })
                            >> DSFR.Tag.unclickable
                        )
                    |> DSFR.Tag.medium
        ]


viewSuiviLocal : Local -> Html msg
viewSuiviLocal _ =
    let
        suivis =
            []
    in
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12 ]
            [ div [ class "p-4 sm:p-8 fr-card--white" ]
                [ div [ class "flex flex-row justify-between" ]
                    [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Suivi du local" ]
                    , DSFR.Button.new
                        { label = "Ajouter un élément de suivi"
                        , onClick = Nothing
                        }
                        |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.withDisabled True
                        |> DSFR.Button.view
                    ]
                , hr [ class "!pb-4" ] []
                , case suivis of
                    [] ->
                        text "-"

                    ds ->
                        ds
                            |> List.map
                                ((\t -> { data = t, toString = identity })
                                    >> DSFR.Tag.unclickable
                                )
                            |> DSFR.Tag.medium
                ]
            ]
        ]


viewDeleteModal : Local -> Maybe (WebData ()) -> Html Msg
viewDeleteModal local deleteRequest =
    case deleteRequest of
        Nothing ->
            nothing

        Just request ->
            let
                modalBody =
                    case request of
                        RD.NotAsked ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "Êtes-vous sûr(e) de vouloir supprimer le local\u{00A0}?"
                                    , description = Just <| text "Cette opération est définitive et irréversible."
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.warning
                                , [ DSFR.Button.new { label = "Je veux vraiment supprimer le local", onClick = Just ConfirmedDelete }
                                        |> DSFR.Button.leftIcon DSFR.Icons.System.deleteFill
                                  , DSFR.Button.new { label = "Annuler", onClick = Just CanceledDelete }
                                        |> DSFR.Button.tertiaryNoOutline
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.iconsLeft
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]

                        RD.Loading ->
                            DSFR.Alert.medium
                                { title = "Suppression en cours..."
                                , description = Nothing
                                }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.info

                        RD.Failure _ ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "Une erreur s'est produite, veuillez fermer la fenêtre et réessayer."
                                    , description = Nothing
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                                , [ DSFR.Button.new { label = "OK", onClick = Just CanceledDelete }
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]

                        RD.Success () ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "Le local a été supprimé avec succès\u{00A0}!"
                                    , description = Just <| text "Cliquez sur OK pour retourner à la liste des locaux."
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.success
                                , [ DSFR.Button.new { label = "OK", onClick = Nothing }
                                        |> DSFR.Button.linkButton (Route.toUrl <| Route.Locaux Nothing)
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]
            in
            DSFR.Modal.view
                { id = "suppression"
                , label = "suppression"
                , openMsg = NoOp
                , closeMsg = Nothing
                , title = text <| "Supprimer le local " ++ .titre local ++ "\u{00A0}?"
                , opened = True
                }
                modalBody
                Nothing
                |> Tuple.first


isValid : NewProprietaire -> Bool
isValid prop =
    case prop.type_ of
        Manual ->
            [ .nom, .prenom, .email ]
                |> List.map (\a -> a prop)
                |> List.any ((/=) "")

        Etablissement ->
            prop.siretChoix /= Nothing


viewAddProprietaireModal : Local -> Maybe NewProprietaire -> Html Msg
viewAddProprietaireModal local newProprietaire =
    case newProprietaire of
        Nothing ->
            nothing

        Just prop ->
            let
                disabledButton =
                    RD.Loading == prop.request || (not <| isValid <| prop)

                modalBody =
                    div []
                        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                            [ DSFR.Radio.group
                                { id = "type-ajout"
                                , options =
                                    [ Etablissement
                                    , Manual
                                    ]
                                , current = Just prop.type_
                                , toLabel =
                                    \s ->
                                        text <|
                                            case s of
                                                Etablissement ->
                                                    "Depuis les établissements de mon territoire"

                                                Manual ->
                                                    "Créer un propriétaire"
                                , toId =
                                    \s ->
                                        case s of
                                            Etablissement ->
                                                "etablissement"

                                            Manual ->
                                                "manuel"
                                , msg = SelectProprietaireType
                                , legend = nothing
                                }
                                |> DSFR.Radio.withHint
                                    (Just <|
                                        \s ->
                                            text <|
                                                case s of
                                                    Etablissement ->
                                                        "Rechercher par SIRET"

                                                    Manual ->
                                                        "Saisir les informations manuellement"
                                    )
                                |> DSFR.Radio.view
                                |> List.singleton
                                |> div [ DSFR.Grid.col12, DSFR.Grid.colSm4 ]
                            , div [ DSFR.Grid.col12, DSFR.Grid.colSm8, class "sm:border-l-2 sm:border-gray" ] <|
                                List.singleton <|
                                    case prop.type_ of
                                        Etablissement ->
                                            viewSiretForm prop

                                        Manual ->
                                            viewManualForm prop
                            ]
                        , [ DSFR.Button.new { label = "OK", onClick = Just ConfirmedAddProprietaire }
                                |> DSFR.Button.withDisabled disabledButton
                          , DSFR.Button.new { label = "Annuler", onClick = Just CanceledAddProprietaire }
                                |> DSFR.Button.secondary
                          ]
                            |> DSFR.Button.group
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
            in
            DSFR.Modal.view
                { id = "ajout"
                , label = "ajout"
                , openMsg = NoOp
                , closeMsg = Just CanceledAddProprietaire
                , title = text <| "Ajouter un propriétaire au local " ++ .titre local
                , opened = True
                }
                modalBody
                Nothing
                |> Tuple.first


viewDeleteProprietaireModal : Local -> Maybe { proprietaire : Proprietaire, request : WebData Local } -> Html Msg
viewDeleteProprietaireModal local proprietaire =
    case proprietaire of
        Nothing ->
            nothing

        Just delProp ->
            let
                disabledButton =
                    RD.Loading == delProp.request

                modalBody =
                    div []
                        [ text "Êtes-vous sûr(e) de vouloir retirer ce propriétaire du local\u{00A0}?"
                        , [ DSFR.Button.new { label = "OK", onClick = Just ConfirmedDeleteProprietaire }
                                |> DSFR.Button.withDisabled disabledButton
                          , DSFR.Button.new { label = "Annuler", onClick = Just CanceledDeleteProprietaire }
                                |> DSFR.Button.secondary
                          ]
                            |> DSFR.Button.group
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
            in
            DSFR.Modal.view
                { id = "suppression-proprietaire"
                , label = "suppression-proprietaire"
                , openMsg = NoOp
                , closeMsg = Just CanceledDeleteProprietaire
                , title = text <| "Supprimer un propriétaire du local " ++ .titre local
                , opened = True
                }
                modalBody
                Nothing
                |> Tuple.first


viewManualForm : NewProprietaire -> Html Msg
viewManualForm prop =
    div [ class "flex flex-col gap-4" ]
        [ div [ class "flex flex-col gap-4" ]
            [ div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                [ DSFR.Input.new
                    { value = prop.nom
                    , onInput = UpdatedProprietaire Nom
                    , label = text "Nom"
                    , name = "proprietaire-manuel-nom"
                    }
                    |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                    |> DSFR.Input.view
                , DSFR.Input.new
                    { value = prop.prenom
                    , onInput = UpdatedProprietaire Prenom
                    , label = text "Prénom"
                    , name = "proprietaire-manuel-prenom"
                    }
                    |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                    |> DSFR.Input.view
                ]
            , div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                [ DSFR.Input.new
                    { value = prop.email
                    , onInput = UpdatedProprietaire Email
                    , label = text "Email"
                    , name = "proprietaire-manuel-email"
                    }
                    |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                    |> DSFR.Input.view
                , DSFR.Input.new
                    { value = prop.telephone
                    , onInput = UpdatedProprietaire Telephone
                    , label = text "Téléphone"
                    , name = "proprietaire-manuel-telephone"
                    }
                    |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                    |> DSFR.Input.view
                ]
            ]
        ]


viewSiretForm : NewProprietaire -> Html Msg
viewSiretForm prop =
    div [ class "flex flex-col gap-4" ]
        [ div [ class "flex flex-col" ]
            [ DSFR.SearchBar.searchBar
                { submitMsg = ConfirmedSiret
                , buttonLabel = "Rechercher"
                , inputMsg = UpdatedSiret
                , inputLabel = ""
                , inputPlaceholder = Nothing
                , inputId = "proprietaire-recherche-siret"
                , inputValue = prop.siretRecherche
                , hints = []
                , fullLabel = Just <| text <| "Rechercher dans les Établissements de mon territoire"
                }
            , case prop.siretResultats of
                RD.Failure err ->
                    div [ class "p-4 gap-4" ]
                        [ DSFR.Alert.small { title = Nothing, description = text err }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.error
                        ]

                RD.Loading ->
                    div [ class "p-4 gap-4" ]
                        [ div [] [ text "Récupération en cours..." ]
                        ]

                RD.Success Nothing ->
                    div [ class "p-4 gap-4" ]
                        [ div [] [ text "Aucun résultat." ]
                        ]

                RD.Success (Just e) ->
                    div [ class "p-4 gap-4" ]
                        [ div [] <|
                            List.singleton <|
                                text "Résultat\u{00A0}:"
                        , div [] <| viewEntreprise True e
                        ]

                _ ->
                    nothing
            ]
        ]
