module Pages.Auth.Check exposing (page)

import Accessibility as Html
import Api.Auth
import Effect
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Page (Maybe String) Shared.Msg (View ()) Model ()
page shared =
    Spa.Page.onNewFlags (\_ -> ()) <|
        Spa.Page.element
            { init = init
            , update = update
            , view = view shared
            , subscriptions = subscriptions
            }


type alias Model =
    { redir : Maybe String }



-- INIT


init : Maybe String -> ( Model, Effect.Effect Shared.Msg () )
init redir =
    ( { redir = redir }
    , Effect.fromSharedCmd <| Api.Auth.checkAuth False redir
    )
        |> Shared.pageChangeEffects



-- UPDATE


update : () -> Model -> ( Model, Effect.Effect Shared.Msg () )
update _ model =
    ( model, Effect.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub ()
subscriptions _ =
    Sub.none



-- VIEW


view : Shared.Shared -> Model -> View ()
view _ model =
    { title = UI.Layout.pageTitle <| "Authentification"
    , body = UI.Layout.lazyBody body model
    , route = Route.AuthCheck model.redir
    }


body : Model -> Html.Html ()
body _ =
    Html.text "Validation du jeton en cours..."
