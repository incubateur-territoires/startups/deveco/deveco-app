module Pages.Admin.Territoires.AddModal exposing (Model, Msg, init, isDone, subscriptions, territoryCreated, update, view)

import Accessibility exposing (Html, div, formWithListeners, p, text)
import Api
import Api.Auth
import DSFR.Alert
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid
import DSFR.Input
import DSFR.Modal
import DSFR.Tag
import Data.Territoire exposing (Territoire)
import Effect
import Html.Attributes exposing (class, title)
import Html.Attributes.Extra exposing (attributeIf)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import MultiSelectRemote
import RemoteData as RD exposing (WebData)
import Shared
import SingleSelectRemote


type alias Model =
    { nomTerritoire : String
    , groupement : Bool
    , typeTerritoire : Type
    , selectNewTerritoire : SingleSelectRemote.SmartSelect Msg NewTerritory
    , selectNewTerritoires : MultiSelectRemote.SmartSelect Msg NewTerritory
    , newTerritories : List NewTerritory
    , saveTerritoireRequest : WebData Territoire
    , isDone : Bool
    , territoryCreated : Bool
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
      --
    | UpdatedNomTerritoire String
    | ToggledGroupement Bool
    | SelectedTypeTerritoire Type
      --
    | SelectedNewTerritoire ( NewTerritory, SingleSelectRemote.Msg NewTerritory )
    | UpdatedSelectNewTerritoire (SingleSelectRemote.Msg NewTerritory)
      --
    | SelectedNewTerritoires ( List NewTerritory, MultiSelectRemote.Msg NewTerritory )
    | UpdatedSelectNewTerritoires (MultiSelectRemote.Msg NewTerritory)
      --
    | UnselectNewTerritoire NewTerritory
      --
    | RequestedSaveTerritoire
    | ReceivedSaveTerritoire (WebData Territoire)
      --
    | CloseModal


init : ( Model, Effect.Effect Shared.Msg Msg )
init =
    { nomTerritoire = ""
    , groupement = False
    , typeTerritoire = Commune
    , newTerritories = []
    , selectNewTerritoire =
        SingleSelectRemote.init "select-new-territoire"
            { selectionMsg = SelectedNewTerritoire
            , internalMsg = UpdatedSelectNewTerritoire
            , characterSearchThreshold = 2
            , debounceDuration = 400
            }
    , selectNewTerritoires =
        MultiSelectRemote.init "select-new-territoires"
            { selectionMsg = SelectedNewTerritoires
            , internalMsg = UpdatedSelectNewTerritoires
            , characterSearchThreshold = 2
            , debounceDuration = 400
            }
    , saveTerritoireRequest = RD.NotAsked
    , isDone = False
    , territoryCreated = False
    }
        |> Effect.withNone


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ SingleSelectRemote.subscriptions model.selectNewTerritoire
        , MultiSelectRemote.subscriptions model.selectNewTerritoires
        ]


type alias NewTerritory =
    { id : String
    , name : String
    }


type Type
    = Commune
    | EPCI
    | Metropole
    | Petr
    | Departement
    | Region


types : List Type
types =
    [ Commune
    , EPCI
    , Metropole
    , Petr
    , Departement
    , Region
    ]


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        UpdatedNomTerritoire nom ->
            ( { model | nomTerritoire = nom }
            , Effect.none
            )

        RequestedSaveTerritoire ->
            { model | saveTerritoireRequest = RD.Loading }
                |> Effect.withCmd (saveTerritoire model)

        ReceivedSaveTerritoire response ->
            { model | saveTerritoireRequest = response, territoryCreated = RD.isSuccess response }
                |> Effect.withNone

        ToggledGroupement groupement ->
            { model | groupement = groupement, newTerritories = [] } |> Effect.withNone

        SelectedTypeTerritoire typeTerritoire ->
            { model | typeTerritoire = typeTerritoire, newTerritories = [] } |> Effect.withNone

        SelectedNewTerritoire ( newTerritoire, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg (selectConfig model.typeTerritoire) model.selectNewTerritoire
            in
            ( { model
                | selectNewTerritoire = updatedSelect
                , newTerritories = [ newTerritoire ]
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectNewTerritoire sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg (selectConfig model.typeTerritoire) model.selectNewTerritoire
            in
            ( { model
                | selectNewTerritoire = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        SelectedNewTerritoires ( newTerritoires, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectConfig model.typeTerritoire) model.selectNewTerritoires
            in
            ( { model
                | selectNewTerritoires = updatedSelect
                , newTerritories = newTerritoires
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectNewTerritoires sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg (selectConfig model.typeTerritoire) model.selectNewTerritoires
            in
            ( { model
                | selectNewTerritoires = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectNewTerritoire { id } ->
            { model | newTerritories = model.newTerritories |> List.filter (\t -> t.id /= id) }
                |> Effect.withNone

        CloseModal ->
            { model | isDone = True }
                |> Effect.withNone


typeTerritoireToString : Type -> String
typeTerritoireToString t =
    case t of
        Commune ->
            "commune"

        EPCI ->
            "epci"

        Metropole ->
            "metropole"

        Petr ->
            "petr"

        Departement ->
            "departement"

        Region ->
            "region"


typeTerritoireToDisplay : Type -> String
typeTerritoireToDisplay t =
    case t of
        Commune ->
            "Commune"

        EPCI ->
            "EPCI"

        Metropole ->
            "Métropole"

        Petr ->
            "PETR"

        Departement ->
            "Département"

        Region ->
            "Région"


stringToTypeTerritoire : String -> Maybe Type
stringToTypeTerritoire string =
    case string of
        "commune" ->
            Just Commune

        "epci" ->
            Just EPCI

        "metropole" ->
            Just Metropole

        "petr" ->
            Just Petr

        "departement" ->
            Just Departement

        "region" ->
            Just Region

        _ ->
            Nothing


view : List Territoire -> Model -> Html Msg
view territoires model =
    DSFR.Modal.view
        { id = "territoire"
        , label = "territoire"
        , openMsg = NoOp
        , closeMsg = Nothing
        , title = text "Ajouter un territoire"
        , opened = True
        }
        (viewTerritoireForm territoires model)
        Nothing
        |> Tuple.first


viewTerritoireForm : List Territoire -> Model -> Html Msg
viewTerritoireForm territoiresExistants model =
    let
        nomExistant =
            List.member model.nomTerritoire <| List.map Data.Territoire.nom <| territoiresExistants
    in
    case model.saveTerritoireRequest of
        RD.Success _ ->
            div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col12 ]
                    [ DSFR.Alert.small { title = Just "Création réussie", description = text "L'ajout des établissements de ce territoire est désormais en cours." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.success
                        |> List.singleton
                        |> div [ class "flex justify-center items-center p-4 w-full" ]
                    ]
                , div [ DSFR.Grid.col12 ]
                    [ footerUtilisateur False model
                    ]
                ]

        _ ->
            formWithListeners [ Events.onSubmit <| RequestedSaveTerritoire, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value = model.nomTerritoire
                        , label = text "Nom"
                        , onInput = UpdatedNomTerritoire
                        , name = "territoire"
                        }
                        |> DSFR.Input.withExtraAttrs
                            [ attributeIf False <| title "Ce nom est déjà pris"
                            , attributeIf False <| class "fr-input-group--error"
                            ]
                        |> DSFR.Input.view
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ DSFR.Input.new
                        { value = model.typeTerritoire |> typeTerritoireToString
                        , onInput =
                            stringToTypeTerritoire
                                >> Maybe.map SelectedTypeTerritoire
                                >> Maybe.withDefault NoOp
                        , label = text "Type"
                        , name = "select-type"
                        }
                        |> DSFR.Input.select
                            { options =
                                if model.groupement then
                                    [ Commune, EPCI, Departement ]

                                else
                                    types
                            , toId = typeTerritoireToString
                            , toLabel = text << typeTerritoireToDisplay
                            }
                        |> DSFR.Input.view
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ DSFR.Checkbox.group
                        { id = "geojson-props"
                        , label = text "Groupement"
                        , onChecked = \_ -> ToggledGroupement
                        , values = [ True ]
                        , checked = [ model.groupement ]
                        , valueAsString = \_ -> "groupement"
                        , toId = \_ -> "groupement"
                        , toLabel = \_ -> "Ce territoire est un groupement"
                        }
                        |> DSFR.Checkbox.viewGroup
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ if not model.groupement then
                        model.selectNewTerritoire
                            |> SingleSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = model.newTerritories |> List.head
                                , optionLabelFn =
                                    \{ name } ->
                                        name
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = text <| typeTerritoireToDisplay <| model.typeTerritoire
                                , searchPrompt = ""
                                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg = \searchText -> "Aucun autre territoire n'a été trouvé pour " ++ searchText
                                , noOptionsMsg = "Aucune entité n'a été trouvée"
                                , error = Nothing
                                }

                      else
                        div [ class "flex flex-col gap-4" ]
                            [ model.selectNewTerritoires
                                |> MultiSelectRemote.viewCustom
                                    { isDisabled = False
                                    , selected = model.newTerritories
                                    , optionLabelFn =
                                        \{ name } ->
                                            name
                                    , optionDescriptionFn = \_ -> ""
                                    , optionsContainerMaxHeight = 300
                                    , selectTitle = text <| (\s -> s ++ "s") <| typeTerritoireToDisplay <| model.typeTerritoire
                                    , viewSelectedOptionFn = \_ -> text ""
                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                    , noResultsForMsg = \searchText -> "Aucun autre territoire n'a été trouvé pour " ++ searchText
                                    , noOptionsMsg = "Aucune entité n'a été trouvée"
                                    , newOption = Nothing
                                    }
                            , model.newTerritories
                                |> List.map (\territoire -> DSFR.Tag.deletable UnselectNewTerritoire { data = territoire, toString = .name })
                                |> DSFR.Tag.medium
                            ]
                    ]
                , div [ DSFR.Grid.col6 ]
                    [ p [ class "italic" ] [ text "" ]
                    ]
                , case model.saveTerritoireRequest of
                    RD.Loading ->
                        DSFR.Alert.small { title = Nothing, description = text "Création en cours" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.info
                            |> List.singleton
                            |> div [ class "flex justify-center items-center p-4 w-full" ]

                    RD.Failure _ ->
                        DSFR.Alert.small { title = Just "Création échouée", description = text "Assurez-vous qu'un territoire avec ce nom n'existe pas déjà." }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.error
                            |> List.singleton
                            |> div [ class "flex justify-center items-center p-4 w-full" ]

                    _ ->
                        nothing
                , Html.Extra.viewIf nomExistant <|
                    div [ DSFR.Grid.col12 ]
                        [ p []
                            [ text "Il existe déjà "
                            , text "un"
                            , text " territoire "
                            , text "avec ce nom"
                            ]
                        ]

                -- , Html.Extra.viewIf (nombreTerritoiresExistants > 0) <|
                --     div [ DSFR.Grid.col12 ]
                --         [ p []
                --             [ text "Il existe déjà "
                --             , if nombreTerritoiresExistants > 1 then
                --                 text <| String.fromInt nombreTerritoiresExistants
                --               else
                --                 text "un"
                --             , text " territoire"
                --             , text <| plural nombreTerritoiresExistants
                --             , text " qui couvre cette définition administrative\u{00A0}:"
                --             ]
                --         , ul [] <|
                --             List.map
                --                 (\t ->
                --                     t
                --                         |> Data.Territoire.nom
                --                         |> text
                --                         |> List.singleton
                --                         |> li
                --                             [ attributeIf (Data.Territoire.nom t == String.trim currentName) <| class "fr-text-default--error"
                --                             ]
                --                 )
                --             <|
                --                 territoiresExistants
                --         ]
                , div [ DSFR.Grid.col12 ]
                    [ footerUtilisateur nomExistant model
                    ]
                ]


footerUtilisateur : Bool -> Model -> Html Msg
footerUtilisateur nomExistant model =
    let
        valid =
            (model.nomTerritoire /= "")
                && not nomExistant
                && (List.length model.newTerritories
                        > (if model.groupement then
                            1

                           else
                            0
                          )
                   )

        ( isButtonDisabled, title_ ) =
            case ( model.saveTerritoireRequest, valid ) of
                ( RD.Loading, _ ) ->
                    ( True, "Enregistrer" )

                ( _, False ) ->
                    ( True, "Enregistrer" )

                _ ->
                    ( False, "Enregistrer" )

        buttons =
            case model.saveTerritoireRequest of
                RD.Success _ ->
                    [ DSFR.Button.new { onClick = Just CloseModal, label = "OK" }
                    ]

                _ ->
                    [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
                        |> DSFR.Button.submit
                        |> DSFR.Button.withAttrs [ title title_, Html.Attributes.name "submit-new-territoire" ]
                        |> DSFR.Button.withDisabled isButtonDisabled
                    , DSFR.Button.new { onClick = Just CloseModal, label = "Annuler" }
                        |> DSFR.Button.secondary
                    ]
    in
    DSFR.Button.group buttons
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


saveTerritoire : Model -> Cmd Msg
saveTerritoire { newTerritories, typeTerritoire, nomTerritoire } =
    let
        jsonBody =
            [ ( "name", Encode.string <| nomTerritoire )
            , ( "type", Encode.string <| typeTerritoireToString <| typeTerritoire )
            , ( "geographyIds", Encode.list Encode.string <| List.map .id newTerritories )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    Api.Auth.post
        { url = Api.adminTerritoires
        , body = jsonBody
        }
        SharedMsg
        ReceivedSaveTerritoire
        Data.Territoire.decodeTerritoire


isDone : Model -> Bool
isDone model =
    model.isDone


territoryCreated : Model -> Bool
territoryCreated model =
    model.territoryCreated


decodeNewTerritoire : Decoder NewTerritory
decodeNewTerritoire =
    Decode.succeed NewTerritory
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "name" Decode.string)


selectConfig : Type -> { headers : List a, url : String -> String, optionDecoder : Decoder (List NewTerritory) }
selectConfig typeTerritoire =
    { headers = []
    , url = Api.newTerritoireSearch (typeTerritoireToString typeTerritoire)
    , optionDecoder = Decode.list <| decodeNewTerritoire
    }
