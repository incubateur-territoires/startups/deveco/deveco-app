module Pages.Admin.Territoires exposing (Model, Msg, page)

import Accessibility exposing (Html, br, div, label, span, text)
import Api
import Api.Auth
import Api.EntityId
import DSFR.Button
import DSFR.CallOut
import DSFR.Grid
import DSFR.Icons exposing (iconMD)
import DSFR.Icons.System exposing (addLine, arrowDownLine, arrowUpLine)
import DSFR.SearchBar
import DSFR.Table
import Data.Territoire exposing (Territoire)
import Effect
import Html
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Html.Extra exposing (nothing)
import Json.Decode as Decode
import Lib.UI exposing (plural, withEmptyAs)
import List.Extra
import Pages.Admin.Territoires.AddModal as AddModal
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


type alias Model =
    { recherche : String
    , territoires : WebData (List Territoire)
    , sorting : Sorting
    , modalOpened : Bool
    , addModalModel : AddModal.Model
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | AddModalMsg AddModal.Msg
    | ReceivedTerritoires (WebData (List Territoire))
    | UpdatedRecherche String
    | OpenAddterritoireModal
    | SetSorting Header


type alias Sorting =
    ( Header, Direction )


type Direction
    = Ascending
    | Descending


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    model.addModalModel
        |> AddModal.subscriptions
        |> Sub.map AddModalMsg


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    let
        ( addModalModel, addModalEffect ) =
            AddModal.init
    in
    ( { recherche = ""
      , territoires = RD.NotAsked
      , modalOpened = False
      , sorting = ( HTerritoireNom, Ascending )
      , addModalModel = addModalModel
      }
    , Effect.batch
        [ getTerritoires
        , Effect.map AddModalMsg addModalEffect
        ]
    )
        |> Shared.pageChangeEffects


getTerritoires : Effect.Effect Shared.Msg Msg
getTerritoires =
    Api.Auth.get
        { url = Api.adminTerritoires
        }
        SharedMsg
        ReceivedTerritoires
        (Decode.list Data.Territoire.decodeTerritoire)
        |> Effect.fromCmd


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        AddModalMsg addModalMsg ->
            let
                ( newAddModalModel, addModalEffect ) =
                    AddModal.update addModalMsg model.addModalModel

                isDone =
                    AddModal.isDone newAddModalModel

                territoryCreated =
                    AddModal.territoryCreated newAddModalModel
            in
            ( { model
                | addModalModel = newAddModalModel
                , modalOpened = not isDone
              }
            , Effect.batch
                [ Effect.map AddModalMsg addModalEffect
                , if territoryCreated then
                    getTerritoires

                  else
                    Effect.none
                ]
            )

        ReceivedTerritoires response ->
            { model | territoires = response }
                |> Effect.withNone

        UpdatedRecherche recherche ->
            { model | recherche = recherche }
                |> Effect.withNone

        OpenAddterritoireModal ->
            let
                ( addModalModel, addModalCmd ) =
                    AddModal.init
            in
            { model
                | modalOpened = True
                , addModalModel = addModalModel
            }
                |> Effect.with (Effect.map AddModalMsg addModalCmd)

        SetSorting header ->
            let
                sorting =
                    case model.sorting of
                        ( h, Ascending ) ->
                            if h == header then
                                ( h, Descending )

                            else
                                ( header, Ascending )

                        ( h, Descending ) ->
                            if h == header then
                                ( h, Ascending )

                            else
                                ( header, Ascending )
            in
            { model | sorting = sorting }
                |> Effect.withNone


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "Territoires - Superadmin"
    , body = [ body model ]
    , route = Route.AdminTerritoires
    }


rechercherTerritoiresInputName : String
rechercherTerritoiresInputName =
    "recherches-territoires"


body : Model -> Html Msg
body model =
    let
        territoriesWithImportInProgress =
            model.territoires
                |> RD.withDefault []
                |> List.filter Data.Territoire.importInProgress
    in
    div [ DSFR.Grid.gridRow ]
        [ if model.modalOpened == True then
            Html.map AddModalMsg <|
                AddModal.view (RD.withDefault [] model.territoires) model.addModalModel

          else
            nothing
        , div [ DSFR.Grid.col12, class "p-4 fr-card--white" ]
            [ if List.length territoriesWithImportInProgress > 0 then
                div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col ]
                        [ DSFR.CallOut.callout Nothing (Just "Import de territoire en cours") <|
                            div []
                                [ span []
                                    [ text "Le"
                                    , text <| plural <| List.length territoriesWithImportInProgress
                                    , text " territoire"
                                    , text <| plural <| List.length territoriesWithImportInProgress
                                    , text " suivant"
                                    , text <| plural <| List.length territoriesWithImportInProgress
                                    , text " "
                                    , text <|
                                        if List.length territoriesWithImportInProgress > 1 then
                                            "sont"

                                        else
                                            "est"
                                    , text " actuellement en cours d'import\u{00A0}:"
                                    ]
                                , br []
                                , text <| String.join ", " <| List.map Data.Territoire.nom <| territoriesWithImportInProgress
                                ]
                        ]
                    ]

              else
                nothing
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col, class "flex flex-row gap-4 justify-end" ]
                    [ DSFR.Button.new { label = "Ajouter un territoire", onClick = Just OpenAddterritoireModal }
                        |> DSFR.Button.leftIcon addLine
                        |> DSFR.Button.view
                    ]
                ]
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ]
                [ div [ DSFR.Grid.col6, class "flex flex-col gap-2" ] <|
                    [ label
                        [ class "fr-label"
                        , Html.Attributes.for rechercherTerritoiresInputName
                        ]
                        [ text "Rechercher un territoire" ]
                    , DSFR.SearchBar.searchBar
                        { submitMsg = NoOp
                        , buttonLabel = "Rechercher"
                        , inputMsg = UpdatedRecherche
                        , inputLabel = "Rechercher un territoire"
                        , inputPlaceholder = Nothing
                        , inputId = rechercherTerritoiresInputName
                        , inputValue = model.recherche
                        , hints = []
                        , fullLabel = Nothing
                        }
                    ]
                ]
            , div [ DSFR.Grid.gridRow ]
                [ div [ DSFR.Grid.col ] <|
                    List.singleton <|
                        viewTerritoiresTable model.sorting model.recherche model.territoires
                ]
            ]
        ]


viewTerritoiresTable : Sorting -> String -> WebData (List Territoire) -> Html Msg
viewTerritoiresTable sorting recherche data =
    case data of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Chargement en cours"
                    ]
                ]

        RD.Failure _ ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Aucun utilisateur pour l'instant."
                    ]
                ]

        RD.Success territoires ->
            case territoires |> List.filter (filterTerritoires recherche) of
                [] ->
                    div [ class "text-center", DSFR.Grid.col ]
                        [ div [ class "fr-card--white p-20" ]
                            [ text "Aucune utilisateur correspondant."
                            ]
                        ]

                us ->
                    div [ DSFR.Grid.col12 ]
                        [ DSFR.Table.table
                            { id = "tableau-territoires"
                            , caption = text "Territoires"
                            , headers = headers
                            , rows = us |> sortWithSorting sorting
                            , toHeader =
                                \header ->
                                    let
                                        sortOnClick =
                                            if isSortable header then
                                                [ onClick <| SetSorting <| header, class "cursor-pointer" ]

                                            else
                                                []

                                        sortIcon =
                                            if isSortable header then
                                                sortingIcon sorting header

                                            else
                                                nothing
                                    in
                                    Html.span sortOnClick
                                        [ headerToString header |> text
                                        , sortIcon
                                        ]
                            , toRowId = Data.Territoire.id >> Api.EntityId.entityIdToString
                            , toCell = toCell
                            }
                            |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                            |> DSFR.Table.noBorders
                            |> DSFR.Table.captionHidden
                            |> DSFR.Table.fixed
                            |> DSFR.Table.view
                        ]


sortingIcon : ( Header, Direction ) -> Header -> Html msg
sortingIcon ( sortingHeader, direction ) header =
    if header == sortingHeader then
        case direction of
            Ascending ->
                iconMD arrowDownLine

            Descending ->
                iconMD arrowUpLine

    else
        nothing


sortWithSorting : ( Header, Direction ) -> List Territoire -> List Territoire
sortWithSorting ( header, direction ) =
    let
        sortWith =
            case header of
                HTerritoireNom ->
                    Data.Territoire.nom >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HTerritoireType ->
                    Data.Territoire.categorie >> Data.Territoire.categorieToDisplay >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HGeographieLibelle ->
                    Data.Territoire.geographieLibelle >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HGeographieType ->
                    Data.Territoire.type_ >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HImport ->
                    Data.Territoire.importInProgress
                        >> (\b ->
                                if b then
                                    "A"

                                else
                                    "Z"
                           )
    in
    List.Extra.stableSortWith
        (\a b ->
            case direction of
                Ascending ->
                    compare (sortWith a) (sortWith b)

                Descending ->
                    compare (sortWith b) (sortWith a)
        )


filterTerritoires : String -> Territoire -> Bool
filterTerritoires recherche territoire =
    let
        lowerRecherche =
            String.toLower recherche

        matches =
            String.toLower
                >> String.contains lowerRecherche

        matchRecherche =
            (lowerRecherche == "")
                || (matches <| Data.Territoire.nom territoire)
                || (matches <| Data.Territoire.geographieLibelle territoire)
                || (matches <| Data.Territoire.categorie territoire)
                || (matches <| Data.Territoire.typeToDisplay <| Data.Territoire.type_ <| territoire)
    in
    matchRecherche


type Header
    = HTerritoireNom
    | HTerritoireType
    | HGeographieLibelle
    | HGeographieType
    | HImport


headers : List Header
headers =
    [ HTerritoireNom
    , HTerritoireType
    , HGeographieLibelle
    , HGeographieType
    , HImport
    ]


isSortable : Header -> Bool
isSortable header =
    not <|
        List.member header []


headerToString : Header -> String
headerToString header =
    case header of
        HTerritoireNom ->
            "Territoire"

        HTerritoireType ->
            "Type"

        HGeographieLibelle ->
            "Libellé de la géographie"

        HGeographieType ->
            "Type de la géographie"

        HImport ->
            "Import en cours"


toCell : Header -> Territoire -> Html Msg
toCell header territoire =
    case header of
        HTerritoireNom ->
            territoire
                |> (Data.Territoire.nom >> withEmptyAs "-")
                |> text

        HTerritoireType ->
            territoire
                |> (Data.Territoire.categorie >> Data.Territoire.categorieToDisplay >> withEmptyAs "-")
                |> text

        HGeographieLibelle ->
            territoire
                |> Data.Territoire.geographieLibelle
                |> text

        HGeographieType ->
            territoire
                |> (Data.Territoire.type_ >> Data.Territoire.typeToDisplay)
                |> text

        HImport ->
            territoire
                |> Data.Territoire.importInProgress
                |> (\iip ->
                        if iip then
                            "En cours"

                        else
                            "Terminé"
                   )
                |> text
