module Pages.Etablissements exposing (Model, page)

import Accessibility exposing (Html, div, form, h1, span, sup, text)
import Api
import Api.Auth
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Modal
import DSFR.Pagination
import DSFR.Tag
import DSFR.Typography as Typo
import Data.GeoJsonProps exposing (GeoJsonProp)
import Effect
import Html exposing (input, p)
import Html.Attributes as Attr exposing (class)
import Html.Extra exposing (nothing, viewIf, viewMaybe)
import Http
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.UI exposing (formatNumberWithThousandSpacing)
import Lib.Variables exposing (hintActivites, hintMotsCles, hintZoneGeographique)
import MultiSelectRemote
import Pages.Etablissements.Filters as Filters exposing (Filters, Pagination)
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Entite
import UI.Etablissement exposing (EtablissementWithExtraInfo, Siret, decodeEtablissementsWithExtraInfo, etablissementCard)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.onNewFlags (Filters.queryToFiltersAndPagination >> DoSearch) <|
        Spa.Page.element <|
            { init = init
            , update = update
            , view = view
            , subscriptions = subscriptions
            }


type alias Model =
    { etablissements : WebData (List EtablissementWithExtraInfo)
    , etablissementsRequests : Int
    , batchRequest : Maybe BatchRequest
    , geoJsonExport : Maybe GeoJsonExport
    , filters : Filters.Model
    }


type alias GeoJsonExport =
    ( List GeoJsonProp, GeoJsonFormat )


type GeoJsonFormat
    = Regular



-- | Newline


geoJsonFormatToString : GeoJsonFormat -> String
geoJsonFormatToString geoJsonFormat =
    case geoJsonFormat of
        Regular ->
            "geojson"



-- Newline ->
--     "ndgeojson"


type alias BatchRequest =
    { request : WebData ( List EtablissementWithExtraInfo, Pagination, BatchResults )
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , activites : List String
    , localisations : List String
    , mots : List String
    }


type alias BatchResults =
    { total : Int
    , successes : Int
    , errors : List String
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | DoSearch ( Filters, Pagination )
    | ReceivedEtablissements (WebData ( List EtablissementWithExtraInfo, Pagination ))
    | FiltersMsg Filters.Msg
    | ToggleFavorite Siret Bool
    | GotFavoriteResult Siret Bool
    | ClickedQualifierFiches
    | CanceledQualifierFiches
    | ConfirmedQualifierFiches
    | ReceivedQualifierFiches (WebData ( List EtablissementWithExtraInfo, Pagination, BatchResults ))
    | SelectedBatchActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchActivites (MultiSelectRemote.Msg String)
    | UnselectBatchActivite String
    | SelectedBatchLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchLocalisations (MultiSelectRemote.Msg String)
    | UnselectBatchLocalisation String
    | SelectedBatchMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchMots (MultiSelectRemote.Msg String)
    | UnselectBatchMot String
    | ClickedExportGeoJson
    | ClickedCanceledGeoJson
    | ToggleGeoJsonProp GeoJsonProp Bool



-- | ChangeGeoJsonFormat GeoJsonFormat


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model.batchRequest
            |> Maybe.map .selectActivites
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.batchRequest
            |> Maybe.map .selectLocalisations
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.batchRequest
            |> Maybe.map .selectMots
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.filters
            |> Filters.subscriptions
            |> Sub.map FiltersMsg
        ]


init : Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init rawQuery =
    let
        etablissementsRequests =
            0

        ( initFilters, effectFilters ) =
            Filters.init rawQuery
    in
    ( { etablissements = RD.Loading
      , etablissementsRequests = etablissementsRequests
      , filters = initFilters
      , batchRequest = Nothing
      , geoJsonExport = Nothing
      }
    , Effect.map FiltersMsg effectFilters
    )
        |> Shared.pageChangeEffects


getEtablissementsTracker : String
getEtablissementsTracker =
    "get-etablissements-tracker"


getEtablissements : Int -> String -> Cmd Msg
getEtablissements trackerCount nextUrl =
    Api.Auth.request
        { method = "GET"
        , headers = []
        , url = Api.searchEtablissements <| nextUrl
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Just <| getEtablissementsTracker ++ "-" ++ String.fromInt trackerCount
        }
        SharedMsg
        ReceivedEtablissements
        decodePayload


sendBatchRequest : Filters -> Pagination -> BatchRequest -> Cmd Msg
sendBatchRequest filters pagination { activites, localisations, mots } =
    let
        jsonBody =
            [ ( "activites", Encode.list Encode.string activites )
            , ( "localisations", Encode.list Encode.string localisations )
            , ( "mots", Encode.list Encode.string mots )
            ]
                |> (Encode.object >> Http.jsonBody)
    in
    Api.Auth.post
        { url = Api.updateQualificationsEtablissements <| Filters.filtersAndPaginationToQuery ( filters, pagination )
        , body = jsonBody
        }
        SharedMsg
        ReceivedQualifierFiches
        decodeBatchResponse


decodeBatchResponse : Decode.Decoder ( List EtablissementWithExtraInfo, Pagination, BatchResults )
decodeBatchResponse =
    Decode.succeed (\( f, p ) b -> ( f, p, b ))
        |> andMap decodePayload
        |> andMap (Decode.field "batchResults" <| decodeBatchResults)


decodeBatchResults : Decode.Decoder BatchResults
decodeBatchResults =
    Decode.succeed BatchResults
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "successes" Decode.int)
        |> andMap (Decode.field "errors" <| Decode.list Decode.string)


decodePayload : Decode.Decoder ( List EtablissementWithExtraInfo, Pagination )
decodePayload =
    Decode.succeed (\p pages results data -> ( data, Pagination p pages results ))
        |> andMap (Decode.field "page" Decode.int)
        |> andMap (Decode.field "pages" Decode.int)
        |> andMap (Decode.field "results" Decode.int)
        |> andMap (Decode.field "data" decodeEtablissementsWithExtraInfo)


requestToggleFavorite : String -> Bool -> Cmd Msg
requestToggleFavorite siret favorite =
    Api.Auth.post
        { url = Api.toggleFavoriteEtablissement
        , body =
            [ ( "siret", Encode.string siret )
            , ( "favori", Encode.bool favorite )
            ]
                |> Encode.object
                |> Http.jsonBody
        }
        SharedMsg
        (\webdata ->
            GotFavoriteResult siret <|
                case webdata of
                    RD.Success _ ->
                        favorite

                    _ ->
                        not favorite
        )
        (Decode.succeed ())


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        FiltersMsg filtersMsg ->
            let
                ( filters, effectFilters ) =
                    Filters.update filtersMsg model.filters
            in
            { model | filters = filters }
                |> Effect.with (Effect.map FiltersMsg effectFilters)

        ReceivedEtablissements etablissements ->
            case etablissements of
                RD.Success ( fs, pagination ) ->
                    ( { model | etablissements = RD.Success fs, filters = Filters.forcePagination pagination model.filters }
                    , Effect.none
                    )

                _ ->
                    ( { model | etablissements = etablissements |> RD.map Tuple.first }
                    , Effect.none
                    )

        DoSearch ( filters, pagination ) ->
            let
                ( filtersModel, effectFilters ) =
                    Filters.doSearch ( filters, pagination ) model.filters
            in
            ( { model
                | filters = filtersModel
                , etablissements = RD.Loading
                , etablissementsRequests = model.etablissementsRequests + 1
              }
            , Effect.batch
                [ Effect.fromCmd (getEtablissements (model.etablissementsRequests + 1) <| Filters.filtersAndPaginationToQuery ( Filters.getFilters filtersModel, Filters.getPagination filtersModel ))
                , Effect.fromCmd (Http.cancel <| getEtablissementsTracker ++ "-" ++ String.fromInt model.etablissementsRequests)
                , Effect.fromShared <| Shared.ScrollIntoView Filters.listFiltersTagsId
                , Effect.map FiltersMsg effectFilters
                ]
            )

        ToggleFavorite siret add ->
            { model
                | etablissements =
                    model.etablissements
                        |> RD.map (List.map (updateEtablissementInList siret add))
            }
                |> Effect.withCmd (requestToggleFavorite siret add)

        GotFavoriteResult siret add ->
            { model
                | etablissements =
                    model.etablissements
                        |> RD.map (List.map (updateEtablissementInList siret add))
            }
                |> Effect.withNone

        ClickedQualifierFiches ->
            { model
                | batchRequest = initBatchRequest
            }
                |> Effect.withNone

        CanceledQualifierFiches ->
            { model | batchRequest = Nothing }
                |> Effect.withNone

        ConfirmedQualifierFiches ->
            case model.batchRequest of
                Nothing ->
                    model
                        |> Effect.withNone

                Just br ->
                    { model
                        | batchRequest =
                            Just { br | request = RD.Loading }
                    }
                        |> Effect.withCmd (sendBatchRequest (Filters.getFilters model.filters) (Filters.getPagination model.filters) br)

        ReceivedQualifierFiches response ->
            let
                ( etablissements, filters, cmd ) =
                    case response of
                        RD.Success ( f, p, _ ) ->
                            ( RD.Success f, Filters.forcePagination p model.filters, Cmd.map FiltersMsg Filters.getFiltersSource )

                        _ ->
                            ( model.etablissements, model.filters, Cmd.none )
            in
            { model
                | batchRequest =
                    model.batchRequest
                        |> Maybe.map (\br -> { br | request = response })
                , filters = filters
                , etablissements = etablissements
            }
                |> Effect.withCmd cmd

        SelectedBatchActivites ( activites, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectActivitesConfig br.selectActivites

                        activitesUniques =
                            case activites of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        activites
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br | selectActivites = updatedSelect, activites = activitesUniques }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchActivites sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectActivitesConfig br.selectActivites
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br | selectActivites = updatedSelect }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchActivite activite ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | activites =
                                        br.activites
                                            |> List.filter ((/=) activite)
                                }
                      }
                    , Effect.none
                    )

        SelectedBatchLocalisations ( localisations, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectLocalisationsConfig br.selectLocalisations

                        localisationsUniques =
                            case localisations of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        localisations
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | localisations = localisationsUniques
                                    , selectLocalisations = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchLocalisations sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectLocalisationsConfig br.selectLocalisations
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | selectLocalisations = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchLocalisation localisation ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | localisations =
                                        br.localisations
                                            |> List.filter ((/=) localisation)
                                }
                      }
                    , Effect.none
                    )

        SelectedBatchMots ( mots, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectMotsConfig br.selectMots

                        motsUniques =
                            case mots of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        mots
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | mots = motsUniques
                                    , selectMots = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchMots sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectMotsConfig br.selectMots
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | selectMots = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchMot mot ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | mots =
                                        br.mots
                                            |> List.filter ((/=) mot)
                                }
                      }
                    , Effect.none
                    )

        ClickedExportGeoJson ->
            { model | geoJsonExport = Just ( [], Regular ) }
                |> Effect.withNone

        ClickedCanceledGeoJson ->
            { model | geoJsonExport = Nothing }
                |> Effect.withNone

        -- ChangeGeoJsonFormat geoJsonFormat ->
        --     { model
        --         | geoJsonExport =
        --             model.geoJsonExport
        --                 |> Maybe.map
        --                     (\( list, _ ) ->
        --                         ( list, geoJsonFormat )
        --                     )
        --     }
        --         |> Effect.withNone
        ToggleGeoJsonProp prop bool ->
            { model
                | geoJsonExport =
                    model.geoJsonExport
                        |> Maybe.map
                            (\( list, format ) ->
                                if bool then
                                    if List.member prop list then
                                        ( list, format )

                                    else
                                        ( prop :: list, format )

                                else
                                    ( list |> List.filter (\e -> e /= prop), format )
                            )
            }
                |> Effect.withNone


initBatchRequest : Maybe BatchRequest
initBatchRequest =
    Just
        { request = RD.NotAsked
        , selectActivites = initSelectActivites
        , selectLocalisations = initSelectLocalisations
        , selectMots = initSelectMots
        , activites = []
        , localisations = []
        , mots = []
        }


updateEtablissementInList : String -> Bool -> EtablissementWithExtraInfo -> EtablissementWithExtraInfo
updateEtablissementInList siret add etablissementWithExtraInfos =
    if etablissementWithExtraInfos.etablissement.siret == siret then
        { etablissementWithExtraInfos
            | infos =
                etablissementWithExtraInfos.infos
                    |> (\infos ->
                            { infos
                                | favori = add
                            }
                       )
        }

    else
        etablissementWithExtraInfos


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Établissements"
    , body = UI.Layout.lazyBody body model
    , route =
        Route.Etablissements <|
            Just <|
                Filters.filtersAndPaginationToQuery ( Filters.getFilters model.filters, Filters.getPagination model.filters )
    }


body : Model -> Html Msg
body model =
    case ( Filters.showingUnfilteredResults (Filters.getCurrentUrl model.filters), model.etablissements ) of
        ( True, RD.Success [] ) ->
            div [ class "fr-card--white text-center p-20" ]
                [ div [ Grid.gridRow ]
                    [ text "Aucun établissement trouvé. C'est sans doute une erreur de notre côté, veuillez nous contacter\u{00A0}!"
                    ]
                ]

        _ ->
            div [ class "flex flex-col gap-4 py-4" ]
                [ viewMaybe (viewBatchRequestModal (Filters.getPagination model.filters |> .results)) model.batchRequest
                , viewMaybe (viewGeoJsonModal (Filters.getCurrentUrl model.filters)) model.geoJsonExport
                , div [ class "flex flex-col gap-4 lg:flex-row" ]
                    [ h1 [ class "fr-h6 !mb-0" ]
                        [ DSFR.Icons.iconLG UI.Entite.iconeEtablissement
                        , text "\u{00A0}"
                        , text "Établissements"
                        ]
                    , div [ class "lg:ml-auto" ]
                        [ DSFR.Button.new { onClick = Nothing, label = "Ajouter un établissement exogène au territoire" }
                            |> DSFR.Button.linkButton (Route.toUrl <| Route.EtablissementNew)
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                            |> DSFR.Button.view
                        ]
                    ]
                , Html.map FiltersMsg <| Filters.filterPanel model.filters
                , viewEtablissementsList (Filters.getPagination model.filters) (Filters.getFilters model.filters) model.etablissements (Filters.getCurrentUrl model.filters)
                ]


viewSelection : WebData (List EtablissementWithExtraInfo) -> Filters -> Pagination -> String -> Html Msg
viewSelection request filters pagination currentUrl =
    let
        results =
            pagination.results
    in
    div [ class "flex flex-col gap-4 mb-4" ]
        [ div [ class "flex flex-col gap-4 lg:flex-row lg:items-center" ]
            [ div [ class "font-bold" ] <|
                case request of
                    RD.Loading ->
                        [ greyPlaceholder 20 ]

                    _ ->
                        [ text (formatNumberWithThousandSpacing pagination.results)
                        , text " résultat"
                        , viewIf (pagination.results > 1) (text "s")
                        ]
            , div [ class "flex flex-col gap-2 items-start lg:flex-row lg:ml-auto" ]
                [ div [ class "flex flex-row gap-2" ]
                    [ DSFR.Button.new
                        { label = "Qualifier"
                        , onClick = Just ClickedQualifierFiches
                        }
                        |> DSFR.Button.secondary
                        |> DSFR.Button.leftIcon DSFR.Icons.Design.editLine
                        |> DSFR.Button.withDisabled (results == 0)
                        |> DSFR.Button.view
                    , form
                        [ Attr.action <|
                            (Api.getExportEtablissements <|
                                currentUrl
                            )
                                ++ "&format=xlsx"
                        , Attr.method "POST"
                        , Attr.target "_blank"
                        ]
                        [ input
                            [ Attr.type_ "hidden"
                            , Attr.name "ids"
                            , Attr.value ""
                            ]
                            []
                        , DSFR.Button.new { onClick = Nothing, label = "Exporter (Excel)" }
                            |> DSFR.Button.submit
                            |> DSFR.Button.secondary
                            |> DSFR.Button.withDisabled (results == 0)
                            |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                            |> DSFR.Button.view
                        ]
                    , DSFR.Button.new
                        { label = "Exporter (GeoJSON)"
                        , onClick = Just <| ClickedExportGeoJson
                        }
                        |> DSFR.Button.secondary
                        |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                        |> DSFR.Button.withDisabled (results == 0)
                        |> DSFR.Button.view
                    ]
                ]
            ]
        , div [ class "lg:ml-auto flex flex-row items-baseline gap-4" ] <|
            case request of
                RD.Loading ->
                    [ greyPlaceholder 20 ]

                _ ->
                    [ text "Tri"
                    , div
                        [ class "fr-select-group"
                        ]
                        [ Html.map FiltersMsg <| Filters.selectOrder filters
                        ]
                    ]
        ]


viewEtablissementsList : Pagination -> Filters -> WebData (List EtablissementWithExtraInfo) -> String -> Html Msg
viewEtablissementsList pagination filters etablissements currentUrl =
    case etablissements of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ Grid.col12 ]
                [ viewSelection etablissements filters pagination currentUrl
                , div [ Grid.gridRow ] <|
                    List.repeat 3 <|
                        cardPlaceholder
                ]

        RD.Failure _ ->
            div [ class "text-center" ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ class "fr-card--white h-[250px] text-center p-20" ]
                [ text "Aucun établissement ne correspond à cette recherche."
                , text " "
                , viewMaybe (\_ -> text "Moteur de recherche par adresse en cours de perfectionnement\u{00A0}: certaines adresses peuvent ne pas être trouvées.") filters.rue
                ]

        RD.Success fs ->
            div [ Attr.id "list-etablissements" ]
                [ viewSelection etablissements filters pagination currentUrl
                , fs
                    |> List.map (\{ etablissement, infos } -> etablissementCard { toggleFavorite = Just ToggleFavorite } etablissement infos False)
                    |> div [ Grid.gridRow ]
                , if pagination.pages > 1 then
                    div [ class "!mt-4" ]
                        [ DSFR.Pagination.view pagination.page pagination.pages <|
                            Filters.toHref ( filters, pagination )
                        ]

                  else
                    nothing
                ]


viewBatchRequestModal : Int -> BatchRequest -> Html Msg
viewBatchRequestModal foundFiches { request, selectLocalisations, selectActivites, selectMots, activites, localisations, mots } =
    DSFR.Modal.view
        { id = "batch-tag"
        , label = "batch-tag"
        , openMsg = NoOp
        , closeMsg = Just <| CanceledQualifierFiches
        , title =
            text <|
                "Qualifier "
                    ++ formatNumberWithThousandSpacing foundFiches
                    ++ " fiche"
                    ++ (if foundFiches > 1 then
                            "s"

                        else
                            ""
                       )
        , opened = True
        }
        (let
            noTags =
                (List.length activites == 0)
                    && (List.length mots == 0)
                    && (List.length localisations == 0)

            disabled =
                request == RD.Loading
         in
         div []
            [ div [ Grid.gridRow, Grid.gridRowGutters ]
                [ div [ Grid.col12 ]
                    [ case request of
                        RD.Success ( _, _, results ) ->
                            div [ class "flex flex-col gap-2" ]
                                [ viewIf (List.length activites > 0) <|
                                    div []
                                        [ div [ Typo.textBold ]
                                            [ text "Activité(s) ajoutée(s)\u{00A0}:"
                                            ]
                                        , div []
                                            [ text <| String.join ", " activites
                                            ]
                                        ]
                                , viewIf (List.length localisations > 0) <|
                                    div []
                                        [ div [ Typo.textBold ]
                                            [ text "Localisation(s) ajoutée(s)\u{00A0}:"
                                            ]
                                        , div []
                                            [ text <| String.join ", " localisations
                                            ]
                                        ]
                                , viewIf (List.length mots > 0) <|
                                    div []
                                        [ div [ Typo.textBold ]
                                            [ text "Mot(s)-clé(s) ajouté(s)\u{00A0}:"
                                            ]
                                        , div []
                                            [ text <| String.join ", " mots
                                            ]
                                        ]
                                , div [ class "flex flex-col gap-4" ]
                                    [ div [ Typo.textBold ]
                                        [ text "Résultat de la mise à jour\u{00A0}:"
                                        ]
                                    , let
                                        { total, successes, errors } =
                                            results
                                      in
                                      div [] <|
                                        [ if total == successes then
                                            div [ class "fr-text-default--success" ] [ text "Les ", text <| formatNumberWithThousandSpacing successes, text " fiches ont été qualifiées avec succès\u{00A0}!" ]

                                          else if successes > 0 then
                                            div [ class "fr-text-default--success" ] [ text <| formatNumberWithThousandSpacing <| successes, text " fiches ont été qualifiées avec succès." ]

                                          else
                                            nothing
                                        , viewIf (List.length errors > 0) <|
                                            div [ class "fr-text-default--error" ]
                                                [ text "Les fiches suivantes n'ont pas été qualifiées correctement\u{00A0}:"
                                                ]
                                        , viewIf (List.length errors > 0) <|
                                            div [ Grid.gridRow ] <|
                                                List.map
                                                    (\ficheName ->
                                                        div [ Grid.col6, class "fr-text-default--error" ]
                                                            [ DSFR.Icons.icon DSFR.Icons.System.closeCircleFill
                                                            , text " "
                                                            , text ficheName
                                                            ]
                                                    )
                                                <|
                                                    errors
                                        ]
                                    ]
                                ]

                        RD.Failure err ->
                            text <|
                                case err of
                                    Http.BadBody cause ->
                                        cause

                                    _ ->
                                        ""

                        _ ->
                            div []
                                [ div [ Grid.gridRow, Grid.gridRowGutters ]
                                    [ div [ Grid.col6 ]
                                        [ div [ class "flex flex-col gap-4" ]
                                            [ selectActivites
                                                |> MultiSelectRemote.viewCustom
                                                    { isDisabled = False
                                                    , selected = activites
                                                    , optionLabelFn = identity
                                                    , optionDescriptionFn = \_ -> ""
                                                    , optionsContainerMaxHeight = 300
                                                    , selectTitle = span [ Typo.textBold ] [ text "Activités réelles et filières", sup [ Attr.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                    , viewSelectedOptionFn = text
                                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                    , noResultsForMsg = \searchText -> "Aucune autre activité n'a été trouvée pour " ++ searchText
                                                    , noOptionsMsg = "Aucune activité n'a été trouvée"
                                                    , newOption = Just identity
                                                    }
                                            , case activites of
                                                [] ->
                                                    nothing

                                                _ ->
                                                    activites
                                                        |> List.map (\activite -> DSFR.Tag.deletable UnselectBatchActivite { data = activite, toString = identity })
                                                        |> DSFR.Tag.medium
                                            ]
                                        ]
                                    , div [ Grid.col6 ]
                                        [ div [ class "flex flex-col gap-4" ]
                                            [ selectLocalisations
                                                |> MultiSelectRemote.viewCustom
                                                    { isDisabled = False
                                                    , selected = localisations
                                                    , optionLabelFn = identity
                                                    , optionDescriptionFn = \_ -> ""
                                                    , optionsContainerMaxHeight = 300
                                                    , selectTitle = span [ Typo.textBold ] [ text "Zone géographique", sup [ Attr.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                    , viewSelectedOptionFn = text
                                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                    , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                                                    , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                                                    , newOption = Just identity
                                                    }
                                            , case localisations of
                                                [] ->
                                                    nothing

                                                _ ->
                                                    localisations
                                                        |> List.map (\localisation -> DSFR.Tag.deletable UnselectBatchLocalisation { data = localisation, toString = identity })
                                                        |> DSFR.Tag.medium
                                            ]
                                        ]
                                    ]
                                , div [ Grid.gridRow, Grid.gridRowGutters ]
                                    [ div [ Grid.col6 ]
                                        [ div [ class "flex flex-col gap-4" ]
                                            [ selectMots
                                                |> MultiSelectRemote.viewCustom
                                                    { isDisabled = False
                                                    , selected = mots
                                                    , optionLabelFn = identity
                                                    , optionDescriptionFn = \_ -> ""
                                                    , optionsContainerMaxHeight = 300
                                                    , selectTitle = span [ Typo.textBold ] [ text "Mots-clés", sup [ Attr.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                    , viewSelectedOptionFn = text
                                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                    , noResultsForMsg = \searchText -> "Aucun autre mot-clé n'a été trouvé pour " ++ searchText
                                                    , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                                                    , newOption = Just identity
                                                    }
                                            , case mots of
                                                [] ->
                                                    nothing

                                                _ ->
                                                    mots
                                                        |> List.map (\mot -> DSFR.Tag.deletable UnselectBatchMot { data = mot, toString = identity })
                                                        |> DSFR.Tag.medium
                                            ]
                                        ]
                                    ]
                                ]
                    ]
                ]
            , div [ Grid.gridRow, Grid.gridRowGutters ]
                [ div [ Grid.col12 ]
                    [ (case request of
                        RD.Success _ ->
                            [ DSFR.Button.new
                                { onClick = Just <| CanceledQualifierFiches
                                , label = "OK"
                                }
                            ]

                        _ ->
                            [ DSFR.Button.new
                                { onClick = Just <| ConfirmedQualifierFiches
                                , label =
                                    if request == RD.Loading then
                                        "Qualification en cours..."

                                    else
                                        "Confirmer"
                                }
                                |> DSFR.Button.withDisabled (disabled || noTags)
                            , DSFR.Button.new { onClick = Just <| CanceledQualifierFiches, label = "Annuler" }
                                |> DSFR.Button.withDisabled disabled
                                |> DSFR.Button.secondary
                            ]
                      )
                        |> DSFR.Button.group
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRightInverted
                        |> DSFR.Button.viewGroup
                    ]
                ]
            ]
        )
        Nothing
        |> Tuple.first


viewGeoJsonModal : String -> GeoJsonExport -> Html Msg
viewGeoJsonModal currentUrl geoJsonExport =
    DSFR.Modal.view
        { id = "geojson-export"
        , label = "geojson-export"
        , openMsg = NoOp
        , closeMsg = Just <| ClickedCanceledGeoJson
        , title =
            text ""
        , opened = True
        }
        (viewGeoJsonModalBody currentUrl geoJsonExport)
        Nothing
        |> Tuple.first


viewGeoJsonModalBody : String -> GeoJsonExport -> Html Msg
viewGeoJsonModalBody currentUrl geoJsonExport =
    div []
        [ div [ Grid.gridRow, Grid.gridRowGutters ]
            [ div [ Grid.col12, class "flex flex-col gap-8" ]
                [ p [ class "!mb-0" ]
                    [ text "Le fichier GeoJSON contient le nom, le SIRET, et la géolocalisation de l'établissement, quand elle est disponible."
                    ]
                , p [ class "!mb-0" ]
                    [ text "Si vous le souhaitez, vous pouvez ajouter des informations à votre fichier GeoJSON."
                    ]
                , DSFR.Checkbox.group
                    { id = "geojson-props"
                    , label = span [ Typo.textBold ] [ text "Données supplémentaires" ]
                    , onChecked = ToggleGeoJsonProp
                    , values = Data.GeoJsonProps.availableGeoJsonProps
                    , checked = Tuple.first geoJsonExport
                    , valueAsString = Data.GeoJsonProps.geoJsonPropToString
                    , toId = Data.GeoJsonProps.geoJsonPropToString
                    , toLabel = Data.GeoJsonProps.geoJsonPropToDisplay
                    }
                    |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
                    |> DSFR.Checkbox.viewGroup
                , p [ class "!mb-0" ]
                    [ text "Vous pouvez utiliser le fichier téléchargé pour visualiser les établissements sur une carte grâce à des services en ligne comme "
                    , Typo.externalLink "https://umap.openstreetmap.fr/fr/map/new/" [] [ text "https://umap.openstreetmap.fr/fr/map/new/" ]
                    , text "."
                    ]
                ]
            ]
        , div [ Grid.gridRow, Grid.gridRowGutters ]
            [ div [ Grid.col12 ]
                [ div [ class "flex flex-row justify-end p-4" ]
                    [ form
                        [ Attr.action <|
                            (Api.getExportEtablissements <|
                                currentUrl
                            )
                                ++ "&format="
                                ++ (geoJsonFormatToString <| Tuple.second <| geoJsonExport)
                        , Attr.method "POST"
                        , Attr.target "_blank"
                        ]
                        [ input
                            [ Attr.type_ "hidden"
                            , Attr.name "props"
                            , Attr.value <| String.join "," <| List.map Data.GeoJsonProps.geoJsonPropToCode <| Tuple.first <| geoJsonExport
                            ]
                            []
                        , DSFR.Button.new { onClick = Nothing, label = "Exporter" }
                            |> DSFR.Button.submit
                            |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                            |> DSFR.Button.withAttrs [ class "!w-full" ]
                            |> DSFR.Button.view
                        ]
                    ]
                ]
            ]
        ]


cardPlaceholder : Html Msg
cardPlaceholder =
    div [ Grid.col12, Grid.colSm6 ]
        [ div [ class "p-2", Typo.textSm ]
            [ div
                [ class "fr-card--white flex flex-col p-4 custom-hover overflow-y-auto gap-2"
                , Attr.style "background-image" "none"
                ]
                [ div [ class "flex flex-row justify-between" ]
                    [ greyBadge 30
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 14
                    , greyPlaceholder 20
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                ]
            ]
        ]


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "grey-background m-1" ]
        |> List.singleton
        |> div [ class "pulse-black" ]


greyBadge : Int -> Html msg
greyBadge length =
    "\u{00A0}"
        |> List.repeat length
        |> String.join ""
        |> (\t -> { data = t, toString = identity })
        |> DSFR.Tag.unclickable
        |> List.singleton
        |> DSFR.Tag.medium
        |> List.singleton
        |> div [ class "pulse-black" ]


initSelectActivites : MultiSelectRemote.SmartSelect Msg String
initSelectActivites =
    MultiSelectRemote.init "select-activites"
        { selectionMsg = SelectedBatchActivites
        , internalMsg = UpdatedSelectBatchActivites
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


initSelectLocalisations : MultiSelectRemote.SmartSelect Msg String
initSelectLocalisations =
    MultiSelectRemote.init "select-zones"
        { selectionMsg = SelectedBatchLocalisations
        , internalMsg = UpdatedSelectBatchLocalisations
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


initSelectMots : MultiSelectRemote.SmartSelect Msg String
initSelectMots =
    MultiSelectRemote.init "select-mots"
        { selectionMsg = SelectedBatchMots
        , internalMsg = UpdatedSelectBatchMots
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


selectActivitesConfig : MultiSelectRemote.SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActivite
    , optionDecoder = Decode.list <| Decode.field "activite" Decode.string
    }


selectLocalisationsConfig : MultiSelectRemote.SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = Decode.list <| Decode.field "localisation" Decode.string
    }


selectMotsConfig : MultiSelectRemote.SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = Decode.list <| Decode.field "motCle" Decode.string
    }


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400
