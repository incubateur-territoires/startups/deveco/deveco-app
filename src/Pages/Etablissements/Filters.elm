module Pages.Etablissements.Filters exposing (Filters, Model, Msg, Pagination, doSearch, filterPanel, filtersAndPaginationToQuery, forcePagination, getCurrentUrl, getFilters, getFiltersSource, getPagination, init, listFiltersTagsId, makeQueryForPortefeuilleMode, queryToFiltersAndPagination, selectOrder, showingUnfilteredResults, subscriptions, toHref, update)

import Accessibility exposing (Html, div, h2, label, option, select, span, sup, text)
import Api
import Api.Auth
import DSFR.Accordion
import DSFR.Button
import DSFR.Checkbox
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Input
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiStreet, decodeApiStreet)
import Data.Commune exposing (Commune)
import Data.Demande exposing (TypeDemande)
import Data.Effectifs exposing (Effectifs)
import Data.Etablissement exposing (EtatAdministratif(..), etatAdministratifToDisplay, etatAdministratifToString)
import Data.FormeJuridique
import Data.Naf exposing (CodeNaf)
import Data.ZRR exposing (ZRR)
import Effect
import Html.Attributes as Attr exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf, viewMaybe)
import Html.Lazy
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap)
import Lib.UI exposing (formatNumberWithThousandSpacing)
import Lib.Variables exposing (hintESS, hintSuiviPortefeuille, hintSuiviTous)
import MultiSelect
import MultiSelectRemote
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelectRemote


type Msg
    = NoOp
    | ReceivedFiltersSource (WebData FiltersSource)
    | UpdatedSearch String
    | ClearAllFilters
    | SetEtatFilter Bool EtatAdministratif
    | SetSuiviFilter Suivi
    | SetFormesJuridiquesFilter Bool
    | SetTerritorialiteFilter Bool Territorialite
    | SelectedCodesNaf ( List CodeNaf, MultiSelectRemote.Msg CodeNaf )
    | UpdatedSelectCodesNaf (MultiSelectRemote.Msg CodeNaf)
    | UnselectedCodeNaf CodeNaf
    | ReceivedCommunes (WebData (List Commune))
    | SelectedCommunes ( List Commune, MultiSelect.Msg Commune )
    | UpdatedSelectCommunes (MultiSelect.Msg Commune)
    | UnselectCommune Commune
    | ReceivedEpcis (WebData (List Epci))
    | SelectedEpcis ( List Epci, MultiSelect.Msg Epci )
    | UpdatedSelectEpcis (MultiSelect.Msg Epci)
    | UnselectEpci Epci
    | SelectedEffectifs ( List Effectifs, MultiSelect.Msg Effectifs )
    | UpdatedSelectEffectifs (MultiSelect.Msg Effectifs)
    | UnselectedEffectifs Effectifs
    | SelectedRue ( ApiStreet, SingleSelectRemote.Msg ApiStreet )
    | UpdatedSelectRue (SingleSelectRemote.Msg ApiStreet)
    | ClearRue
    | SelectedActivites ( List String, MultiSelect.Msg String )
    | UpdatedSelectActivites (MultiSelect.Msg String)
    | UnselectActivite String
    | SelectedDemandes ( List TypeDemande, MultiSelect.Msg TypeDemande )
    | UpdatedSelectDemandes (MultiSelect.Msg TypeDemande)
    | UnselectDemande TypeDemande
    | SelectedZones ( List String, MultiSelect.Msg String )
    | UpdatedSelectZones (MultiSelect.Msg String)
    | UnselectZone String
    | SelectedMots ( List String, MultiSelect.Msg String )
    | UpdatedSelectMots (MultiSelect.Msg String)
    | UnselectMot String
    | SelectedQpvs ( List Qpv, MultiSelect.Msg Qpv )
    | UpdatedSelectQpvs (MultiSelect.Msg Qpv)
    | UnselectQpv Qpv
    | SelectedZonages ( List Zonage, MultiSelect.Msg Zonage )
    | UpdatedSelectZonages (MultiSelect.Msg Zonage)
    | UnselectZonage Zonage
    | SelectedZRR ( List ZRR, MultiSelect.Msg ZRR )
    | UpdatedSelectZRR (MultiSelect.Msg ZRR)
    | UnselectedZRR ZRR
    | SetESSFilter Bool Ess
    | ClickedSearch
    | UpdateUrl ( Filters, Pagination )
    | ClickedOrderBy OrderBy OrderDirection
    | SharedMsg Shared.Msg


init : Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init rawQuery =
    let
        ( filters, pagination ) =
            queryToFiltersAndPagination rawQuery

        effectifsIds =
            filters.effectifs
                |> List.map .id

        newEffectifs =
            Data.Effectifs.tranchesEffectifs
                |> List.filter (\{ id } -> List.member id effectifsIds)

        zrrsIds =
            filters.zrrs
                |> List.map .id

        newZrrs =
            Data.ZRR.zrrs
                |> List.filter (\{ id } -> List.member id zrrsIds)

        apiStreet =
            filters.rue |> Maybe.map .label |> Maybe.withDefault ""

        ( selectRue, selectRueCmd ) =
            SingleSelectRemote.init selectRueId
                { selectionMsg = SelectedRue
                , internalMsg = UpdatedSelectRue
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
                |> SingleSelectRemote.setText apiStreet selectRueConfig

        nextUrl =
            filtersAndPaginationToQuery ( filters, pagination )
    in
    ( { filters = { filters | effectifs = newEffectifs, zrrs = newZrrs }
      , filtersSource = RD.Loading
      , selectCodesNaf =
            MultiSelectRemote.init selectCodesNafId
                { selectionMsg = SelectedCodesNaf
                , internalMsg = UpdatedSelectCodesNaf
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectCommunes =
            MultiSelect.init "select-commune"
                { selectionMsg = SelectedCommunes
                , internalMsg = UpdatedSelectCommunes
                }
      , selectEpcis =
            MultiSelect.init "select-epci"
                { selectionMsg = SelectedEpcis
                , internalMsg = UpdatedSelectEpcis
                }
      , selectEffectifs =
            MultiSelect.init "select-effectifs"
                { selectionMsg = SelectedEffectifs
                , internalMsg = UpdatedSelectEffectifs
                }
      , selectRue = selectRue
      , selectActivites =
            MultiSelect.init selectActivitesId
                { selectionMsg = SelectedActivites
                , internalMsg = UpdatedSelectActivites
                }
      , selectDemandes =
            MultiSelect.init selectDemandesId
                { selectionMsg = SelectedDemandes
                , internalMsg = UpdatedSelectDemandes
                }
      , selectZones =
            MultiSelect.init selectZonesId
                { selectionMsg = SelectedZones
                , internalMsg = UpdatedSelectZones
                }
      , selectMots =
            MultiSelect.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                }
      , selectQpvs =
            MultiSelect.init selectQpvsId
                { selectionMsg = SelectedQpvs
                , internalMsg = UpdatedSelectQpvs
                }
      , selectZrrs =
            MultiSelect.init "select-zrr"
                { selectionMsg = SelectedZRR
                , internalMsg = UpdatedSelectZRR
                }
      , selectZonages =
            MultiSelect.init selectZonagesId
                { selectionMsg = SelectedZonages
                , internalMsg = UpdatedSelectZonages
                }
      , pagination = pagination
      , currentUrl = nextUrl
      , communes = RD.Loading
      , epcis = RD.Loading
      }
    , Effect.batch <|
        [ Effect.fromCmd <| getFiltersSource
        , Effect.fromCmd <| getCommunes
        , Effect.fromCmd <| getEpcis
        , Effect.fromCmd selectRueCmd
        , Effect.fromShared <| Shared.ReplaceUrl <| Route.Etablissements <| Just <| nextUrl
        ]
    )


type alias Model =
    { filters : Filters
    , filtersSource : WebData FiltersSource
    , selectCodesNaf : MultiSelectRemote.SmartSelect Msg CodeNaf
    , selectCommunes : MultiSelect.SmartSelect Msg Commune
    , selectEpcis : MultiSelect.SmartSelect Msg Epci
    , selectEffectifs : MultiSelect.SmartSelect Msg Effectifs
    , selectRue : SingleSelectRemote.SmartSelect Msg ApiStreet
    , selectActivites : MultiSelect.SmartSelect Msg String
    , selectDemandes : MultiSelect.SmartSelect Msg TypeDemande
    , selectZones : MultiSelect.SmartSelect Msg String
    , selectMots : MultiSelect.SmartSelect Msg String
    , selectQpvs : MultiSelect.SmartSelect Msg Qpv
    , selectZonages : MultiSelect.SmartSelect Msg Zonage
    , selectZrrs : MultiSelect.SmartSelect Msg ZRR
    , pagination : Pagination
    , currentUrl : String
    , communes : WebData (List Commune)
    , epcis : WebData (List Epci)
    }


type alias Epci =
    { id : String
    , label : String
    }


type alias Filters =
    { recherche : String
    , etat : List EtatAdministratif
    , codesNaf : List CodeNaf
    , formesJuridiques : FormesJuridiques
    , communes : List Commune
    , epcis : List Epci
    , effectifs : List Effectifs
    , rue : Maybe ApiStreet
    , suivi : Suivi
    , activites : List String
    , demandes : List TypeDemande
    , zones : List String
    , mots : List String
    , orderBy : OrderBy
    , orderDirection : OrderDirection
    , territorialite : List Territorialite
    , qpvs : List Qpv
    , zonages : List Zonage
    , zrrs : List ZRR
    , ess : List Ess
    }


type alias Qpv =
    { id : Int
    , nom : String
    }


type alias Zonage =
    { id : Int
    , nom : String
    }


type FormesJuridiques
    = Courantes
    | Toutes


type Suivi
    = Tout
    | Portefeuille
    | Favoris
    | Accompagnes


type OrderBy
    = EtablissementConsultation
    | EtablissementCreation



-- | Alphabetical


orderByToString : OrderBy -> String
orderByToString orderBy =
    case orderBy of
        -- Alphabetical ->
        --     "AZ"
        --
        EtablissementConsultation ->
            "CO"

        EtablissementCreation ->
            "DC"


stringToOrderBy : String -> Maybe OrderBy
stringToOrderBy orderBy =
    case orderBy of
        -- "AZ" ->
        --     Just Alphabetical
        --
        "CO" ->
            Just EtablissementConsultation

        "DC" ->
            Just EtablissementCreation

        _ ->
            Nothing


type OrderDirection
    = Desc
    | Asc


orderDirectionToString : OrderDirection -> String
orderDirectionToString orderDirection =
    case orderDirection of
        Asc ->
            "ASC"

        Desc ->
            "DESC"


stringToOrderDirection : String -> Maybe OrderDirection
stringToOrderDirection orderDirection =
    case orderDirection of
        "ASC" ->
            Just Asc

        "DESC" ->
            Just Desc

        _ ->
            Nothing


type Territorialite
    = Endogene
    | Exogene


type Ess
    = EssOui
    | EssNon


territorialiteToLabel : Territorialite -> String
territorialiteToLabel territorialite =
    case territorialite of
        Endogene ->
            "Sur le territoire"

        Exogene ->
            "Exogène au territoire"


territorialiteToString : Territorialite -> String
territorialiteToString territorialite =
    case territorialite of
        Endogene ->
            "endogene"

        Exogene ->
            "exogene"


stringToTerritorialite : Maybe String -> List Territorialite
stringToTerritorialite s =
    case s of
        Nothing ->
            []

        Just "exogene" ->
            [ Exogene ]

        Just "endogene" ->
            [ Endogene ]

        Just "tous" ->
            [ Exogene, Endogene ]

        _ ->
            []


type alias Pagination =
    { page : Int
    , pages : Int
    , results : Int
    }


emptyFilters : Filters
emptyFilters =
    { recherche = ""
    , etat = []
    , codesNaf = []
    , formesJuridiques = Toutes
    , communes = []
    , epcis = []
    , effectifs = []
    , rue = Nothing
    , suivi = Tout
    , activites = []
    , demandes = []
    , zones = []
    , mots = []
    , orderBy = EtablissementConsultation
    , orderDirection = Desc
    , territorialite = []
    , qpvs = []
    , zonages = []
    , zrrs = []
    , ess = []
    }


type alias FiltersSource =
    { activites : List String
    , demandes : List TypeDemande
    , zones : List String
    , mots : List String
    , qpvs : List Qpv
    , zonages : List Zonage
    }


viewFilters : Model -> Html Msg
viewFilters { filtersSource, communes, epcis, selectCommunes, selectEpcis, selectCodesNaf, selectEffectifs, selectRue, selectActivites, selectZones, selectMots, selectDemandes, selectQpvs, selectZonages, selectZrrs, filters } =
    let
        { activites, zones, mots, demandes, qpvs, zonages } =
            case filtersSource of
                RD.Success sources ->
                    sources

                _ ->
                    { activites = [], zones = [], mots = [], demandes = [], qpvs = [], zonages = [] }

        moreThanOneCommune =
            communes
                |> RD.withDefault []
                |> List.length
                |> (\length -> length > 2)

        isPetr =
            epcis
                |> RD.withDefault []
                |> List.length
                |> (\length -> length > 0)
    in
    div []
        [ let
            selected =
                0
                    + List.length filters.etat
                    + List.length filters.codesNaf
                    + (if filters.formesJuridiques == Toutes then
                        0

                       else
                        1
                      )
                    + List.length filters.communes
                    + List.length filters.epcis
                    + List.length filters.effectifs
                    + (filters.rue |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                    + List.length filters.territorialite
                    + List.length filters.qpvs
                    + List.length filters.zrrs
                    + List.length filters.ess
          in
          DSFR.Accordion.raw
            { id = "filtres-donnees-publiques"
            , title =
                [ h2 [ Typo.fr_h6, class "flex flex-row gap-4" ]
                    [ span [] [ text "Données publiques" ]
                    , if selected > 0 then
                        span [ class "circled" ]
                            [ text <| formatNumberWithThousandSpacing <| selected
                            ]

                      else
                        nothing
                    ]
                ]
            , borderless = False
            , content =
                [ div [ class "flex flex-col lg:flex-row gap-9" ]
                    [ div [ class "lg:basis-2/3 flex flex-col" ]
                        [ div [ class "flex flex-col gap-9" ]
                            [ Html.Lazy.lazy2 codesNafFilter filters.codesNaf selectCodesNaf
                            , Html.Lazy.lazy2 effectifsFilter filters.effectifs selectEffectifs
                            , Html.Lazy.lazy essFilter filters.ess
                            ]
                        ]
                    , div [ class "lg:basis-2/3 flex flex-col" ]
                        [ div [ class "flex flex-col gap-9" ]
                            [ Html.Lazy.lazy2 rueFilter selectRue filters.rue
                            , Html.Lazy.lazy3 qpvsFilter selectQpvs qpvs filters.qpvs
                            , Html.Lazy.lazy2 zrrsFilter filters.zrrs selectZrrs
                            , viewIf moreThanOneCommune <|
                                Html.Lazy.lazy3 communeFilter (communes |> RD.withDefault []) selectCommunes filters.communes
                            , viewIf isPetr <|
                                Html.Lazy.lazy3 epciFilter (epcis |> RD.withDefault []) selectEpcis filters.epcis
                            ]
                        ]
                    , div [ class "lg:basis-2/3 flex flex-col" ]
                        [ div [ class "flex flex-col gap-9" ]
                            [ Html.Lazy.lazy etatFilter filters.etat
                            , Html.Lazy.lazy formesFilter filters.formesJuridiques
                            , Html.Lazy.lazy territorialiteFilter filters.territorialite
                            ]
                        ]
                    ]
                ]
            }
        , let
            selected =
                0
                    + List.length filters.activites
                    + List.length filters.demandes
                    + List.length filters.zones
                    + List.length filters.mots
                    + List.length filters.zonages
          in
          DSFR.Accordion.raw
            { id = "filtres-donnees-personnalisees"
            , title =
                [ h2 [ Typo.fr_h6, class "flex flex-row gap-4" ]
                    [ span [] [ text "Données personnalisées" ]
                    , if selected > 0 then
                        span [ class "circled" ]
                            [ text <| formatNumberWithThousandSpacing <| selected
                            ]

                      else
                        nothing
                    ]
                ]
            , borderless = False
            , content =
                [ div [ class "flex flex-col lg:flex-row gap-9" ]
                    [ div [ class "lg:basis-2/3 flex flex-col" ]
                        [ div [ class "flex flex-col gap-9" ]
                            [ Html.Lazy.lazy3 activitesFilter selectActivites activites filters.activites
                            , Html.Lazy.lazy3 zonesFilter selectZones zones filters.zones
                            , Html.Lazy.lazy3 motsFilter selectMots mots filters.mots
                            ]
                        ]
                    , div [ class "lg:basis-2/3 flex flex-col" ]
                        [ div [ class "flex flex-col gap-9" ]
                            [ Html.Lazy.lazy3 demandesFilter selectDemandes demandes filters.demandes
                            ]
                        ]
                    , div [ class "lg:basis-2/3 flex flex-col" ]
                        [ div [ class "flex flex-col gap-9" ]
                            [ Html.Lazy.lazy3 zonagesFilter selectZonages zonages filters.zonages
                            ]
                        ]
                    ]
                ]
            }
        ]


hasFilterTags : String -> Bool
hasFilterTags url =
    let
        filters =
            url |> queryToFilters

        hasFilters =
            [ List.length filters.activites > 0
            , List.length filters.codesNaf > 0
            , List.length filters.communes > 0
            , List.length filters.demandes > 0
            , List.length filters.effectifs > 0
            , List.length filters.epcis > 0
            , List.length filters.ess > 0
            , List.length filters.etat > 0
            , filters.formesJuridiques /= Toutes
            , List.length filters.mots > 0
            , List.length filters.qpvs > 0
            , filters.recherche /= ""
            , filters.rue /= Nothing
            , filters.suivi /= Tout
            , List.length filters.territorialite > 0
            , List.length filters.zonages > 0
            , List.length filters.zones > 0
            , List.length filters.zrrs > 0
            ]
    in
    List.any (\a -> a == True) hasFilters


codesNafFilter : List CodeNaf -> MultiSelectRemote.SmartSelect Msg CodeNaf -> Html Msg
codesNafFilter codesNaf selectCodesNaf =
    div [ class "flex flex-col gap-4" ]
        [ selectCodesNaf
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = codesNaf
                , optionLabelFn = \code -> code.id ++ " - " ++ code.label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Codes NAF" ]
                , viewSelectedOptionFn = text << (\code -> code.id ++ " - " ++ code.label)
                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , noResultsForMsg = \searchText -> "Aucun autre code NAF n'a été trouvé pour " ++ searchText
                , noOptionsMsg = "Aucun code NAF n'a été trouvé"
                , newOption = Nothing
                }
        , case codesNaf of
            [] ->
                nothing

            _ ->
                codesNaf
                    |> List.map
                        (\codeNaf ->
                            { data = codeNaf, toString = .id }
                                |> DSFR.Tag.deletable UnselectedCodeNaf
                                |> DSFR.Tag.withAttrs
                                    [ Attr.title <|
                                        codeNaf.id
                                            ++ (if codeNaf.label == "" then
                                                    ""

                                                else
                                                    " - " ++ codeNaf.label
                                               )
                                    ]
                        )
                    |> DSFR.Tag.medium
        ]


effectifsFilter : List Effectifs -> MultiSelect.SmartSelect Msg Effectifs -> Html Msg
effectifsFilter effectifs selectEffectifs =
    div [ class "flex flex-col gap-4" ]
        [ selectEffectifs
            |> MultiSelect.viewCustom
                { isDisabled = False
                , selected = effectifs
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Effectifs" ]
                , viewSelectedOptionFn = text << (\code -> code.id ++ " - " ++ code.label)
                , noResultsForMsg = \searchText -> "Aucune autre tranche d'effectifs n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune tranche d'effectifs n'a été trouvée"
                , options = Data.Effectifs.tranchesEffectifs
                , searchFn =
                    \searchText allOptions ->
                        List.filter
                            (String.contains (String.toLower searchText) << String.toLower << .label)
                            allOptions
                , searchPrompt = ""
                }
        , case effectifs of
            [] ->
                nothing

            _ ->
                effectifs
                    |> List.map
                        (\effectif ->
                            { data = effectif, toString = .label }
                                |> DSFR.Tag.deletable UnselectedEffectifs
                        )
                    |> DSFR.Tag.medium
        ]


zrrsFilter : List ZRR -> MultiSelect.SmartSelect Msg ZRR -> Html Msg
zrrsFilter zrrs selectZrrs =
    div [ class "flex flex-col gap-2" ]
        [ selectZrrs
            |> MultiSelect.viewCustom
                { isDisabled = False
                , selected = zrrs
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "ZRR - Zone de Revitalisation Rurale" ]
                , viewSelectedOptionFn = text << .label
                , noResultsForMsg = \searchText -> "Aucune autre classement n'a été trouvé pour " ++ searchText
                , noOptionsMsg = "Aucun classement n'a été trouvé"
                , options = Data.ZRR.zrrs
                , searchFn =
                    \searchText allOptions ->
                        List.filter
                            (String.contains (String.toLower searchText) << String.toLower << .label)
                            allOptions
                , searchPrompt = "Filtrer par ZRR"
                }
        , case zrrs of
            [] ->
                nothing

            _ ->
                zrrs
                    |> List.map
                        (\zrr ->
                            { data = zrr, toString = .label }
                                |> DSFR.Tag.deletable UnselectedZRR
                        )
                    |> DSFR.Tag.medium
        ]


suiviToString : Suivi -> String
suiviToString suivi =
    case suivi of
        Tout ->
            "Tout le territoire "

        Portefeuille ->
            "Portefeuille de votre service"

        Accompagnes ->
            "Anciens créateurs accompagnés"

        Favoris ->
            "Vos favoris"


suiviToLabel : Suivi -> Html msg
suiviToLabel suivi =
    case suivi of
        Tout ->
            span [ Typo.textBold ]
                [ text "Tout le territoire"
                , sup [ Attr.title hintSuiviTous ]
                    [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ]
                    ]
                ]

        Portefeuille ->
            span [ Typo.textBold ]
                [ text "Portefeuille de votre service"
                , sup [ Attr.title hintSuiviPortefeuille ]
                    [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ]
                    ]
                ]

        Accompagnes ->
            span [ Typo.textBold ]
                [ text "Anciens créateurs accompagnés"
                ]

        Favoris ->
            span [ Typo.textBold ]
                [ text "Vos favoris"
                ]


suiviToId : Suivi -> String
suiviToId suivi =
    case suivi of
        Tout ->
            "suivi-toutes"

        Portefeuille ->
            "suivi-portefeuille"

        Favoris ->
            "suivi-favoris"

        Accompagnes ->
            "suivi-accompagnes"


idToSuivi : String -> Maybe Suivi
idToSuivi s =
    case s of
        "suivi-toutes" ->
            Just Tout

        "suivi-portefeuille" ->
            Just Portefeuille

        "suivi-favoris" ->
            Just Favoris

        "suivi-accompagnes" ->
            Just Accompagnes

        _ ->
            Nothing


suiviFilter : Suivi -> Html Msg
suiviFilter suivi =
    div [ class "mb-4" ]
        [ DSFR.Input.new
            { value = suiviToId suivi
            , onInput =
                idToSuivi
                    >> Maybe.map SetSuiviFilter
                    >> Maybe.withDefault NoOp
            , label =
                span
                    [ Typo.textBold
                    , Typo.textLg
                    , class "!mb-0"
                    ]
                    [ text "Sélection" ]
            , name = "select-suivi"
            }
            |> DSFR.Input.select
                { options =
                    [ Tout
                    , Portefeuille
                    , Favoris
                    , Accompagnes
                    ]
                , toId = suiviToId
                , toLabel = suiviToLabel
                }
            |> DSFR.Input.view
        ]


etatFilter : List EtatAdministratif -> Html Msg
etatFilter etat =
    DSFR.Checkbox.group
        { id = "filtres-etat-type"
        , values =
            [ Actif
            , Inactif
            ]
        , toLabel = etatAdministratifToDisplay
        , toId = etatAdministratifToString
        , checked = etat
        , valueAsString = etatAdministratifToString
        , onChecked = \data bool -> SetEtatFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "État administratif"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


formesToLabel : FormesJuridiques -> String
formesToLabel formes =
    case formes of
        Courantes ->
            "Seulement les plus courantes"

        Toutes ->
            "Toutes"


formesFilter : FormesJuridiques -> Html Msg
formesFilter formesJuridiques =
    let
        formesJuridiquesHint =
            "Les formes juridiques les plus courantes excluent\u{00A0}:\n"
                ++ (Data.FormeJuridique.formesJuridiquesExclues |> List.map Data.FormeJuridique.formeJuridiqueToLabel |> List.sort |> String.join "\n")
    in
    DSFR.Checkbox.group
        { id = "filtres-formes-juridiques"
        , values =
            [ Courantes
            ]
        , toLabel = formesToLabel
        , toId =
            \formes ->
                case formes of
                    Courantes ->
                        "courantes"

                    Toutes ->
                        "toutes"
        , checked = [ formesJuridiques ]
        , valueAsString =
            \formes ->
                case formes of
                    Courantes ->
                        "courantes"

                    Toutes ->
                        "toutes"
        , onChecked = \_ -> SetFormesJuridiquesFilter
        , label =
            div [ Typo.textBold ]
                [ text "Formes juridiques"
                , sup [ Attr.title formesJuridiquesHint ]
                    [ span [ Typo.textXs ]
                        [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine
                        ]
                    ]
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


essFilter : List Ess -> Html Msg
essFilter ess =
    DSFR.Checkbox.group
        { id = "filtres-ess"
        , values =
            [ EssOui
            , EssNon
            ]
        , toLabel =
            \e ->
                case e of
                    EssOui ->
                        "Oui"

                    EssNon ->
                        "Non"
        , toId =
            \e ->
                case e of
                    EssOui ->
                        "oui"

                    EssNon ->
                        "non"
        , checked = ess
        , valueAsString =
            \e ->
                case e of
                    EssOui ->
                        "oui"

                    EssNon ->
                        "non"
        , onChecked =
            \data bool -> SetESSFilter bool data
        , label =
            div [ Typo.textBold ]
                [ span [ Typo.textBold ] [ text "Économie sociale et solidaire", sup [ Attr.title hintESS ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


communeFilter : List Commune -> MultiSelect.SmartSelect Msg Commune -> List Commune -> Html Msg
communeFilter optionsCommunes selectCommunes communes =
    let
        communeToLabel c =
            case ( c.label, c.departement ) of
                ( "", "" ) ->
                    c.id

                _ ->
                    c.label ++ " (" ++ c.departement ++ ")"
    in
    div [ class "flex flex-col gap-2" ]
        [ selectCommunes
            |> MultiSelect.viewCustom
                { isDisabled = optionsCommunes |> List.length |> (==) 0
                , selected = communes
                , options = optionsCommunes
                , optionLabelFn = communeToLabel
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Communes"
                , viewSelectedOptionFn = .label >> text
                , noResultsForMsg = \searchText -> "Aucune autre commune n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune commune n'a été trouvée"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower << .label) <|
                            allOptions
                , searchPrompt = "Filtrer par commune"
                }
        , case communes of
            [] ->
                nothing

            _ ->
                communes
                    |> List.map (\commune -> DSFR.Tag.deletable UnselectCommune { data = commune, toString = communeToLabel })
                    |> DSFR.Tag.medium
        ]


epciFilter : List Epci -> MultiSelect.SmartSelect Msg Epci -> List Epci -> Html Msg
epciFilter optionsEpcis selectEpcis epcis =
    let
        epciToLabel c =
            case c.label of
                "" ->
                    c.id

                _ ->
                    c.label
    in
    div [ class "flex flex-col gap-2" ]
        [ selectEpcis
            |> MultiSelect.viewCustom
                { isDisabled = optionsEpcis |> List.length |> (==) 0
                , selected = epcis
                , options = optionsEpcis
                , optionLabelFn = epciToLabel
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "EPCIs"
                , viewSelectedOptionFn = .label >> text
                , noResultsForMsg = \searchText -> "Aucun autre EPCI n'a été trouvé pour " ++ searchText
                , noOptionsMsg = "Aucun EPCI n'a été trouvé"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower << .label) <|
                            allOptions
                , searchPrompt = "Filtrer par commune"
                }
        , case epcis of
            [] ->
                nothing

            _ ->
                epcis
                    |> List.map (\epci -> DSFR.Tag.deletable UnselectEpci { data = epci, toString = epciToLabel })
                    |> DSFR.Tag.medium
        ]


rueFilter : SingleSelectRemote.SmartSelect Msg ApiStreet -> Maybe ApiStreet -> Html Msg
rueFilter selectRue selectedRue =
    div [ class "flex flex-col gap-2" ]
        [ selectRue
            |> SingleSelectRemote.viewCustom
                { isDisabled = False
                , selected = selectedRue
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Adresse" ]
                , searchPrompt = "Rechercher une adresse"
                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , noResultsForMsg = \searchText -> "Aucune autre adresse n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune adresse n'a été trouvée"
                , error = Nothing
                }
        , selectedRue
            |> viewMaybe
                (\rue ->
                    DSFR.Tag.deletable (\_ -> ClearRue) { data = rue.label, toString = identity }
                        |> List.singleton
                        |> DSFR.Tag.medium
                )
        ]


activitesFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
activitesFilter selectActivites optionsActivites activites =
    div [ class "flex flex-col gap-4" ]
        [ selectActivites
            |> MultiSelect.viewCustom
                { isDisabled = optionsActivites |> List.length |> (==) 0
                , selected = activites
                , options = optionsActivites
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Activité réelle et filière"
                , viewSelectedOptionFn = text
                , noResultsForMsg = \searchText -> "Aucune autre activité n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune activité n'a été trouvée"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower)
                            allOptions
                , searchPrompt = "Filtrer par activité"
                }
        , case activites of
            [] ->
                nothing

            _ ->
                activites
                    |> List.map (\activite -> DSFR.Tag.deletable UnselectActivite { data = activite, toString = identity })
                    |> DSFR.Tag.medium
        ]


zonesFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
zonesFilter selectZones optionsZones zones =
    div [ class "flex flex-col gap-4" ]
        [ selectZones
            |> MultiSelect.viewCustom
                { isDisabled = optionsZones |> List.length |> (==) 0
                , selected = zones
                , options = optionsZones
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Zone géographique"
                , viewSelectedOptionFn = text
                , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower)
                            allOptions
                , searchPrompt = "Filtrer par zone géographique"
                }
        , case zones of
            [] ->
                nothing

            _ ->
                zones
                    |> List.map (\zone -> DSFR.Tag.deletable UnselectZone { data = zone, toString = identity })
                    |> DSFR.Tag.medium
        ]


motsFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
motsFilter selectMots optionsMots mots =
    div [ class "flex flex-col gap-4" ]
        [ selectMots
            |> MultiSelect.viewCustom
                { isDisabled = optionsMots |> List.length |> (==) 0
                , selected = mots
                , options = optionsMots
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Mots-clés"
                , viewSelectedOptionFn = text
                , noResultsForMsg = \searchText -> "Aucun autre mot-clé n'a été trouvé pour " ++ searchText
                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower)
                            allOptions
                , searchPrompt = "Filtrer par mot-clé"
                }
        , case mots of
            [] ->
                nothing

            _ ->
                mots
                    |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
                    |> DSFR.Tag.medium
        ]


qpvsFilter : MultiSelect.SmartSelect Msg Qpv -> List Qpv -> List Qpv -> Html Msg
qpvsFilter selectQpvs optionsQpvs qpvs =
    div [ class "flex flex-col gap-2" ]
        [ selectQpvs
            |> MultiSelect.viewCustom
                { isDisabled = optionsQpvs |> List.length |> (==) 0
                , selected = qpvs
                , options = optionsQpvs
                , optionLabelFn = .nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "QPV - Quartier Prioritaire de la Politique de la Ville"
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg = \searchText -> "Aucun autre QPV n'a été trouvé pour " ++ searchText
                , noOptionsMsg = "Aucun QPV n'a été trouvé"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower << .nom)
                            allOptions
                , searchPrompt = "Filtrer par QPV"
                }
        , case qpvs of
            [] ->
                nothing

            _ ->
                qpvs
                    |> List.map (\qpv -> DSFR.Tag.deletable UnselectQpv { data = qpv, toString = .nom })
                    |> DSFR.Tag.medium
        ]


zonagesFilter : MultiSelect.SmartSelect Msg Zonage -> List Zonage -> List Zonage -> Html Msg
zonagesFilter selectZonages optionsZonages zonages =
    div [ class "flex flex-col gap-2" ]
        [ selectZonages
            |> MultiSelect.viewCustom
                { isDisabled = optionsZonages |> List.length |> (==) 0
                , selected = zonages
                , options = optionsZonages
                , optionLabelFn = .nom
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Zonage"
                , viewSelectedOptionFn = .nom >> text
                , noResultsForMsg = \searchText -> "Aucun autre Zonage n'a été trouvé pour " ++ searchText
                , noOptionsMsg = "Aucun Zonage n'a été trouvé"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower << .nom)
                            allOptions
                , searchPrompt = "Filtrer par Zonage"
                }
        , case zonages of
            [] ->
                nothing

            _ ->
                zonages
                    |> List.map (\zonage -> DSFR.Tag.deletable UnselectZonage { data = zonage, toString = .nom })
                    |> DSFR.Tag.medium
        ]


demandesFilter : MultiSelect.SmartSelect Msg TypeDemande -> List TypeDemande -> List TypeDemande -> Html Msg
demandesFilter selectDemandes optionsDemandes demandes =
    div [ class "flex flex-col gap-4" ]
        [ selectDemandes
            |> MultiSelect.viewCustom
                { isDisabled = optionsDemandes |> List.length |> (==) 0
                , selected = demandes
                , options = optionsDemandes
                , optionLabelFn = Data.Demande.typeDemandeToDisplay
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Demandes en cours"
                , viewSelectedOptionFn = Data.Demande.typeDemandeToDisplay >> text
                , noResultsForMsg = \searchText -> "Aucun autre type de demande n'a été trouvé pour " ++ searchText
                , noOptionsMsg = "Aucun demande n'a été trouvé"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower << Data.Demande.typeDemandeToDisplay)
                            allOptions
                , searchPrompt = "Filtrer par demande en cours"
                }
        , case demandes of
            [] ->
                nothing

            _ ->
                demandes
                    |> List.map (\demande -> DSFR.Tag.deletable UnselectDemande { data = demande, toString = Data.Demande.typeDemandeToDisplay })
                    |> DSFR.Tag.medium
        ]


territorialiteFilter : List Territorialite -> Html Msg
territorialiteFilter territorialites =
    DSFR.Checkbox.group
        { id = "filtres-territorialite"
        , values =
            [ Endogene
            , Exogene
            ]
        , toLabel = territorialiteToLabel
        , toId = territorialiteToString
        , checked = territorialites
        , valueAsString = territorialiteToString
        , onChecked = \data bool -> SetTerritorialiteFilter bool data
        , label =
            div [ Typo.textBold ]
                [ text "Appartenance au territoire"
                ]
        }
        |> DSFR.Checkbox.groupWithExtraAttrs [ class "!mb-0" ]
        |> DSFR.Checkbox.viewGroup


showFilterTags : Model -> Html Msg
showFilterTags model =
    let
        currentUrl =
            model.currentUrl

        communesSources =
            model.communes
                |> RD.withDefault []

        epcisSources =
            model.epcis
                |> RD.withDefault []

        qpvsSources =
            model.filtersSource
                |> RD.map .qpvs
                |> RD.withDefault []

        zonagesSources =
            model.filtersSource
                |> RD.map .zonages
                |> RD.withDefault []

        pagination =
            let
                p =
                    queryToPagination currentUrl
            in
            { p | page = 1 }

        filters =
            queryToFilters currentUrl

        { suivi, recherche, territorialite, zones, codesNaf, effectifs, ess, rue, qpvs, zrrs, communes, etat, activites, mots, demandes, zonages, epcis, formesJuridiques } =
            filters

        newCommunes =
            hydrateData .id communesSources communes

        newEpcis =
            hydrateData .id epcisSources epcis

        newQpvs =
            hydrateData .id qpvsSources qpvs

        newZonages =
            hydrateData .id zonagesSources zonages

        newZrrs =
            List.map
                (\zrr ->
                    case zrr.id of
                        "P" ->
                            { zrr | label = "Partiellement classé" }

                        "C" ->
                            { zrr | label = "Classé" }

                        "N" ->
                            { zrr | label = "Non classé" }

                        _ ->
                            { zrr | label = "" }
                )
                zrrs

        effectifsIds =
            effectifs
                |> List.map .id

        newEffectifs =
            Data.Effectifs.tranchesEffectifs
                |> List.filter (\{ id } -> List.member id effectifsIds)
    in
    div
        [ class "flex flex-row gap-2 mt-1" ]
        [ DSFR.Tag.groupWrapper <|
            toFilterDeletableTags "Sélection"
                (if suivi == Tout then
                    []

                 else
                    [ suivi ]
                )
                (\_ -> UpdateUrl <| ( { filters | suivi = Tout }, pagination ))
                suiviToString
                ++ toFilterDeletableTags "Recherche"
                    (if recherche /= "" then
                        [ recherche ]

                     else
                        []
                    )
                    (\_ -> UpdateUrl <| ( { filters | recherche = "" }, pagination ))
                    identity
                ++ toFilterDeletableTags "Code NAF"
                    codesNaf
                    (\c ->
                        UpdateUrl <|
                            ( { filters
                                | codesNaf =
                                    filters.codesNaf
                                        |> List.filter
                                            (\t -> t /= c)
                              }
                            , pagination
                            )
                    )
                    (\c -> c.id)
                ++ toFilterDeletableTags "Effectifs"
                    newEffectifs
                    (\e ->
                        UpdateUrl <|
                            ( { filters
                                | effectifs =
                                    newEffectifs
                                        |> List.filter
                                            (\t -> t.id /= e.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.label)
                ++ toFilterDeletableTags "Économie sociale et solidaire"
                    ess
                    (\e ->
                        UpdateUrl <|
                            ( { filters
                                | ess =
                                    filters.ess
                                        |> List.filter
                                            (\t -> t /= e)
                              }
                            , pagination
                            )
                    )
                    (\e ->
                        case e of
                            EssOui ->
                                "Oui"

                            EssNon ->
                                "Non"
                    )
                ++ toFilterDeletableTags "Appartenance"
                    territorialite
                    (\terr ->
                        UpdateUrl <|
                            ( { filters
                                | territorialite =
                                    filters.territorialite
                                        |> List.filter
                                            (\t -> t /= terr)
                              }
                            , pagination
                            )
                    )
                    territorialiteToLabel
                ++ toFilterDeletableTags "Adresse"
                    (case rue of
                        Nothing ->
                            []

                        Just a ->
                            [ a ]
                    )
                    (\_ -> UpdateUrl <| ( { filters | rue = Nothing }, pagination ))
                    (\e -> e.label)
                ++ toFilterDeletableTags "QPV"
                    newQpvs
                    (\qpv ->
                        UpdateUrl <|
                            ( { filters
                                | qpvs =
                                    filters.qpvs
                                        |> List.filter
                                            (\t -> t.id /= qpv.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
                ++ toFilterDeletableTags "ZRR"
                    newZrrs
                    (\zrr ->
                        UpdateUrl <|
                            ( { filters
                                | zrrs =
                                    filters.zrrs
                                        |> List.filter
                                            (\t -> t.id /= zrr.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.label)
                ++ toFilterDeletableTags "Commune"
                    newCommunes
                    (\co ->
                        UpdateUrl <|
                            ( { filters
                                | communes =
                                    filters.communes
                                        |> List.filter
                                            (\t -> t.id /= co.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.label)
                ++ toFilterDeletableTags "Epci"
                    newEpcis
                    (\co ->
                        UpdateUrl <|
                            ( { filters
                                | epcis =
                                    filters.epcis
                                        |> List.filter
                                            (\t -> t.id /= co.id)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.label)
                ++ toFilterDeletableTags "Formes juridiques"
                    (if formesJuridiques == Courantes then
                        [ formesJuridiques ]

                     else
                        []
                    )
                    (\_ -> UpdateUrl <| ( { filters | formesJuridiques = Toutes }, pagination ))
                    formesToLabel
                ++ toFilterDeletableTags "État administratif"
                    etat
                    (\e ->
                        UpdateUrl <|
                            ( { filters
                                | etat = setEtats filters False e
                              }
                            , pagination
                            )
                    )
                    etatAdministratifToDisplay
                ++ toFilterDeletableTags "Zone"
                    zones
                    (\zone ->
                        UpdateUrl <|
                            ( { filters
                                | zones =
                                    filters.zones
                                        |> List.filter
                                            (\z -> z /= zone)
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Activité"
                    activites
                    (\a ->
                        UpdateUrl <|
                            ( { filters
                                | activites =
                                    filters.activites
                                        |> List.filter
                                            (\z -> z /= a)
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Mot-clé"
                    mots
                    (\m ->
                        UpdateUrl <|
                            ( { filters
                                | mots =
                                    filters.mots
                                        |> List.filter
                                            (\z -> z /= m)
                              }
                            , pagination
                            )
                    )
                    identity
                ++ toFilterDeletableTags "Demande en cours"
                    demandes
                    (\m ->
                        UpdateUrl <|
                            ( { filters
                                | demandes =
                                    filters.demandes
                                        |> List.filter
                                            (\z -> z /= m)
                              }
                            , pagination
                            )
                    )
                    Data.Demande.typeDemandeToDisplay
                ++ toFilterDeletableTags "Zonage"
                    newZonages
                    (\m ->
                        UpdateUrl <|
                            ( { filters
                                | zonages =
                                    filters.zonages
                                        |> List.filter
                                            (\z -> z /= m)
                              }
                            , pagination
                            )
                    )
                    (\e -> e.nom)
        ]


toFilterDeletableTags : String -> List data -> (data -> msg) -> (data -> String) -> List (Html msg)
toFilterDeletableTags prefix list toMsg toString =
    list
        |> List.sortBy toString
        |> List.map
            (\data ->
                DSFR.Tag.deletable toMsg { data = data, toString = \d -> prefix ++ "\u{00A0}: " ++ toString d }
            )
        |> List.map DSFR.Tag.oneMedium


filterPanel : Model -> Html Msg
filterPanel model =
    div [ class "fr-card--white p-4 flex flex-col gap-4" ]
        [ DSFR.Accordion.raw
            { id = "filtres-globaux"
            , title =
                [ h2 [ Typo.fr_h6, class "!mb-0" ] [ text "Filtres" ]
                ]
            , borderless = False
            , content =
                [ div [ class "flex flex-col gap-4" ]
                    [ div [ class "flex flex-col lg:flex-row gap-4" ]
                        [ div [ class "lg:basis-1/2" ]
                            [ Html.Lazy.lazy suiviFilter model.filters.suivi
                            ]
                        , div [ class "lg:basis-1/2" ]
                            [ div [ class "mb-4" ]
                                [ DSFR.Input.new
                                    { value = model.filters.recherche
                                    , onInput = UpdatedSearch
                                    , label =
                                        label
                                            [ class "fr-label !mb-0"
                                            , Typo.textBold
                                            , Typo.textLg
                                            , Attr.for rechercherEtablissementInputName
                                            ]
                                            [ text "Nom, SIRET, SIREN, nom du contact" ]
                                    , name = rechercherEtablissementInputName
                                    }
                                    |> DSFR.Input.view
                                ]
                            ]
                        ]
                    , viewFilters model
                    , DSFR.Button.new { label = "Rechercher", onClick = Just ClickedSearch }
                        |> DSFR.Button.withDisabled (model.currentUrl == filtersAndPaginationToQuery ( model.filters, model.pagination ))
                        |> DSFR.Button.withAttrs [ class "!w-full justify-center" ]
                        |> DSFR.Button.large
                        |> DSFR.Button.view
                        |> List.singleton
                        |> div [ class "flex flex-row w-full mt-4" ]
                    ]
                ]
            }
        , if hasFilterTags model.currentUrl then
            div [ Attr.id listFiltersTagsId ]
                [ div [ class "flex flex-row !mt-2" ]
                    [ showFilterTags model
                    , div [ class "ml-auto shrink-0" ] [ clearAllFiltersButton model.currentUrl ]
                    ]
                ]

          else
            text ""
        ]


clearAllFiltersButton : String -> Html Msg
clearAllFiltersButton currentUrl =
    DSFR.Button.new
        { label = "Tout effacer"
        , onClick = Just <| ClearAllFilters
        }
        |> DSFR.Button.withDisabled (queryToFilters currentUrl == emptyFilters)
        |> DSFR.Button.tertiaryNoOutline
        |> DSFR.Button.view


queryToFilters : String -> Filters
queryToFilters rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    recherche =
                        query
                            |> QS.getAsStringList queryKeys.recherche
                            |> List.head
                            |> Maybe.withDefault ""

                    etat =
                        query
                            |> QS.getAsStringList queryKeys.etat
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "T" ->
                                            [ Actif, Inactif ]

                                        Just "F" ->
                                            [ Inactif ]

                                        Just "A" ->
                                            [ Actif ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )

                    codesNaf =
                        query
                            |> QS.getAsStringList queryKeys.codesNaf
                            |> List.map (\id -> { id = id, label = "" })

                    effectifs =
                        query
                            |> QS.getAsStringList queryKeys.effectifs
                            |> List.map (\id -> { id = id, label = "" })

                    zrrs =
                        query
                            |> QS.getAsStringList queryKeys.zrrs
                            |> List.map (\id -> { id = id, label = "" })

                    formesJuridiques =
                        query
                            |> QS.getAsStringList queryKeys.formesJuridiques
                            |> List.head
                            |> Maybe.map
                                (\formes ->
                                    if formes == "courantes" then
                                        Courantes

                                    else
                                        Toutes
                                )
                            |> Maybe.withDefault Toutes

                    communes =
                        query
                            |> QS.getAsStringList queryKeys.communes
                            |> List.map (\id -> { id = id, label = "", departement = "" })

                    epcis =
                        query
                            |> QS.getAsStringList queryKeys.epcis
                            |> List.map (\id -> { id = id, label = "" })

                    rue =
                        query
                            |> QS.getAsStringList queryKeys.rue
                            |> List.head

                    cp =
                        query
                            |> QS.getAsStringList queryKeys.cp
                            |> List.head

                    suivi =
                        query
                            |> QS.getAsStringList queryKeys.suivi
                            |> List.head
                            |> Maybe.map
                                (\s ->
                                    if s == "portefeuille" then
                                        Portefeuille

                                    else if s == "favoris" then
                                        Favoris

                                    else if s == "accompagnes" then
                                        Accompagnes

                                    else
                                        Tout
                                )
                            |> Maybe.withDefault Tout

                    activites =
                        query
                            |> QS.getAsStringList queryKeys.activites

                    demandes =
                        query
                            |> QS.getAsStringList queryKeys.demandes
                            |> List.map Data.Demande.stringToTypeDemande
                            |> List.filterMap identity

                    zones =
                        query
                            |> QS.getAsStringList queryKeys.zones

                    mots =
                        query
                            |> QS.getAsStringList queryKeys.mots

                    orderBy =
                        query
                            |> QS.getAsStringList queryKeys.ordre
                            |> List.head
                            |> Maybe.andThen stringToOrderBy
                            |> Maybe.withDefault EtablissementConsultation

                    orderDirection =
                        query
                            |> QS.getAsStringList queryKeys.direction
                            |> List.head
                            |> Maybe.andThen stringToOrderDirection
                            |> Maybe.withDefault Desc

                    territorialite =
                        query
                            |> QS.getAsStringList queryKeys.territorialite
                            |> List.head
                            |> stringToTerritorialite

                    qpvs =
                        query
                            |> QS.getAsStringList queryKeys.qpvs
                            |> List.filterMap (String.toInt >> Maybe.map (\id -> { id = id, nom = "" }))

                    zonages =
                        query
                            |> QS.getAsStringList queryKeys.zonages
                            |> List.filterMap (String.toInt >> Maybe.map (\id -> { id = id, nom = "" }))

                    ess =
                        query
                            |> QS.getAsStringList queryKeys.ess
                            |> List.head
                            |> (\e ->
                                    case e of
                                        Just "tous" ->
                                            [ EssOui, EssNon ]

                                        Just "oui" ->
                                            [ EssOui ]

                                        Just "non" ->
                                            [ EssNon ]

                                        Just _ ->
                                            []

                                        Nothing ->
                                            []
                               )
                in
                { recherche = recherche
                , etat = etat
                , codesNaf = codesNaf
                , formesJuridiques = formesJuridiques
                , communes = communes
                , epcis = epcis
                , effectifs = effectifs
                , rue = Maybe.map2 (\r c -> ApiStreet (r ++ " - " ++ c) r c "") rue cp
                , suivi = suivi
                , activites = activites
                , zones = zones
                , mots = mots
                , demandes = demandes
                , orderBy = orderBy
                , orderDirection = orderDirection
                , territorialite = territorialite
                , qpvs = qpvs
                , zonages = zonages
                , zrrs = zrrs
                , ess = ess
                }
           )


filtersToQuery : Filters -> QS.Query
filtersToQuery filters =
    let
        setEtat =
            let
                isActif =
                    List.member Actif filters.etat

                isInactif =
                    List.member Inactif filters.etat
            in
            case ( isActif, isInactif ) of
                ( True, True ) ->
                    QS.setStr queryKeys.etat "T"

                ( True, False ) ->
                    QS.setStr queryKeys.etat "A"

                ( False, True ) ->
                    QS.setStr queryKeys.etat "F"

                ( False, False ) ->
                    identity

        setFormesJuridiques =
            case filters.formesJuridiques of
                Courantes ->
                    "courantes"
                        |> QS.setStr queryKeys.formesJuridiques

                _ ->
                    identity

        setRecherche =
            case filters.recherche of
                "" ->
                    identity

                recherche ->
                    recherche
                        |> QS.setStr queryKeys.recherche

        setCodesNaf =
            filters.codesNaf
                |> List.map .id
                |> QS.setListStr queryKeys.codesNaf

        setEffectifs =
            filters.effectifs
                |> List.map .id
                |> QS.setListStr queryKeys.effectifs

        setZrrs =
            filters.zrrs
                |> List.map .id
                |> QS.setListStr queryKeys.zrrs

        setCodeCommune =
            filters.communes
                |> List.map .id
                |> QS.setListStr queryKeys.communes

        setEpci =
            filters.epcis
                |> List.map .id
                |> QS.setListStr queryKeys.epcis

        setRue =
            filters.rue
                |> Maybe.map .name
                |> Maybe.map (QS.setStr queryKeys.rue)
                |> Maybe.withDefault identity

        setCp =
            filters.rue
                |> Maybe.map .postcode
                |> Maybe.map (QS.setStr queryKeys.cp)
                |> Maybe.withDefault identity

        setSuivi =
            case filters.suivi of
                Favoris ->
                    "favoris"
                        |> QS.setStr queryKeys.suivi

                Portefeuille ->
                    "portefeuille"
                        |> QS.setStr queryKeys.suivi

                Accompagnes ->
                    "accompagnes"
                        |> QS.setStr queryKeys.suivi

                _ ->
                    identity

        setActivites =
            case filters.activites of
                [] ->
                    identity

                activites ->
                    activites
                        |> QS.setListStr queryKeys.activites

        setDemandes =
            case filters.demandes of
                [] ->
                    identity

                demandes ->
                    demandes
                        |> List.map Data.Demande.demandeTypeToString
                        |> QS.setListStr queryKeys.demandes

        setZones =
            case filters.zones of
                [] ->
                    identity

                zones ->
                    zones
                        |> QS.setListStr queryKeys.zones

        setMots =
            case filters.mots of
                [] ->
                    identity

                mots ->
                    mots
                        |> QS.setListStr queryKeys.mots

        setOrder =
            case filters.orderBy of
                -- Alphabetical ->
                --     QS.setStr queryKeys.ordre <|
                --         orderByToString Alphabetical
                --
                EtablissementCreation ->
                    QS.setStr queryKeys.ordre <|
                        orderByToString EtablissementCreation

                EtablissementConsultation ->
                    identity

        setDirection =
            case filters.orderDirection of
                Asc ->
                    QS.setStr queryKeys.direction <|
                        orderDirectionToString Asc

                Desc ->
                    identity

        setTerritorialite =
            let
                isExogene =
                    List.member Exogene filters.territorialite

                isEndogene =
                    List.member Endogene filters.territorialite
            in
            if isExogene && isEndogene then
                QS.setStr queryKeys.territorialite "tous"

            else if isExogene then
                QS.setStr queryKeys.territorialite "exogene"

            else if isEndogene then
                QS.setStr queryKeys.territorialite "endogene"

            else
                identity

        setQpvs =
            case filters.qpvs of
                [] ->
                    identity

                demandes ->
                    demandes
                        |> List.map (.id >> String.fromInt)
                        |> QS.setListStr queryKeys.qpvs

        setZonages =
            case filters.zonages of
                [] ->
                    identity

                demandes ->
                    demandes
                        |> List.map (.id >> String.fromInt)
                        |> QS.setListStr queryKeys.zonages

        setEss =
            let
                isEss =
                    List.member EssOui filters.ess

                isNotEss =
                    List.member EssNon filters.ess
            in
            if isEss && isNotEss then
                QS.setStr queryKeys.ess "tous"

            else if isEss then
                QS.setStr queryKeys.ess "oui"

            else if isNotEss then
                QS.setStr queryKeys.ess "non"

            else
                identity
    in
    QS.empty
        |> setEtat
        |> setFormesJuridiques
        |> setRecherche
        |> setCodesNaf
        |> setCodeCommune
        |> setEpci
        |> setEffectifs
        |> setRue
        |> setCp
        |> setSuivi
        |> setActivites
        |> setZones
        |> setMots
        |> setDemandes
        |> setOrder
        |> setDirection
        |> setTerritorialite
        |> setQpvs
        |> setZonages
        |> setZrrs
        |> setEss


getPagination : Model -> Pagination
getPagination { pagination } =
    pagination


getCurrentUrl : Model -> String
getCurrentUrl { currentUrl } =
    currentUrl


getFilters : Model -> Filters
getFilters { filters } =
    filters


queryToPagination : String -> Pagination
queryToPagination rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    p =
                        query
                            |> QS.getAsStringList "page"
                            |> List.head
                            |> Maybe.andThen String.toInt
                            |> Maybe.withDefault 1

                    pages =
                        query
                            |> QS.getAsStringList "pages"
                            |> List.head
                            |> Maybe.andThen String.toInt
                            |> Maybe.withDefault 1
                in
                { defaultPagination
                    | page = p
                    , pages = pages
                }
           )


defaultPagination : Pagination
defaultPagination =
    { page = 1, pages = 1, results = 0 }


paginationToQuery : Pagination -> QS.Query
paginationToQuery pagination =
    let
        setPage =
            if pagination.page > 1 then
                QS.setStr queryKeys.page <|
                    String.fromInt <|
                        pagination.page

            else
                identity
    in
    QS.empty
        |> setPage


queryKeys : { etat : String, recherche : String, page : String, pages : String, codesNaf : String, formesJuridiques : String, communes : String, epcis : String, effectifs : String, rue : String, cp : String, suivi : String, activites : String, demandes : String, zones : String, mots : String, ordre : String, direction : String, territorialite : String, qpvs : String, zonages : String, zrrs : String, ess : String }
queryKeys =
    { etat = "etat"
    , recherche = "recherche"
    , page = "page"
    , pages = "pages"
    , codesNaf = "codesNaf"
    , formesJuridiques = "formes"
    , communes = "communes"
    , epcis = "epcis"
    , effectifs = "effectifs"
    , rue = "rue"
    , cp = "cp"
    , suivi = "suivi"
    , activites = "activites"
    , zones = "zones"
    , mots = "mots"
    , demandes = "demandes"
    , ordre = "o"
    , direction = "d"
    , territorialite = "territorialite"
    , qpvs = "qpvs"
    , zonages = "zonages"
    , zrrs = "zrrs"
    , ess = "ess"
    }


filtersAndPaginationToQuery : ( Filters, Pagination ) -> String
filtersAndPaginationToQuery ( filters, pagination ) =
    QS.merge (filtersToQuery filters) (paginationToQuery pagination)
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


makeQueryForPortefeuilleMode : () -> String
makeQueryForPortefeuilleMode () =
    filtersAndPaginationToQuery ( { emptyFilters | suivi = Portefeuille }, defaultPagination )


toHref : ( Filters, Pagination ) -> Int -> String
toHref ( filters, pagination ) newPage =
    Route.toUrl <|
        Route.Etablissements <|
            Just <|
                filtersAndPaginationToQuery ( filters, { pagination | page = newPage } )


queryToFiltersAndPagination : Maybe String -> ( Filters, Pagination )
queryToFiltersAndPagination rawQuery =
    rawQuery
        |> Maybe.withDefault ""
        |> (\query -> ( queryToFilters query, queryToPagination query ))


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model.selectCodesNaf |> MultiSelectRemote.subscriptions
        , model.selectCommunes |> MultiSelect.subscriptions
        , model.selectEpcis |> MultiSelect.subscriptions
        , model.selectEffectifs |> MultiSelect.subscriptions
        , model.selectRue |> SingleSelectRemote.subscriptions
        , model.selectActivites |> MultiSelect.subscriptions
        , model.selectDemandes |> MultiSelect.subscriptions
        , model.selectZones |> MultiSelect.subscriptions
        , model.selectMots |> MultiSelect.subscriptions
        , model.selectRue |> SingleSelectRemote.subscriptions
        , model.selectQpvs |> MultiSelect.subscriptions
        , model.selectZrrs |> MultiSelect.subscriptions
        , model.selectZonages |> MultiSelect.subscriptions
        ]


hydrateData : (a -> b) -> List a -> List a -> List a
hydrateData accessor sources data =
    data
        |> List.map
            (\datum ->
                sources
                    |> List.filter (accessor >> (==) (accessor datum))
                    |> List.head
                    |> Maybe.withDefault datum
            )


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedFiltersSource response ->
            let
                sourceQpvs =
                    response
                        |> RD.map .qpvs
                        |> RD.withDefault []

                qpvs =
                    model.filters.qpvs

                newQpvs =
                    qpvs
                        |> List.map
                            (\qpv ->
                                sourceQpvs
                                    |> List.filter (.id >> (==) qpv.id)
                                    |> List.head
                                    |> Maybe.withDefault qpv
                            )
            in
            { model | filtersSource = response, filters = model.filters |> (\fs -> { fs | qpvs = newQpvs }) }
                |> Effect.withNone

        ClearAllFilters ->
            let
                ( updatedSelectRue, selectCmd ) =
                    SingleSelectRemote.setText "" selectRueConfig model.selectRue

                newModel =
                    { model | filters = emptyFilters, selectRue = updatedSelectRue }
            in
            ( newModel
            , Effect.batch [ updateUrl newModel, Effect.fromCmd selectCmd ]
            )

        UpdatedSearch recherche ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | recherche = recherche }) }
            in
            ( newModel
            , Effect.none
            )

        SetEtatFilter add etat ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | etat = setEtats model.filters add etat }) }
            in
            ( newModel
            , Effect.none
            )

        SetSuiviFilter suivi ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | suivi = suivi }) }
            in
            ( newModel
            , Effect.none
            )

        SetFormesJuridiquesFilter courantesOnly ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | formesJuridiques =
                                                if courantesOnly then
                                                    Courantes

                                                else
                                                    Toutes
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SetTerritorialiteFilter add territorialite ->
            let
                currentTerritorialites =
                    model.filters.territorialite

                isMember =
                    List.member territorialite currentTerritorialites

                newTerritorialites =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentTerritorialites

                        ( True, False ) ->
                            currentTerritorialites ++ [ territorialite ]

                        ( False, True ) ->
                            currentTerritorialites
                                |> List.filter (\t -> t /= territorialite)

                        ( False, False ) ->
                            currentTerritorialites

                newModel =
                    { model | filters = model.filters |> (\f -> { f | territorialite = newTerritorialites }) }
            in
            ( newModel
            , Effect.none
            )

        SelectedCodesNaf ( codesNaf, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCodesNafConfig model.selectCodesNaf

                codesNafUniques =
                    case codesNaf of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                codesNaf

                newModel =
                    { model
                        | selectCodesNaf = updatedSelect
                        , filters =
                            model.filters
                                |> (\l ->
                                        { l | codesNaf = codesNafUniques }
                                   )
                    }
            in
            ( newModel
            , Effect.batch [ Effect.none, Effect.fromCmd selectCmd ]
            )

        UpdatedSelectCodesNaf sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCodesNafConfig model.selectCodesNaf
            in
            ( { model
                | selectCodesNaf = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectedCodeNaf codeNaf ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | codesNaf =
                                                f.codesNaf
                                                    |> List.filter ((/=) codeNaf)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ReceivedCommunes resp ->
            let
                communesSources =
                    resp
                        |> RD.withDefault []

                communes =
                    model.filters.communes

                newCommunes =
                    hydrateData .id communesSources communes
            in
            { model
                | communes = resp
                , filters =
                    model.filters |> (\f -> { f | communes = newCommunes })
            }
                |> Effect.withNone

        SelectedCommunes ( communes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectCommunes

                communesUniques =
                    case communes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                communes

                newModel =
                    { model
                        | selectCommunes = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | communes = communesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectCommunes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectCommunes
            in
            ( { model | selectCommunes = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectCommune commune ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | communes =
                                                filters.communes
                                                    |> List.filter (\c -> c.id /= commune.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ReceivedEpcis resp ->
            let
                epcisSources =
                    resp
                        |> RD.withDefault []

                epcis =
                    model.filters.epcis

                newEpcis =
                    epcis
                        |> List.map
                            (\comm ->
                                epcisSources
                                    |> List.filter (.id >> (==) comm.id)
                                    |> List.head
                                    |> Maybe.withDefault comm
                            )
            in
            { model
                | epcis = resp
                , filters =
                    model.filters |> (\f -> { f | epcis = newEpcis })
            }
                |> Effect.withNone

        SelectedEpcis ( epcis, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectEpcis

                epcisUniques =
                    case epcis of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                epcis

                newModel =
                    { model
                        | selectEpcis = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | epcis = epcisUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectEpcis sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectEpcis
            in
            ( { model | selectEpcis = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectEpci epci ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | epcis =
                                                filters.epcis
                                                    |> List.filter (\c -> c.id /= epci.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedEffectifs ( effectifs, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectEffectifs

                effectifsUniques =
                    case effectifs of
                        [] ->
                            []

                        e :: rest ->
                            if List.member e rest then
                                rest

                            else
                                effectifs

                newModel =
                    { model
                        | selectEffectifs = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | effectifs = effectifsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectEffectifs sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectEffectifs
            in
            ( { model
                | selectEffectifs = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectedEffectifs effectifs ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | effectifs =
                                                filters.effectifs
                                                    |> List.filter ((/=) effectifs)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedRue ( apiRue, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectRueConfig model.selectRue

                newModel =
                    { model
                        | selectRue = updatedSelect
                        , filters =
                            model.filters
                                |> (\f -> { f | rue = Just apiRue })
                    }
            in
            ( newModel
            , Effect.batch [ Effect.fromCmd selectCmd, Effect.none ]
            )

        UpdatedSelectRue sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectRueConfig model.selectRue

                newModel =
                    { model | selectRue = updatedSelect }
            in
            ( newModel, Effect.batch [ Effect.fromCmd selectCmd, Effect.none ] )

        ClearRue ->
            let
                ( updatedSelectRue, selectCmd ) =
                    SingleSelectRemote.setText "" selectRueConfig model.selectRue

                newModel =
                    { model | filters = model.filters |> (\f -> { f | rue = Nothing }), selectRue = updatedSelectRue }
            in
            ( newModel, Effect.batch [ Effect.fromCmd selectCmd, Effect.none ] )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                activites

                newModel =
                    { model
                        | selectActivites = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | activites = activitesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | activites =
                                                filters.activites
                                                    |> List.filter ((/=) activite)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedDemandes ( demandes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectDemandes

                demandesUniques =
                    case demandes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                demandes

                newModel =
                    { model
                        | selectDemandes = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | demandes = demandesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectDemandes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectDemandes
            in
            ( { model
                | selectDemandes = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectDemande demande ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | demandes =
                                                filters.demandes
                                                    |> List.filter ((/=) demande)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedZones ( zones, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZones

                zonesUniques =
                    case zones of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                zones

                newModel =
                    { model
                        | selectZones = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | zones = zonesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectZones sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZones
            in
            ( { model
                | selectZones = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectZone zone ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | zones =
                                                filters.zones
                                                    |> List.filter ((/=) zone)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedMots ( mots, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectMots

                motsUniques =
                    case mots of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                mots

                newModel =
                    { model
                        | selectMots = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | mots = motsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | mots =
                                                filters.mots
                                                    |> List.filter ((/=) mot)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedQpvs ( qpvs, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectQpvs

                qpvsUniques =
                    case qpvs of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a.id <| List.map .id rest then
                                rest

                            else
                                qpvs

                newModel =
                    { model
                        | selectQpvs = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | qpvs = qpvsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectQpvs sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectQpvs
            in
            ( { model
                | selectQpvs = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectQpv qpv ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | qpvs =
                                                filters.qpvs
                                                    |> List.filter (\{ id } -> id /= qpv.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedZonages ( zonages, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZonages

                zonagesUniques =
                    case zonages of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a.id <| List.map .id rest then
                                rest

                            else
                                zonages

                newModel =
                    { model
                        | selectZonages = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | zonages = zonagesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectZonages sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZonages
            in
            ( { model
                | selectZonages = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectZonage zonage ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | zonages =
                                                filters.zonages
                                                    |> List.filter (\{ id } -> id /= zonage.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedZRR ( zrrs, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZrrs

                zrrsUniques =
                    case zrrs of
                        [] ->
                            []

                        z :: rest ->
                            if List.member z rest then
                                rest

                            else
                                zrrs

                newModel =
                    { model
                        | selectZrrs = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | zrrs = zrrsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectZRR sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZrrs
            in
            ( { model
                | selectZrrs = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectedZRR zrrs ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | zrrs =
                                                filters.zrrs
                                                    |> List.filter ((/=) zrrs)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SetESSFilter add ess ->
            let
                currentEss =
                    model.filters.ess

                isMember =
                    List.member ess currentEss

                newEss =
                    case ( add, isMember ) of
                        ( True, True ) ->
                            currentEss

                        ( True, False ) ->
                            currentEss ++ [ ess ]

                        ( False, True ) ->
                            currentEss
                                |> List.filter (\t -> t /= ess)

                        ( False, False ) ->
                            currentEss

                newModel =
                    { model | filters = model.filters |> (\f -> { f | ess = newEss }) }
            in
            ( newModel
            , Effect.none
            )

        ClickedOrderBy orderBy orderDirection ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | orderBy = orderBy, orderDirection = orderDirection }) }
            in
            ( newModel
            , updateUrl newModel
            )

        UpdateUrl ( filters, pagination ) ->
            ( model, updateUrl { filters = filters, pagination = pagination } )

        ClickedSearch ->
            ( model, updateUrl model )


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectCodesNafId : String
selectCodesNafId =
    "champ-selection-codes-naf"


selectCodesNafConfig : MultiSelectRemote.SelectConfig CodeNaf
selectCodesNafConfig =
    { headers = []
    , url = Api.rechercheCodeNaf
    , optionDecoder = Decode.list <| Data.Naf.decodeCodeNaf
    }


selectRueId : String
selectRueId =
    "champ-selection-rue"


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectDemandesId : String
selectDemandesId =
    "champ-selection-demandes"


selectZonesId : String
selectZonesId =
    "champ-selection-zones"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


selectQpvsId : String
selectQpvsId =
    "champ-selection-qpvs"


selectZonagesId : String
selectZonagesId =
    "champ-selection-zonages"


selectRueConfig : SingleSelectRemote.SelectConfig ApiStreet
selectRueConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiStreet
    }


getCommunes : Cmd Msg
getCommunes =
    Api.Auth.get
        { url = Api.getCommunes }
        SharedMsg
        ReceivedCommunes
        (Decode.list <| Data.Commune.decodeCommune)


getEpcis : Cmd Msg
getEpcis =
    Api.Auth.get
        { url = Api.getEpcisForPetr }
        SharedMsg
        ReceivedEpcis
        (Decode.list <| Decode.map2 Epci (Decode.field "id" Decode.string) (Decode.field "libEpci" Decode.string))


getFiltersSource : Cmd Msg
getFiltersSource =
    Api.Auth.get
        { url = Api.getFichesFiltersSource "etablissements" }
        SharedMsg
        ReceivedFiltersSource
        decodeFiltersSource


decodeFiltersSource : Decode.Decoder FiltersSource
decodeFiltersSource =
    Decode.succeed FiltersSource
        |> andMap (Decode.field "activites" <| Decode.list Decode.string)
        |> andMap (Decode.field "demandes" <| Decode.list Data.Demande.decodeTypeDemande)
        |> andMap (Decode.field "zones" <| Decode.list Decode.string)
        |> andMap (Decode.field "mots" <| Decode.list Decode.string)
        |> andMap
            (Decode.field "qpvs" <|
                Decode.list <|
                    Decode.map2 Qpv
                        (Decode.field "id" Decode.int)
                        (Decode.field "nom" Decode.string)
            )
        |> andMap
            (Decode.field "zonages" <|
                Decode.list <|
                    Decode.map2 Zonage
                        (Decode.field "id" Decode.int)
                        (Decode.field "nom" Decode.string)
            )


rechercherEtablissementInputName : String
rechercherEtablissementInputName =
    "rechercher-etablissement"


setEtats : Filters -> Bool -> EtatAdministratif -> List EtatAdministratif
setEtats filters add etat =
    let
        etats =
            filters.etat

        isMember =
            List.member etat etats
    in
    case ( add, isMember ) of
        ( True, True ) ->
            etats

        ( True, False ) ->
            etats ++ [ etat ]

        ( False, True ) ->
            etats
                |> List.filter (\et -> et /= etat)

        ( False, False ) ->
            etats


listFiltersTagsId : String
listFiltersTagsId =
    "liste-filters-tags"


updateUrl : { data | filters : Filters, pagination : Pagination } -> Effect.Effect Shared.Msg Msg
updateUrl { filters, pagination } =
    Effect.fromShared <|
        Shared.Navigate <|
            Route.Etablissements <|
                Just <|
                    filtersAndPaginationToQuery ( filters, pagination )


{-| This tells us that, even if the url is not "clean",
we are showing unfiltered results, though they may be ordered.
-}
showingUnfilteredResults : String -> Bool
showingUnfilteredResults currentUrl =
    List.member currentUrl
        [ ""
        , "d=" ++ orderDirectionToString Asc
        , "o=" ++ orderByToString EtablissementConsultation
        , "o=" ++ orderByToString EtablissementCreation
        , "d=" ++ orderDirectionToString Asc ++ "&" ++ "o=" ++ orderByToString EtablissementConsultation
        , "d=" ++ orderDirectionToString Asc ++ "&" ++ "o=" ++ orderByToString EtablissementCreation
        ]


codeToOrderBy : String -> Msg
codeToOrderBy code =
    let
        match ob od () =
            if code == (orderByToString ob ++ orderDirectionToString od) then
                Just ( ob, od )

            else
                Nothing

        or : (() -> Maybe data) -> Maybe data -> Maybe data
        or orElse maybe =
            case maybe of
                Just m ->
                    Just m

                Nothing ->
                    orElse ()
    in
    match EtablissementConsultation Desc ()
        |> or (match EtablissementConsultation Asc)
        |> or (match EtablissementCreation Desc)
        |> or (match EtablissementCreation Asc)
        -- |> or (match Alphabetical Desc)
        -- |> or (match Alphabetical Asc)
        |> Maybe.withDefault ( EtablissementConsultation, Desc )
        |> (\( ob, od ) -> ClickedOrderBy ob od)


selectOrder : { a | orderBy : OrderBy, orderDirection : OrderDirection } -> Html Msg
selectOrder { orderBy, orderDirection } =
    let
        toOption ob od lab =
            option
                [ Attr.value <| orderByToString ob ++ orderDirectionToString od
                , Attr.selected <| ( orderBy, orderDirection ) == ( ob, od )
                ]
                [ text lab ]
    in
    select
        [ class "fr-select"
        , Attr.id "select-sort"
        , Attr.name "select-sortBy"
        , Events.onInput codeToOrderBy
        ]
        [ toOption EtablissementConsultation Desc "Derniers consultés"
        , toOption EtablissementCreation Desc "Derniers créés (base SIRENE)"

        -- , toOption EtablissementConsultation Asc "Établissements consultés récemment en dernier"
        -- , toOption EtablissementCreation Asc "Établissements créés récemment en dernier"
        -- , toOption Alphabetical Asc "Noms classés par ordre alphabétique"
        -- , toOption Alphabetical Desc "Noms classés par ordre alphabétique inversé"
        ]


forcePagination : a -> { b | pagination : a } -> { b | pagination : a }
forcePagination pagination model =
    { model | pagination = pagination }


doSearch : ( Filters, Pagination ) -> Model -> ( Model, Effect.Effect sharedMsg Msg )
doSearch ( filters, pagination ) model =
    let
        nextUrl =
            filtersAndPaginationToQuery ( filters, pagination )

        requestChanged =
            nextUrl /= model.currentUrl
    in
    if requestChanged then
        let
            communesSources =
                model.communes
                    |> RD.withDefault []

            communes =
                filters.communes

            newCommunes =
                hydrateData .id communesSources communes

            epcisSources =
                model.epcis
                    |> RD.withDefault []

            epcis =
                filters.epcis

            newEpcis =
                hydrateData .id epcisSources epcis

            qpvsSources =
                model.filtersSource
                    |> RD.map .qpvs
                    |> RD.withDefault []

            qpvs =
                filters.qpvs

            newQpvs =
                hydrateData .id qpvsSources qpvs

            zonagesSources =
                model.filtersSource
                    |> RD.map .zonages
                    |> RD.withDefault []

            zonages =
                filters.zonages

            newZonages =
                hydrateData .id zonagesSources zonages

            effectifsIds =
                filters.effectifs
                    |> List.map .id

            newEffectifs =
                Data.Effectifs.tranchesEffectifs
                    |> List.filter (\{ id } -> List.member id effectifsIds)

            zrrsIds =
                filters.zrrs
                    |> List.map .id

            newZrrs =
                Data.ZRR.zrrs
                    |> List.filter (\{ id } -> List.member id zrrsIds)

            currentApiStreet =
                model.filters.rue

            apiStreet =
                case currentApiStreet of
                    Nothing ->
                        filters.rue

                    Just current ->
                        filters.rue
                            |> Maybe.andThen
                                (\rue ->
                                    if rue.name == current.name && rue.postcode == current.postcode then
                                        Just current

                                    else
                                        filters.rue
                                )

            ( updatedSelect, selectCmd ) =
                SingleSelectRemote.setText
                    (apiStreet
                        |> Maybe.map .label
                        |> Maybe.withDefault ""
                    )
                    selectRueConfig
                    model.selectRue
        in
        ( { model
            | pagination = pagination
            , filters =
                { filters
                    | communes = newCommunes
                    , epcis = newEpcis
                    , qpvs = newQpvs
                    , zonages = newZonages
                    , effectifs = newEffectifs
                    , zrrs = newZrrs
                    , rue = apiStreet
                }
            , currentUrl = nextUrl
            , selectRue = updatedSelect
          }
        , Effect.fromCmd selectCmd
        )

    else
        ( model, Effect.none )
