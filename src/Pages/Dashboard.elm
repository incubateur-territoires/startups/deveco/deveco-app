module Pages.Dashboard exposing (Model, Msg, page)

import Accessibility exposing (Html, decorativeImg, div, h2, hr, li, p, span, text, ul)
import Api
import Api.Auth
import DSFR.Button
import DSFR.Card
import DSFR.Grid
import DSFR.Tag
import DSFR.Tile
import DSFR.Typography as Typo exposing (link)
import Data.Demande
import Data.Effectifs
import Data.Etablissement
import Data.Role
import Data.Territoire
import Date exposing (Date)
import Dict
import Effect
import Html
import Html.Attributes exposing (class, style)
import Html.Events
import Html.Extra exposing (nothing, viewIf)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (formatNumberWithThousandSpacing, numberCard, plural)
import Lib.Variables exposing (contactEmail)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import Time
import UI.Layout
import Url.Builder as Builder
import User
import View exposing (View)


type alias Model =
    { stats : WebData Stats
    , viewActive : Bool
    }


type alias Stats =
    { nouveautes : Int
    , etablissementsOuverts : List EtablissementLight
    , etablissementsFermes : List EtablissementLight
    , rappelsCrees : List RappelLight
    , etablissementsEtiquetes : Int
    , contacts : Int
    , demandes : List DemandeLight
    }


type alias EtablissementLight =
    { siret : String
    , nom : String
    , enseigne : Maybe String
    , trancheEffectifs : String
    }


type alias RappelLight =
    { titre : String
    , siret : String
    , nom : String
    , dateRappel : Date
    }


type alias DemandeLight =
    { type_ : Data.Demande.TypeDemande
    }


type Msg
    = ReceivedStats (WebData Stats)
    | ToggleView
    | SharedMsg Shared.Msg


page : Shared.Shared -> Page () Shared.Msg (View Msg) Model Msg
page { identity, now, timezone } =
    Spa.Page.element
        { view = view now timezone identity
        , init = init identity
        , update = update
        , subscriptions = \_ -> Sub.none
        }


init : Maybe User -> () -> ( Model, Effect.Effect Shared.Msg Msg )
init identity () =
    ( { stats = RD.Loading
      , viewActive = True
      }
    , case identity of
        Nothing ->
            Effect.fromSharedCmd <| Api.Auth.checkAuth True Nothing

        Just user ->
            if Data.Role.isDeveco <| User.role user then
                Effect.fromCmd getStats

            else
                Effect.fromShared <|
                    Shared.ReplaceUrl <|
                        Route.defaultPageForRole <|
                            User.role user
    )
        |> Shared.pageChangeEffects


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        ReceivedStats response ->
            { model | stats = response }
                |> Effect.withNone

        ToggleView ->
            { model | viewActive = not model.viewActive }
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg


view : Time.Posix -> Time.Zone -> Maybe User -> Model -> View Msg
view now timezone identity model =
    { title =
        UI.Layout.pageTitle <|
            Maybe.withDefault "Outil pour développeur économique" <|
                Maybe.map (\_ -> "Tableau de bord") <|
                    identity
    , body = [ body now timezone identity model ]
    , route = Route.Dashboard
    }


body : Time.Posix -> Time.Zone -> Maybe User -> Model -> Html Msg
body now timezone identity model =
    identity
        |> Maybe.map (viewUserDashboard now timezone model)
        |> Maybe.withDefault viewLanding


viewLanding : Html msg
viewLanding =
    div [ class "flex flex-col fr-card--white" ]
        [ div [ DSFR.Grid.gridRow, class "p-4 sm:p-8" ]
            [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col3, class "" ]
                [ decorativeImg [ Html.Attributes.src "/assets/deveco-accueil.svg" ] ]
            , div [ DSFR.Grid.col7, class "flex" ]
                [ div [ class "self-center" ]
                    [ h2 [ Typo.fr_h4 ] [ text "Bienvenue sur Deveco\u{00A0}!" ]
                    , p [] [ text "Ce service facilite l'accès et la gestion des données des entreprises pour les développeurs économiques au sein des collectivités locales." ]
                    , DSFR.Button.new { label = "Accéder à mon compte", onClick = Nothing }
                        |> DSFR.Button.linkButton (Route.toUrl <| Route.Connexion Nothing)
                        |> DSFR.Button.primary
                        |> DSFR.Button.view
                    ]
                ]
            ]
        , div [ DSFR.Grid.gridRow, class "fr-background-contrast--blue-ecume p-8 sm:p-16" ]
            [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col6 ]
                [ text "Gagnez en efficacité pour mieux accompagner le développement économique de votre territoire." ]
            , div [ DSFR.Grid.colOffset1, DSFR.Grid.col3 ]
                [ let
                    emails =
                        [ contactEmail ]

                    subject =
                        Builder.string "subject" <|
                            "Demande d'information sur Dévéco"

                    mailto =
                        "mailto:"
                            ++ String.join "," emails
                            ++ Builder.toQuery [ subject ]
                  in
                  DSFR.Button.new { label = "Contactez-nous", onClick = Nothing }
                    |> DSFR.Button.linkButton mailto
                    |> DSFR.Button.view
                ]
            ]
        , div [ class "p-4 sm:p-8" ]
            [ div [ DSFR.Grid.gridRow, class "p-2 sm:p-4" ]
                [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col10, class "fr-card--white" ]
                    [ div [ DSFR.Grid.gridRow, class "flex items-end justify-end" ]
                        [ div [ DSFR.Grid.col4, class "px-8" ]
                            [ decorativeImg [ Html.Attributes.src "/assets/deveco-information.svg" ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ decorativeImg [ Html.Attributes.src "/assets/deveco-connaissance.svg" ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ decorativeImg [ Html.Attributes.src "/assets/deveco-pilotage.svg" ]
                            ]
                        ]
                    ]
                ]
            , div [ DSFR.Grid.gridRow, class "p-2 sm:p-4" ]
                [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col10, class "fr-card--white" ]
                    [ div [ DSFR.Grid.gridRow ]
                        [ div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [ Typo.textBold ] [ text "Partage d’infos entre agents d’une même équipe" ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [ Typo.textBold ] [ text "Connaissance du tissu économique du territoire" ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [ Typo.textBold ] [ text "Pilotage de l’activité" ] ]
                            ]
                        ]
                    ]
                ]
            , div [ DSFR.Grid.gridRow, class "p-2 sm:p-4" ]
                [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col10, class "fr-card--white" ]
                    [ div [ DSFR.Grid.gridRow ]
                        [ div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [] [ text "Centralisez le suivi de la relation aux établissements et créateurs d'entreprises du territoire. Gérez les locaux d’activité vacants." ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [] [ text "Explorez le tissu économique local à partir des données fiables et actualisées des institutions publiques." ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [] [ text "Suivez et analysez l’activité des services de développement économique." ] ]
                            ]
                        ]
                    ]
                ]
            ]
        ]


viewUserDashboard : Time.Posix -> Time.Zone -> Model -> User -> Html Msg
viewUserDashboard now timezone model user =
    div [ class "p-2" ] <|
        List.singleton <|
            div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col12, DSFR.Grid.colSm2 ]
                    [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                        [ div [ DSFR.Grid.col12 ] [ viewRoleAndTerritory user ]
                        , div [ DSFR.Grid.col12 ]
                            [ DSFR.Tile.vertical
                                { title = text "Gestion des collaborateurs"
                                , description = Nothing
                                , link = Route.toUrl <| Route.ParamsCollaborateurs
                                }
                                |> DSFR.Tile.view
                            ]
                        , div [ DSFR.Grid.col12 ]
                            [ DSFR.Tile.vertical
                                { title = text "Gestion des étiquettes"
                                , description = Nothing
                                , link = Route.toUrl <| Route.ParamsTags
                                }
                                |> DSFR.Tile.view
                            ]
                        , div [ DSFR.Grid.col12 ]
                            [ DSFR.Tile.vertical
                                { title = text "Import de mes données"
                                , description = Nothing
                                , link = Route.toUrl <| Route.ParamsImport
                                }
                                |> DSFR.Tile.view
                            ]
                        , let
                            nouveautes =
                                model.stats
                                    |> RD.map .nouveautes
                                    |> RD.withDefault 0
                          in
                          div [ DSFR.Grid.col12 ]
                            [ DSFR.Tile.vertical
                                { title =
                                    p []
                                        [ text "Nouveautés et évolutions de Deveco"
                                        , text " "
                                        , if nouveautes > 0 then
                                            span [ class "circled" ]
                                                [ text <| formatNumberWithThousandSpacing <| nouveautes
                                                ]

                                          else
                                            nothing
                                        ]
                                , description = Nothing
                                , link = Lib.Variables.hrefNouveautes
                                }
                                |> DSFR.Tile.isExternal
                                |> DSFR.Tile.view
                            ]
                        ]
                    ]
                , div [ DSFR.Grid.col12, DSFR.Grid.colSm10 ]
                    [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                        [ div [ DSFR.Grid.col12 ]
                            [ viewStats (Date.fromPosix timezone now) model.viewActive model.stats
                            ]
                        ]
                    ]
                ]


viewRoleAndTerritory : User -> Html msg
viewRoleAndTerritory user =
    let
        greetings =
            "Bonjour " ++ User.prenom user
    in
    DSFR.Card.card greetings DSFR.Card.vertical
        |> DSFR.Card.withDescription
            (user
                |> User.role
                |> Data.Role.territoire
                |> Maybe.map
                    (\territoire ->
                        [ span [] [ text <| "Votre territoire de référence est\u{00A0}:" ]
                        , span [ Typo.textBold ] [ text <| Data.Territoire.nom territoire ]
                        ]
                    )
                |> Maybe.withDefault
                    [ text "Vous êtes Superadmin"
                    ]
                |> div [ class "flex flex-col" ]
                |> Just
            )
        |> DSFR.Card.view


numberCardWithPlaceholderDefault : WebData data -> (data -> prop) -> (prop -> Html Msg) -> Html Msg
numberCardWithPlaceholderDefault webdata accessor viewer =
    case RD.map accessor webdata of
        RD.Success data ->
            viewer data

        _ ->
            placeholderCard


viewStats : Date -> Bool -> WebData Stats -> Html Msg
viewStats now viewActive stats =
    case stats of
        RD.Failure _ ->
            text "Une erreur s'est produite, veuillez réessayer."

        _ ->
            div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
                [ div [ DSFR.Grid.col12, DSFR.Grid.colSm8 ]
                    [ div [ class "flex flex-col gap-4 px-4" ]
                        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "flex flex-col gap-4" ]
                            [ div [ DSFR.Grid.col12 ]
                                [ h2 [ Typo.fr_h4, class "!mb-0" ] [ text "Actualités du service" ]
                                , text "sur les 5 derniers jours"
                                ]
                            , div []
                                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                                    [ div [ DSFR.Grid.col3 ]
                                        [ numberCardWithPlaceholderDefault
                                            stats
                                            .contacts
                                            (\contacts ->
                                                numberCard []
                                                    ""
                                                    [ "nouveau"
                                                    , if contacts > 1 then
                                                        "x"

                                                      else
                                                        ""
                                                    , " "
                                                    , "contact"
                                                    , plural <| contacts
                                                    , " "
                                                    , "qualifié"
                                                    , plural <| contacts
                                                    ]
                                                    contacts
                                            )
                                        ]
                                    , div [ DSFR.Grid.col3 ]
                                        [ numberCardWithPlaceholderDefault
                                            stats
                                            .demandes
                                            (\demandes ->
                                                numberCard []
                                                    ""
                                                    [ "nouvelle"
                                                    , plural <| List.length <| demandes
                                                    , " demande"
                                                    , plural <| List.length <| demandes
                                                    ]
                                                    (List.length <| demandes)
                                            )
                                        ]
                                    , div [ DSFR.Grid.col3 ]
                                        [ numberCardWithPlaceholderDefault
                                            stats
                                            .etablissementsEtiquetes
                                            (\etablissementsEtiquetes ->
                                                numberCard []
                                                    ""
                                                    [ "établiss."
                                                    , " étiqueté"
                                                    , plural <| etablissementsEtiquetes
                                                    ]
                                                    etablissementsEtiquetes
                                            )
                                        ]
                                    , div [ DSFR.Grid.col3 ]
                                        [ numberCardWithPlaceholderDefault
                                            stats
                                            .rappelsCrees
                                            (\rappelsCrees ->
                                                numberCard []
                                                    ""
                                                    [ "rappel"
                                                    , plural <| List.length <| rappelsCrees
                                                    , " créé"
                                                    , plural <| List.length <| rappelsCrees
                                                    ]
                                                    (List.length <| rappelsCrees)
                                            )
                                        ]
                                    ]
                                ]
                            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                                [ div [ DSFR.Grid.col12 ]
                                    [ div [ class "fr-card--white p-2 flex flex-col gap-4" ]
                                        [ div [ class "flex flex-row justify-between" ]
                                            [ h2 [ Typo.fr_h6, class "!mb-0" ] [ text "Mes rappels créés" ]
                                            , DSFR.Button.new { label = "Voir tout", onClick = Nothing }
                                                |> DSFR.Button.linkButton (Route.toUrl <| Route.Rappels)
                                                |> DSFR.Button.view
                                            ]
                                        , case RD.map .rappelsCrees stats of
                                            RD.Success rappelsCrees ->
                                                if List.length rappelsCrees == 0 then
                                                    span [ class "italic" ] [ text "Aucun rappel créé ces 5 derniers jours" ]

                                                else
                                                    ul [] <|
                                                        List.map (viewRappelLight now >> List.singleton >> li []) <|
                                                            List.sortWith (Lib.Date.sortOlderDateFirst .dateRappel) <|
                                                                rappelsCrees

                                            _ ->
                                                div [ class "flex flex-col gap-2" ] [ greyPlaceholder 10, greyPlaceholder 14 ]
                                        ]
                                    ]
                                ]
                            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                                [ div [ DSFR.Grid.col12 ]
                                    [ div [ class "fr-card--white p-2 flex flex-col gap-4" ]
                                        [ div [ class "flex flex-col" ]
                                            [ h2 [ Typo.fr_h6, class "!mb-0" ] [ text "Dernières demandes créées" ]
                                            , span [ class "italic" ] [ text "établissements et créateurs" ]
                                            ]
                                        , case RD.map .demandes stats of
                                            RD.Success demandes ->
                                                let
                                                    stackedDemandes =
                                                        demandes
                                                            |> List.foldl
                                                                (\{ type_ } acc ->
                                                                    Dict.update (Data.Demande.typeDemandeToDisplay type_)
                                                                        (Maybe.map ((+) 1 >> Just) >> Maybe.withDefault (Just 1))
                                                                        acc
                                                                )
                                                                Dict.empty
                                                            |> Dict.toList
                                                in
                                                case stackedDemandes of
                                                    [] ->
                                                        span [ class "italic" ] [ text "Aucune demande créée ces 5 derniers jours" ]

                                                    _ ->
                                                        Html.node "chart-pie"
                                                            [ Encode.object
                                                                [ ( "values"
                                                                  , stackedDemandes
                                                                        |> Encode.list
                                                                            (\( type_, count ) ->
                                                                                Encode.object
                                                                                    [ ( "label", Encode.string <| type_ )
                                                                                    , ( "count", Encode.float <| toFloat count )
                                                                                    ]
                                                                            )
                                                                  )
                                                                , ( "config"
                                                                  , Encode.object
                                                                        [ ( "title", Encode.string "test" )
                                                                        ]
                                                                  )
                                                                ]
                                                                |> Encode.encode 0
                                                                |> Html.Attributes.attribute "data"
                                                            ]
                                                            []

                                            _ ->
                                                div [ class "flex flex-col gap-2" ] [ greyPlaceholder 10, greyPlaceholder 14 ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                , div [ DSFR.Grid.col12, DSFR.Grid.colSm4 ]
                    [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "fr-card--white flex flex-col" ]
                        [ div [ DSFR.Grid.col12 ]
                            [ h2 [ Typo.fr_h4, class "!mb-0" ] [ text "Actualités du territoire" ]
                            , text "sur les 5 derniers jours"
                            ]
                        , div [ DSFR.Grid.col12 ]
                            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                                [ Html.div
                                    (DSFR.Grid.col6
                                        :: (if viewActive then
                                                []

                                            else
                                                [ Html.Events.onClick ToggleView, style "cursor" "pointer" ]
                                           )
                                    )
                                    [ numberCardWithPlaceholderDefault
                                        stats
                                        .etablissementsOuverts
                                        (\etablissementsOuverts ->
                                            numberCard
                                                (if viewActive then
                                                    [ class "border-b-8 dark-blue-border" ]

                                                 else
                                                    []
                                                )
                                                ""
                                                [ "établiss."
                                                , " arrivé"
                                                , plural <| List.length <| etablissementsOuverts
                                                ]
                                                (List.length <| etablissementsOuverts)
                                        )
                                    ]
                                , Html.div
                                    (DSFR.Grid.col6
                                        :: (if not viewActive then
                                                []

                                            else
                                                [ Html.Events.onClick ToggleView, style "cursor" "pointer" ]
                                           )
                                    )
                                    [ numberCardWithPlaceholderDefault
                                        stats
                                        .etablissementsFermes
                                        (\etablissementsFermes ->
                                            numberCard
                                                (if not viewActive then
                                                    [ class "border-b-8 dark-blue-border" ]

                                                 else
                                                    []
                                                )
                                                ""
                                                [ "établiss."
                                                , " fermé"
                                                , plural <| List.length <| etablissementsFermes
                                                ]
                                                (List.length <| etablissementsFermes)
                                        )
                                    ]
                                , div
                                    [ class "triangle dark-blue-border"
                                    , style "margin-left" <|
                                        "calc( "
                                            ++ (if viewActive then
                                                    "2"

                                                else
                                                    "7"
                                               )
                                            ++ "5% - 14px)"
                                    ]
                                    []
                                , hr [ class "fr-hr h-[1px] w-full !border-solid dark-blue-border !border-2 !p-0" ] []
                                ]
                            ]
                        , viewIf (viewActive && RD.isSuccess stats) <|
                            div [ DSFR.Grid.col12, class "flex flex-col gap-2" ]
                                [ h2 [ Typo.fr_h6, class "!mb-0" ]
                                    [ text "Arrivées sur le territoire"
                                    ]
                                , case RD.withDefault [] <| RD.map .etablissementsOuverts <| stats of
                                    [] ->
                                        span [ class "italic" ] [ text "Aucune arrivée sur le territoire" ]

                                    etablissementsOuverts ->
                                        ul [] <|
                                            List.map (viewEtablissementLight True >> List.singleton >> li []) <|
                                                etablissementsOuverts
                                ]
                        , viewIf (not viewActive && RD.isSuccess stats) <|
                            div [ DSFR.Grid.col12, class "flex flex-col gap-2" ]
                                [ h2 [ Typo.fr_h6, class "!mb-0" ]
                                    [ text "Fermetures sur le territoire"
                                    ]
                                , case RD.withDefault [] <| RD.map .etablissementsFermes <| stats of
                                    [] ->
                                        span [ class "italic" ] [ text "Aucune fermeture sur le territoire" ]

                                    etablissementsFermes ->
                                        ul [] <|
                                            List.map (viewEtablissementLight False >> List.singleton >> li []) <|
                                                etablissementsFermes
                                ]
                        ]
                    ]
                ]


viewEtablissementLight : Bool -> EtablissementLight -> Html msg
viewEtablissementLight active etablissement =
    div [ class "flex flex-col gap-2 " ]
        [ span []
            [ link (Route.toUrl <| Route.Etablissement etablissement.siret)
                [ class <|
                    if active then
                        "text-france-blue"

                    else
                        "text-marianne-red"
                ]
                [ text <|
                    String.toUpper <|
                        Data.Etablissement.displayNomEnseigne etablissement
                ]
            ]
        , DSFR.Tag.unclickable
            { data =
                Maybe.withDefault "Effectifs inconnus" <|
                    Data.Effectifs.trancheToLabel <|
                        etablissement.trancheEffectifs
            , toString = identity
            }
            |> DSFR.Tag.oneSmall
        ]


viewRappelLight : Date -> RappelLight -> Html msg
viewRappelLight now { titre, siret, nom, dateRappel } =
    let
        jours =
            Date.diff Date.Days now dateRappel
    in
    div []
        [ div []
            [ link (Route.toUrl <| Route.Etablissement siret) [ class "blue-text" ] [ text nom ]
            , text " - "
            , if jours == 0 then
                text "aujourd'hui"

              else
                span []
                    [ text <|
                        if jours < 0 then
                            "il y a "

                        else
                            "dans "
                    , text <| String.fromInt <| abs <| jours
                    , text " jour"
                    , text <| plural <| jours
                    ]
            ]
        , div [] [ text titre ]
        ]


getStats : Cmd Msg
getStats =
    Api.Auth.get
        { url = Api.getStats }
        SharedMsg
        ReceivedStats
        decodeStats


decodeStats : Decoder Stats
decodeStats =
    Decode.succeed Stats
        |> andMap (Decode.map (Maybe.withDefault 0) <| optionalNullableField "nouveautes" <| Decode.int)
        |> andMap (Decode.field "etablissementsOuverts" <| Decode.list etablissementLightDecoder)
        |> andMap (Decode.field "etablissementsFermes" <| Decode.list etablissementLightDecoder)
        |> andMap (Decode.field "rappelsCrees" <| Decode.list rappelLightDecoder)
        |> andMap (Decode.field "etablissementsEtiquetes" <| Decode.int)
        |> andMap (Decode.field "contacts" <| Decode.int)
        |> andMap (Decode.field "demandes" <| Decode.list demandeLightDecoder)


etablissementLightDecoder : Decoder EtablissementLight
etablissementLightDecoder =
    Decode.succeed EtablissementLight
        |> andMap (Decode.field "siret" Decode.string)
        |> andMap (Decode.field "nomPublic" Decode.string)
        |> andMap (optionalNullableField "nomEnseigne" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "NN") <| Decode.field "trancheEffectifs" <| Decode.nullable Decode.string)


rappelLightDecoder : Decoder RappelLight
rappelLightDecoder =
    Decode.succeed RappelLight
        |> andMap (Decode.field "titre" Decode.string)
        |> andMap (Decode.field "siret" Decode.string)
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "dateRappel" <| Lib.Date.decodeCalendarDate)


demandeLightDecoder : Decoder DemandeLight
demandeLightDecoder =
    Decode.succeed DemandeLight
        |> andMap (Decode.field "typeDemande" <| Data.Demande.decodeTypeDemande)


placeholderCard : Html msg
placeholderCard =
    DSFR.Card.card "" DSFR.Card.vertical
        |> DSFR.Card.withArrow False
        |> DSFR.Card.withNoTitle
        |> DSFR.Card.withDescription
            (Just <|
                div [ class "text-center" ] <|
                    [ div [ Typo.textBold, Typo.fr_h1 ] <|
                        [ greyPlaceholder 1
                        ]
                    , greyPlaceholder 4
                    , greyPlaceholder 4
                    ]
            )
        |> DSFR.Card.view


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "grey-background m-1" ]
        |> List.singleton
        |> div [ class "pulse-black" ]
