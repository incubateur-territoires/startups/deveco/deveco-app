module UI.Layout exposing (MenuItem, layout, lazyBody, pageTitle, parametresSideMenu)

import Accessibility exposing (Html, a, br, button, div, header, li, main_, nav, p, text, ul)
import Accessibility.Aria as Aria
import Accessibility.Landmark exposing (navigation)
import Accessibility.Role as Role
import DSFR.Footer
import DSFR.Grid as Grid
import DSFR.Icons.System
import DSFR.Icons.User
import DSFR.Typography as Typo
import Data.Role
import Html.Attributes as Attr exposing (class, style)
import Html.Attributes.Extra
import Html.Events as Events
import Html.Lazy
import Route exposing (Route)
import Shared exposing (Shared)
import Spa
import UI.Avertissement
import UI.Tutoriel
import User


lazyBody : (model -> Html msg) -> model -> List (Html msg)
lazyBody body model =
    Html.Lazy.lazy body model
        |> List.singleton


pageTitle : String -> String
pageTitle t =
    t ++ " - Deveco"


layout : Shared -> Route -> List (Html (Spa.Msg Shared.Msg msg)) -> List (Html (Spa.Msg Shared.Msg msg))
layout shared currentRoute children =
    [ head shared <|
        menuForUser currentRoute shared.identity
    , container children
    , DSFR.Footer.simple shared |> Accessibility.map Spa.mapSharedMsg
    , UI.Tutoriel.viewModal shared.newFeatures |> Accessibility.map (Shared.Tutoriel >> Spa.mapSharedMsg)
    , UI.Avertissement.viewModal shared.avertissement |> Accessibility.map (Shared.Avertissement >> Spa.mapSharedMsg)
    ]


container : List (Html msg) -> Html msg
container =
    main_
        [ Grid.container
        , class "fr-p-2w"
        , style "min-height" "calc(100vh - 400px)"
        ]


isCurrentRoute : Route -> Route -> Bool
isCurrentRoute current route =
    route
        == current
        || (case ( current, route ) of
                ( Route.Locaux _, Route.Locaux _ ) ->
                    True

                ( Route.Local _, Route.Locaux _ ) ->
                    True

                ( Route.LocalNew, Route.Locaux _ ) ->
                    True

                ( Route.Etablissements _, Route.Etablissements _ ) ->
                    True

                ( Route.Etablissement _, Route.Etablissements _ ) ->
                    True

                ( Route.EtablissementNew, Route.Etablissements _ ) ->
                    True

                ( Route.Createurs _, Route.Createurs _ ) ->
                    True

                ( Route.Createur _, Route.Createurs _ ) ->
                    True

                ( Route.CreateurNew, Route.Createurs _ ) ->
                    True

                ( Route.CreateurEdit _, Route.Createurs _ ) ->
                    True

                ( Route.Stats _, Route.Stats _ ) ->
                    True

                _ ->
                    False
           )


menuForUser : Route -> Maybe Shared.User -> List (MenuItem msg)
menuForUser current maybeUser =
    case maybeUser of
        Nothing ->
            [ MenuItem (text "Accueil") False (Just ()) <| Just <| "/"
            , MenuItem (text "Statistiques") (isCurrentRoute current <| Route.Stats Nothing) Nothing <| Just <| Route.toUrl <| Route.Stats Nothing
            ]

        Just user ->
            if Data.Role.isDeveco <| User.role user then
                [ MenuItem (text "Accueil") (isCurrentRoute current <| Route.Dashboard) Nothing <| Just <| Route.toUrl <| Route.Dashboard
                , MenuItem (text "Établissements") (isCurrentRoute current <| Route.Etablissements Nothing) Nothing <| Just <| Route.toUrl <| Route.Etablissements Nothing
                , MenuItem (text "Créateurs d'entreprise") (isCurrentRoute current <| Route.Createurs Nothing) Nothing <| Just <| Route.toUrl <| Route.Createurs Nothing
                , MenuItem (text "Immobilier") (isCurrentRoute current <| Route.Locaux Nothing) Nothing <| Just <| Route.toUrl <| Route.Locaux Nothing
                , MenuItem (text "Mes rappels") (isCurrentRoute current <| Route.Rappels) Nothing <| Just <| Route.toUrl <| Route.Rappels
                , MenuItem (text "Analyse territoriale") (isCurrentRoute current <| Route.Analysis) Nothing <| Just <| Route.toUrl <| Route.Analysis
                , MenuItem (text "Activité du service") (isCurrentRoute current <| Route.Activite Nothing) Nothing <| Just <| Route.toUrl <| Route.Activite Nothing
                ]

            else
                [ MenuItem (text "Utilisateurs") (isCurrentRoute current <| Route.AdminUtilisateurs) Nothing <| Just <| Route.toUrl <| Route.AdminUtilisateurs
                , MenuItem (text "Territoires") (isCurrentRoute current <| Route.AdminTerritoires) Nothing <| Just <| Route.toUrl <| Route.AdminTerritoires
                , MenuItem (text "Étiquettes") (isCurrentRoute current <| Route.AdminTags) Nothing <| Just <| Route.toUrl <| Route.AdminTags
                ]


head : Shared -> List (MenuItem (Spa.Msg Shared.Msg msg)) -> Html (Spa.Msg Shared.Msg msg)
head shared menuItems =
    header
        [ Attr.attribute "role" "banner"
        , Attr.id "top"
        , class "fr-header"
        ]
        [ div
            [ class "fr-header__body"
            ]
            [ div
                [ Grid.container
                ]
                [ div
                    [ class "fr-header__body-row"
                    ]
                    [ div
                        [ class "fr-header__brand fr-enlarge-link"
                        ]
                        [ div
                            [ class "fr-header__brand-top"
                            ]
                            [ div
                                [ class "fr-header__logo"
                                ]
                                [ Typo.linkStandalone Typo.linkNoIcon
                                    "/"
                                    [ Attr.title "Accueil - Deveco"
                                    , Attr.target "_self"
                                    ]
                                    [ p
                                        [ class "fr-logo"
                                        ]
                                        [ text "Agence"
                                        , br []
                                        , text "Nationale"
                                        , br []
                                        , text "de la Cohésion"
                                        , br []
                                        , text "des Territoires"
                                        ]
                                    ]
                                ]
                            , div
                                [ class "fr-header__operator"
                                ]
                                []
                            , div
                                [ class "fr-header__navbar"
                                ]
                                [ button
                                    [ class "fr-btn--menu fr-btn"
                                    , Attr.attribute "aria-controls" modalMenuId
                                    , Attr.attribute "aria-haspopup" "menu"
                                    , Attr.title "Menu"
                                    , Attr.id modalMenuMobileTitle
                                    , Events.onClick <| Spa.mapSharedMsg <| Shared.ToggleHeaderMenu
                                    ]
                                    [ text "Menu" ]
                                ]
                            ]
                        , div
                            [ class "fr-header__service"
                            ]
                            [ Typo.linkStandalone Typo.linkNoIcon
                                "/"
                                [ Attr.title "Accueil - Deveco"
                                , Attr.target "_self"
                                ]
                                [ p
                                    [ class "fr-header__service-title"
                                    ]
                                    [ text "Deveco"
                                    ]
                                ]
                            , p
                                [ class "fr-header__service-tagline"
                                ]
                                [ text "Faciliter l'accès et la gestion des données entreprises pour les collectivités" ]
                            ]
                        ]
                    , div
                        [ class "fr-header__tools"
                        ]
                        [ div
                            [ class "fr-header__tools-links"
                            ]
                            [ ul [ class "fr-links-group" ] <|
                                case shared.identity of
                                    Nothing ->
                                        [ loginLink
                                        ]

                                    Just _ ->
                                        [ profileAccess
                                        , logoutLink
                                        ]
                            ]
                        ]
                    ]
                ]
            ]
        , mobileMenu shared menuItems
        , div
            [ class "fr-header__menu fr-modal"
            , Attr.id modalMenuId
            ]
            [ div
                [ Grid.container
                ]
                [ div
                    []
                    [ nav
                        [ class "fr-nav flex"
                        , Attr.id "header-navigation"
                        , navigation
                        , Aria.label "Menu principal"
                        ]
                        [ ul
                            [ class "fr-nav__list flex-auto relative"
                            ]
                          <|
                            (List.map viewMenu menuItems
                                ++ (if shared.identity == Nothing then
                                        []

                                    else
                                        [ li
                                            [ class "fr-nav__item !absolute !-right-[16px]"
                                            ]
                                            [ button
                                                [ class "fr-nav__link"
                                                , Events.onClick <|
                                                    Spa.mapSharedMsg <|
                                                        Shared.Tutoriel <|
                                                            UI.Tutoriel.open
                                                ]
                                                [ text "Prise en main express" ]
                                            ]
                                        ]
                                   )
                            )
                        ]
                    ]
                ]
            ]
        ]


modalMenuId : String
modalMenuId =
    "modal-menu"


modalMenuMobileTitle : String
modalMenuMobileTitle =
    "fr-btn-menu-mobile"


mobileMenu : Shared -> List (MenuItem (Spa.Msg Shared.Msg msg)) -> Html (Spa.Msg Shared.Msg msg)
mobileMenu shared menuItems =
    div
        [ class "fr-header__menu fr-modal"
        , Attr.classList [ ( "fr-modal--opened", shared.headerMenuOpen ) ]
        , Attr.id modalMenuId
        , Attr.attribute "aria-labelledby" modalMenuMobileTitle
        ]
        [ div
            [ Grid.container
            ]
            [ button
                [ class "fr-link--close fr-link"
                , Attr.attribute "aria-controls" modalMenuId
                , Events.onClick <| Spa.mapSharedMsg <| Shared.ToggleHeaderMenu
                ]
                [ text "Fermer" ]
            , div
                [ class "fr-header__menu-links hidden"
                ]
                []
            , div
                [ class "fr-header__menu-links"
                ]
                [ ul
                    [ class "fr-links-group"
                    ]
                  <|
                    (\list ->
                        case shared.identity of
                            Just _ ->
                                logoutLink
                                    :: profileAccess
                                    :: list

                            Nothing ->
                                loginLink
                                    :: list
                    )
                    <|
                        List.map viewMenu <|
                            menuItems
                ]
            ]
        ]


profileAccess : Html msg
profileAccess =
    li
        []
        [ Typo.linkStandalone
            (Typo.linkLeftIcon DSFR.Icons.System.settings5Line)
            (Route.toUrl <| Route.ParamsProfil)
            []
            [ text "Paramètres" ]
        ]


loginLink : Html (Spa.Msg Shared.Msg msg)
loginLink =
    li
        []
        [ Typo.linkStandalone
            (Typo.linkLeftIcon DSFR.Icons.User.userSettingLine)
            (Route.toUrl <| Route.Connexion Nothing)
            []
            [ text "Me connecter" ]
        ]


logoutLink : Html (Spa.Msg Shared.Msg msg)
logoutLink =
    li
        []
        [ Typo.linkStandalone
            (Typo.linkLeftIcon DSFR.Icons.System.logoutBoxRLine)
            (Route.toUrl <| Route.Deconnexion)
            [ Attr.id "logout" ]
            [ text "Me déconnecter" ]
        ]


type alias MenuItem msg =
    { label : Html msg, current : Bool, target : Maybe (), href : Maybe String }


viewMenu : MenuItem msg -> Html msg
viewMenu { label, current, href, target } =
    li
        [ class "fr-nav__item"
        ]
        [ a
            [ class "fr-nav__link"
            , Html.Attributes.Extra.attributeMaybe Attr.href href
            , Attr.attribute "role" "tab"
            , Role.tab
            , Html.Attributes.Extra.attributeIf current <| class "blue-text"
            , Attr.rel ""
            , Html.Attributes.Extra.attributeIf current Aria.currentPage
            , target |> Maybe.map (\_ -> Attr.target "_self") |> Maybe.withDefault Html.Attributes.Extra.empty
            ]
            [ label ]
        ]


viewSideMenu : MenuItem msg -> Html msg
viewSideMenu { label, current, href } =
    li
        [ class "fr-sidemenu__item"
        ]
        [ a
            [ class "fr-sidemenu__link"
            , Typo.textSm
            , href
                |> Maybe.map Attr.href
                |> Maybe.withDefault (Attr.target "_self")
            , Attr.attribute "role" "tab"
            , Role.tab
            , Html.Attributes.Extra.attributeIf current <| class "blue-text"
            , Attr.rel ""
            , Html.Attributes.Extra.attributeIf current Aria.currentPage
            ]
            [ label ]
        ]


parametresSideMenu : Route -> Html msg
parametresSideMenu current =
    nav
        [ class "fr-sidemenu"
        , Attr.id "sidemenu-navigation"
        , navigation
        , Aria.label "Menu principal"
        , class "fr-card--white p-4"
        ]
        [ ul [ class "fr-sidemenu__list" ] <|
            List.map viewSideMenu <|
                [ MenuItem (text "Informations personnelles") (isCurrentRoute current Route.ParamsProfil) Nothing <| Just <| Route.toUrl <| Route.ParamsProfil
                , MenuItem (text "Gestion des collaborateurs") (isCurrentRoute current Route.ParamsCollaborateurs) Nothing <| Just <| Route.toUrl <| Route.ParamsCollaborateurs
                , MenuItem (text "Importer mes données") (isCurrentRoute current Route.ParamsImport) Nothing <| Just <| Route.toUrl <| Route.ParamsImport
                , MenuItem (text "Gestion des étiquettes de qualification") (isCurrentRoute current Route.ParamsTags) Nothing <| Just <| Route.toUrl <| Route.ParamsTags
                , MenuItem (text "Accès SIG") (isCurrentRoute current Route.ParamsSIG) Nothing <| Just <| Route.toUrl <| Route.ParamsSIG
                , MenuItem (text "Zonages personnalisés") (isCurrentRoute current Route.ParamsZonages) Nothing <| Just <| Route.toUrl <| Route.ParamsZonages
                ]
        ]
