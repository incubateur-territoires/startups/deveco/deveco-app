port module UI.Avertissement exposing (Model, Msg, init, update, viewModal)

import Accessibility exposing (Html, div, p, text)
import DSFR.Modal
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)


type Model
    = Model (Maybe (Html Msg))


init : Bool -> Model
init averti =
    Model <|
        if averti then
            Nothing

        else
            Just avertissement


type Msg
    = NoOp
    | Close


update : Msg -> Model -> ( Model, Cmd Msg )
update msg (Model model) =
    Tuple.mapFirst Model <|
        case msg of
            NoOp ->
                ( model, Cmd.none )

            Close ->
                ( Nothing, setAverti () )


avertissement : Html msg
avertissement =
    div [ class "flex flex-col" ]
        [ p []
            [ text "Vous rencontrez des ralentissements lors de l'utilisation de Deveco ? Avec un nombre d'utilisateurs multiplié par quatre en moins de trois mois, notre serveur est effectivement mis à rude épreuve et toute l'équipe est sur le pont pour résoudre ça le plus rapidement possible. Continuez à nous faire part de vos retours."
            ]
        , p [] [ text "Merci de votre compréhension." ]
        ]


viewModal : Model -> Html Msg
viewModal (Model maybeAvertissementModal) =
    case maybeAvertissementModal of
        Nothing ->
            nothing

        Just cur ->
            DSFR.Modal.view
                { id = modalId
                , label = modalId
                , openMsg = NoOp
                , closeMsg = Just Close
                , title = nothing
                , opened = True
                }
                (div [ class "flex flex-col gap-4" ] [ cur ])
                Nothing
                |> Tuple.first


modalId : String
modalId =
    "avertissement-modal"


port setAverti : () -> Cmd msg
