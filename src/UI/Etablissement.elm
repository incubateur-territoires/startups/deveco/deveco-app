module UI.Etablissement exposing (EtablissementWithExtraInfo, FicheInfos, Siret, decodeEtablissementWithExtraInfo, decodeEtablissementsWithExtraInfo, etablissementCard)

import Accessibility exposing (a, button, div, span, text)
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.Communication
import DSFR.Icons.System
import DSFR.Typography as Typo
import Data.Entite exposing (Entite)
import Data.Etablissement exposing (Etablissement, EtatAdministratif(..))
import Data.Fiche exposing (Fiche)
import Date exposing (Date)
import Html exposing (Html)
import Html.Attributes as Attr exposing (class)
import Html.Attributes.Extra exposing (empty)
import Html.Events
import Html.Extra exposing (viewIf)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Lib.Date
import Lib.UI exposing (withEmptyAs)
import Route
import UI.Entite


type alias EtablissementSimple =
    ( Etablissement, FicheInfos )


type alias EtablissementHistorique =
    { predecesseurs : List EtablissementSimple
    , successeurs : List EtablissementSimple
    }


type alias EtablissementWithExtraInfo =
    { etablissement : Etablissement
    , entite : Maybe Entite
    , fiche : Maybe Fiche
    , infos : FicheInfos
    , sireneUpdate : Maybe Date
    , freres : List EtablissementSimple
    , historique : EtablissementHistorique
    }


type alias FicheInfos =
    { contacts : Bool
    , echanges : Bool
    , rappels : Bool
    , favori : Bool
    }


type alias Siret =
    String


type alias CardMessages msg =
    { toggleFavorite : Maybe (Siret -> Bool -> msg)
    }


etablissementCard : CardMessages msg -> Etablissement -> FicheInfos -> Bool -> Html msg
etablissementCard { toggleFavorite } etablissement infos full =
    div [ Grid.col12, Html.Attributes.Extra.attributeIf (not full) Grid.colSm4, class "p-2" ]
        [ div [ Typo.textSm, class "!mb-0" ]
            [ div
                [ class "flex flex-col overflow-y-auto fr-card--white p-2 border-2 border-black etablissement-card"
                , Attr.style "background-image" "none"
                ]
                [ div [ class "flex flex-row justify-between relative p-2" ]
                    [ div [ class "flex flex-row items-center gap-2" ]
                        [ UI.Entite.badgeInactif <|
                            Just <|
                                .etatAdministratif <|
                                    etablissement
                        , UI.Entite.badgeExogene <|
                            .exogene <|
                                etablissement
                        , UI.Entite.badgeSiege <|
                            .siege <|
                                etablissement
                        , text "\u{00A0}"
                        ]
                    ]
                , div [ class "flex flex-row justify-between px-2" ]
                    [ div [ class "w-full flex flex-row gap-2" ]
                        [ viewIf infos.contacts <|
                            span [ Attr.title "Contacts qualifiés" ] <|
                                List.singleton <|
                                    DSFR.Icons.iconMD <|
                                        DSFR.Icons.custom <|
                                            "ri-contacts-book-2-line"
                        , viewIf infos.echanges <|
                            span [ Attr.title "Échanges consignés" ] <|
                                List.singleton <|
                                    DSFR.Icons.iconMD DSFR.Icons.Communication.discussLine
                        , viewIf infos.rappels <|
                            span [ Attr.title "Rappels en cours" ] <|
                                List.singleton <|
                                    DSFR.Icons.iconMD DSFR.Icons.System.timerLine
                        , text "\u{00A0}"
                        ]
                    , span
                        [ toggleFavorite
                            |> Maybe.map
                                (\_ ->
                                    Attr.title <|
                                        if infos.favori then
                                            "Retirer des favoris"

                                        else
                                            "Ajouter aux favoris"
                                )
                            |> Maybe.withDefault empty
                        ]
                        [ DSFR.Icons.iconMD <|
                            if infos.favori then
                                DSFR.Icons.System.starFill

                            else
                                DSFR.Icons.System.starLine
                        ]
                        |> (case toggleFavorite of
                                Nothing ->
                                    identity

                                Just msg ->
                                    List.singleton
                                        >> button [ class "!h-0", Html.Events.onClick <| msg etablissement.siret <| not <| infos.favori ]
                           )
                    ]
                , a
                    [ class "fr-card--white flex flex-col p-4 custom-hover overflow-y-auto gap-2"
                    , Attr.style "background-image" "none"
                    , Attr.href <|
                        Route.toUrl <|
                            Route.Etablissement <|
                                etablissement.siret
                    ]
                    [ div []
                        [ div
                            [ Typo.textBold
                            , class "!mb-0"
                            , class "line-clamp-1"
                            ]
                          <|
                            List.singleton <|
                                text <|
                                    Data.Etablissement.displayNomEnseigne <|
                                        etablissement
                        , div
                            [ class "!mb-0"
                            , class "line-clamp-1"
                            , Attr.title <|
                                .adresse <|
                                    etablissement
                            ]
                          <|
                            List.singleton <|
                                text <|
                                    withEmptyAs "-" <|
                                        .adresse <|
                                            etablissement
                        ]
                    , etablissement
                        |> .activitePrincipaleUniteLegale
                        |> Maybe.withDefault "-"
                        |> Lib.UI.infoLine (Just 2) "Activité NAF"
                        |> List.singleton
                        |> div [ class "!mb-0" ]
                    , div [ class "!mb-0" ] <|
                        List.singleton <|
                            if etablissement.etatAdministratif == Inactif then
                                etablissement
                                    |> .dateFermetureEtablissement
                                    |> Maybe.map Lib.Date.formatDateShort
                                    |> Maybe.withDefault "-"
                                    |> Lib.UI.infoLine (Just 1) "Date de fermeture"

                            else
                                etablissement
                                    |> .dateCreationEtablissement
                                    |> Maybe.map Lib.Date.formatDateShort
                                    |> Maybe.withDefault "-"
                                    |> Lib.UI.infoLine (Just 1) "Date de création"
                    ]
                ]
            ]
        ]


decodeEtablissementWithExtraInfo : Decoder EtablissementWithExtraInfo
decodeEtablissementWithExtraInfo =
    Decode.succeed EtablissementWithExtraInfo
        |> andMap (Decode.field "entreprise" Data.Etablissement.decodeEtablissement)
        |> andMap (optionalNullableField "entite" Data.Entite.decodeEntite)
        |> andMap (optionalNullableField "fiche" Data.Fiche.decodeFiche)
        |> andMap (Decode.field "infos" decodeFicheInfos)
        |> andMap (optionalNullableField "sireneUpdate" Lib.Date.decodeCalendarDate)
        |> andMap
            (Decode.map (Maybe.withDefault []) <|
                optionalNullableField "freres" <|
                    Decode.list <|
                        decodeEtablissementSimple
            )
        |> andMap
            (Decode.map (Maybe.withDefault (EtablissementHistorique [] [])) <|
                optionalNullableField "history" <|
                    Decode.map2 EtablissementHistorique
                        (Decode.map (Maybe.withDefault []) <|
                            optionalNullableField "predecesseurs" <|
                                Decode.list <|
                                    decodeEtablissementSimple
                        )
                        (Decode.map (Maybe.withDefault []) <|
                            optionalNullableField "successeurs" <|
                                Decode.list <|
                                    decodeEtablissementSimple
                        )
            )


decodeEtablissementSimple : Decoder ( Etablissement, FicheInfos )
decodeEtablissementSimple =
    Decode.succeed Tuple.pair
        |> andMap (Decode.field "entreprise" Data.Etablissement.decodeEtablissement)
        |> andMap (Decode.field "infos" decodeFicheInfos)


decodeEtablissementsWithExtraInfo : Decoder (List EtablissementWithExtraInfo)
decodeEtablissementsWithExtraInfo =
    decodeEtablissementWithExtraInfo
        |> Decode.list


decodeFicheInfos : Decoder FicheInfos
decodeFicheInfos =
    Decode.succeed FicheInfos
        |> andMap (Decode.field "contacts" Decode.bool)
        |> andMap (Decode.field "echanges" Decode.bool)
        |> andMap (Decode.field "rappels" Decode.bool)
        |> andMap (Decode.field "favori" Decode.bool)
