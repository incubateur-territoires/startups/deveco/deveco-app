module UI.Tutoriel exposing (Model, Msg, init, open, update, viewModal)

import Accessibility exposing (Html, div, hr, p, text)
import DSFR.Button
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Modal
import DSFR.Typography as Typo
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)
import Lib.Variables exposing (contactEmail)


type Model
    = Model (Maybe (Html Msg))


init : Model
init =
    Model Nothing


type Msg
    = NoOp
    | Open
    | Close


update : Msg -> Model -> ( Model, Cmd Msg )
update msg (Model model) =
    Tuple.mapFirst Model <|
        case msg of
            NoOp ->
                ( model, Cmd.none )

            Open ->
                ( Just tutoriel, Cmd.none )

            Close ->
                ( Nothing, Cmd.none )


tutoriel : Html msg
tutoriel =
    div [ class "flex flex-col" ]
        [ header "PRISE EN MAIN EXPRESS"
        , hr [] []
        , p []
            [ text "Un nouveau guide est à votre disposition pour faire vos premiers pas sur Deveco."
            ]
        , p [] [ text "En 20 minutes, retrouvez les principales fonctionnalités de Deveco dans l'onglet \"Tutoriel\"." ]
        , p []
            [ text "Échangez avec notre support grâce à la bulle de chat située en bas à droite de votre écran ou bien écrivez à l'équipe\u{00A0}: "
            , Typo.externalLink
                ("mailto:" ++ contactEmail)
                []
                [ text contactEmail ]
            ]
        ]


header : String -> Html msg
header titleText =
    Accessibility.h1 [ Typo.fr_h4, class "!m-0" ]
        [ DSFR.Icons.iconLG DSFR.Icons.System.arrowRightLine
        , text " "
        , text titleText
        ]


viewModal : Model -> Html Msg
viewModal (Model maybeNewFeaturesModal) =
    case maybeNewFeaturesModal of
        Nothing ->
            nothing

        Just cur ->
            DSFR.Modal.view
                { id = modalId
                , label = modalId
                , openMsg = NoOp
                , closeMsg = Just Close
                , title = nothing
                , opened = True
                }
                (div [ class "flex flex-col gap-4" ] [ cur, footer ])
                Nothing
                |> Tuple.first


footer : Html Msg
footer =
    DSFR.Button.group
        [ DSFR.Button.new
            { onClick = Nothing
            , label = "Ouvrir le tutoriel"
            }
            |> DSFR.Button.linkButtonExternal tutorielLink
        , DSFR.Button.new
            { onClick = Just Close
            , label = "Plus tard"
            }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


modalId : String
modalId =
    "new-features-modal"


tutorielLink : String
tutorielLink =
    "https://deveco.incubateur.anct.gouv.fr/documentation/"


open : Msg
open =
    Open
