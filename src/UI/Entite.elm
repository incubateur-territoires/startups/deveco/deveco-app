module UI.Entite exposing (badgeAncienCreateur, badgeExogene, badgeInactif, badgeSiege, icon, iconeCreateur, iconeEtablissement)

import Accessibility exposing (Html, span, text)
import DSFR.Badge
import DSFR.Color
import DSFR.Icons
import DSFR.Icons.Buildings
import DSFR.Icons.User
import Data.Entite exposing (Entite)
import Data.Etablissement exposing (EtatAdministratif)
import Html.Attributes
import Html.Extra exposing (nothing)


createur : String
createur =
    "Créateur d'entreprise"


etablissement : String
etablissement =
    "Établissement"


toDisplay : Entite -> String
toDisplay entite =
    entite.etablissement
        |> Maybe.map (\_ -> etablissement)
        |> Maybe.withDefault createur


iconeEtablissement : DSFR.Icons.IconName
iconeEtablissement =
    DSFR.Icons.Buildings.hotelLine


iconeCreateur : DSFR.Icons.IconName
iconeCreateur =
    DSFR.Icons.User.userLine


icon : Entite -> Html msg
icon entite =
    entite.etablissement
        |> Maybe.map (\_ -> iconeEtablissement)
        |> Maybe.withDefault iconeCreateur
        |> DSFR.Icons.iconLG
        |> List.singleton
        |> span [ Html.Attributes.title <| toDisplay entite ]


badgeInactif : Maybe EtatAdministratif -> Html msg
badgeInactif maybeEtatAdministratif =
    maybeEtatAdministratif
        |> Maybe.andThen
            (\etatAdministratif ->
                case etatAdministratif of
                    Data.Etablissement.Inactif ->
                        Just "Fermé"

                    _ ->
                        Nothing
            )
        |> Maybe.map (text >> DSFR.Badge.system { context = DSFR.Badge.Error, withIcon = False } >> DSFR.Badge.badgeMD)
        |> Maybe.withDefault nothing


badgeExogene : Bool -> Html msg
badgeExogene exogene =
    if exogene then
        text "Exogène"
            |> DSFR.Badge.system { context = DSFR.Badge.New, withIcon = False }
            |> DSFR.Badge.badgeMD

    else
        nothing


badgeSiege : Bool -> Html msg
badgeSiege siege =
    if siege then
        text "Siège"
            |> DSFR.Badge.system { context = DSFR.Badge.Info, withIcon = False }
            |> DSFR.Badge.badgeMD

    else
        nothing


badgeAncienCreateur : Maybe etablissement -> Html msg
badgeAncienCreateur etablissementCree =
    etablissementCree
        |> Maybe.map
            (\_ ->
                text "Ancien créateur"
                    |> DSFR.Badge.default
                    |> DSFR.Badge.withColor DSFR.Color.brownCaramel
                    |> DSFR.Badge.badgeMD
            )
        |> Maybe.withDefault nothing
