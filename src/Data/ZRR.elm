module Data.ZRR exposing (ZRR, zrrs)


type alias ZRR =
    { id : String
    , label : String
    }


zrrs : List ZRR
zrrs =
    [ { id = "C", label = "Classé" }
    , { id = "P", label = "Partiellement classé" }
    , { id = "N", label = "Non classé" }
    ]
