module Data.Territoire exposing (Territoire, TerritoireId, categorie, categorieToDisplay, decodeTerritoire, geographieLibelle, id, importInProgress, nom, typeToDisplay, type_)

import Api.EntityId exposing (EntityId)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)


type Territoire
    = Territoire TerritoireData


type TerritoireId
    = TerritoireId


type alias TerritoireData =
    { id : EntityId TerritoireId
    , nom : String
    , type_ : String
    , importInProgress : Bool
    , geographieLibelle : String
    , geographieType : String
    , geographieId : String
    }


id : Territoire -> EntityId TerritoireId
id (Territoire data) =
    data.id


nom : Territoire -> String
nom (Territoire data) =
    data.nom


type_ : Territoire -> String
type_ (Territoire data) =
    data.geographieType


categorie : Territoire -> String
categorie (Territoire data) =
    data.type_


importInProgress : Territoire -> Bool
importInProgress (Territoire data) =
    data.importInProgress


geographieLibelle : Territoire -> String
geographieLibelle (Territoire data) =
    data.geographieLibelle


categorieToDisplay : String -> String
categorieToDisplay territoryCategorie =
    case territoryCategorie of
        "petr" ->
            "PETR"

        "metropole" ->
            "Ville à arrondissements"

        "departement" ->
            "Département"

        "region" ->
            "Région"

        "epci" ->
            "EPCI"

        "commune" ->
            "Commune"

        "perimetre" ->
            "Groupement"

        _ ->
            "-"


typeToDisplay : String -> String
typeToDisplay territoryType =
    case territoryType of
        "CA" ->
            "Communauté d'agglomération"

        "EPT" ->
            "Établissement public territorial"

        "ME" ->
            "Métropole"

        "CC" ->
            "Communauté de communes"

        "CU" ->
            "Communauté urbaine"

        "petr" ->
            "PETR"

        "metropole" ->
            "Ville à arrondissements"

        "departement" ->
            "Département"

        "region" ->
            "Région"

        "COM" ->
            "Commune"

        "ARM" ->
            "Arrondissement"

        "groupement de commune" ->
            "Communes"

        "groupement de epci" ->
            "EPCIs"

        "groupement de departement" ->
            "Départements"

        _ ->
            "-"


decodeTerritoire : Decoder Territoire
decodeTerritoire =
    Decode.succeed TerritoireData
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "name" Decode.string)
        |> andMap (Decode.field "territoryType" Decode.string)
        |> andMap (Decode.field "importInProgress" Decode.bool)
        |> andMap (Decode.map (Maybe.withDefault "-") <| optionalNullableField "geographyLibelle" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| optionalNullableField "geographyType" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| optionalNullableField "geographyId" Decode.string)
        |> Decode.map Territoire
