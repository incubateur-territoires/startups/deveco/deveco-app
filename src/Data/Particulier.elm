module Data.Particulier exposing (Particulier, decodeParticulier, displayNomCreateur)

import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap, optionalNullableField)


type alias Particulier =
    { nom : String
    , prenom : String
    , email : String
    , telephone : String
    , adresse : String
    , ville : String
    , codePostal : String
    , geolocation : Maybe String
    , description : String
    , qpv : Maybe String
    }


decodeParticulier : Decode.Decoder Particulier
decodeParticulier =
    Decode.succeed Particulier
        |> andMap (Decode.field "nom" Decode.string)
        |> andMap (Decode.field "prenom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "email" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "telephone" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "adresse" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "ville" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "codePostal" Decode.string)
        |> andMap (Decode.maybe <| Decode.field "geolocation" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "description" Decode.string)
        |> andMap (optionalNullableField "qpv" <| Decode.map (\qpv -> "QPV " ++ qpv) <| Decode.field "nomQp" Decode.string)


displayNomCreateur : Particulier -> String
displayNomCreateur particulier =
    particulier.prenom ++ " " ++ particulier.nom
