module Data.Naf exposing (CodeNaf, decodeCodeNaf)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)


type alias CodeNaf =
    { id : String
    , label : String
    }


decodeCodeNaf : Decoder CodeNaf
decodeCodeNaf =
    Decode.succeed CodeNaf
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "label" Decode.string)
