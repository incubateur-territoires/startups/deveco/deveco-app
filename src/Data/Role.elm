module Data.Role exposing (Role(..), decodeRole, isDeveco, territoire, welcomeEmailSent)

import Data.Territoire exposing (Territoire)
import Json.Decode as Decode exposing (Decoder)


type Role
    = Deveco Territoire Bool
    | Superadmin


decodeRole : Decoder Role
decodeRole =
    Decode.field "accountType" Decode.string
        |> Decode.andThen
            (\s ->
                case s of
                    "deveco" ->
                        Decode.map2 (\t b -> Deveco t b)
                            (Decode.field "territoire" Data.Territoire.decodeTerritoire)
                            (Decode.field "welcomeEmailSent" Decode.bool)

                    "superadmin" ->
                        Decode.succeed Superadmin

                    _ ->
                        Decode.fail <| "role inconnu\u{00A0}: " ++ s
            )


territoire : Role -> Maybe Territoire
territoire role =
    case role of
        Superadmin ->
            Nothing

        Deveco t _ ->
            Just t


welcomeEmailSent : Role -> Maybe Bool
welcomeEmailSent role =
    case role of
        Superadmin ->
            Nothing

        Deveco _ b ->
            Just b


isDeveco : Role -> Bool
isDeveco role =
    case role of
        Superadmin ->
            False

        Deveco _ _ ->
            True
