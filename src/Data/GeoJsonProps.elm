module Data.GeoJsonProps exposing (GeoJsonProp(..), availableGeoJsonProps, geoJsonPropToCode, geoJsonPropToDisplay, geoJsonPropToString)


type GeoJsonProp
    = DateCreation
    | DateFermeture


availableGeoJsonProps : List GeoJsonProp
availableGeoJsonProps =
    [ DateCreation
    , DateFermeture
    ]


geoJsonPropToString : GeoJsonProp -> String
geoJsonPropToString geoJsonProps =
    case geoJsonProps of
        DateCreation ->
            "dateCreation"

        DateFermeture ->
            "dateFermeture"


geoJsonPropToDisplay : GeoJsonProp -> String
geoJsonPropToDisplay geoJsonProps =
    case geoJsonProps of
        DateCreation ->
            "Date de création"

        DateFermeture ->
            "Date de fermeture"


geoJsonPropToCode : GeoJsonProp -> String
geoJsonPropToCode geoJsonProps =
    case geoJsonProps of
        DateCreation ->
            "dc"

        DateFermeture ->
            "df"
