module Data.Fiche exposing (Fiche, FicheId, activitesReelles, decodeFiche, encodeContact, entiteId, futureEnseigne, getParticulier, id, localisations, motsCles, nom)

import Api.EntityId exposing (EntityId)
import Data.Cloture exposing (Cloture)
import Data.Demande exposing (Demande)
import Data.Echange exposing (Echange)
import Data.Entite as Entite exposing (Entite)
import Data.Etablissement
import Data.Particulier exposing (Particulier)
import Data.Rappel exposing (Rappel)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode exposing (Value)
import Lib.Date
import Time exposing (Posix)


type FicheId
    = FicheId


type alias Fiche =
    { id : EntityId FicheId
    , entite : Entite
    , demandes : List Demande
    , echanges : List Echange
    , rappels : List Rappel
    , clotures : List Cloture
    , dateModification : Posix
    , auteurModification : String
    }


decodeFiche : Decoder Fiche
decodeFiche =
    Decode.succeed Fiche
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap Entite.decodeEntite
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "demandes" <| Decode.list Data.Demande.decodeDemande)
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "echanges" <| Decode.list Data.Echange.decodeEchange)
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "rappels" <| Decode.list Data.Rappel.decodeRappel)
        |> andMap (Decode.map (List.filterMap identity) <| Decode.map (Maybe.withDefault []) <| optionalNullableField "demandes" <| Decode.list Data.Cloture.decodeCloture)
        |> andMap (Decode.field "dateModification" Lib.Date.decodeDateWithTimeFromISOString)
        |> andMap (Decode.field "auteurModification" Decode.string)


encodeContact : Entite.Contact -> Value
encodeContact =
    Entite.encodeContact


id : Fiche -> EntityId FicheId
id =
    .id


nom : Fiche -> String
nom { entite } =
    case ( entite.etablissement, entite.createur ) of
        ( Just e, _ ) ->
            Data.Etablissement.displayNomEnseigne e

        ( _, Just p ) ->
            Data.Particulier.displayNomCreateur p

        _ ->
            ""


activitesReelles : Fiche -> List String
activitesReelles =
    .entite >> Entite.activitesReelles


localisations : Fiche -> List String
localisations =
    .entite >> Entite.localisations


motsCles : Fiche -> List String
motsCles =
    .entite >> Entite.motsCles


getParticulier : Fiche -> Maybe Particulier
getParticulier =
    .entite >> Entite.getParticulier


futureEnseigne : Fiche -> Maybe String
futureEnseigne =
    .entite >> Entite.getFutureEnseigne


entiteId : Fiche -> EntityId Entite.EntiteId
entiteId =
    .entite >> Entite.id
