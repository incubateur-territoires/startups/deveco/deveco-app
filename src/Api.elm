module Api exposing (addProprietaire, adminTerritoires, authStatus, createCloture, createContact, createCreateur, createEchange, createEtablissement, createLocal, createRappel, createUtilisateur, deleteProprietaire, editContact, geoTokens, getActiviteStats, getAdminTags, getAnalysisStats, getCollaborateurs, getCommunes, getContact, getEpcisForPetr, getEtablissement, getEtablissements, getExportCreateurs, getExportEtablissements, getFiche, getFicheType, getFiches, getFichesFiltersSource, getLocal, getLocaux, getRappels, getStats, getStatsData, getTerritoryTags, getUtilisateurs, getUtilisateursCsv, getZonage, getZonages, jwt, lierFiche, login, logout, newTerritoireSearch, rechercheActivite, rechercheActiviteParticulier, rechercheAdresse, rechercheCodeNaf, rechercheLocalisation, rechercheMot, refreshStats, searchEtablissements, searchInseeSiret, supprimerLienFiche, toggleActifAdmin, toggleFavoriteEtablissement, updateAccount, updateEchange, updateEtablissement, updateFiche, updateFiches, updateLocal, updatePassword, updatePasswordAdmin, updateQualificationsEtablissements, updateRappel)

import Api.EntityId exposing (EntityId, entityIdToString)
import Data.Echange exposing (EchangeId)
import Data.Entite exposing (ContactId)
import Data.Fiche exposing (FicheId)
import Data.Local exposing (LocalId)
import Data.Rappel exposing (RappelId)
import Data.Territoire exposing (TerritoireId)
import Data.Zonage exposing (ZonageId)
import Url.Builder as Builder exposing (QueryParameter)


apiRoot : String
apiRoot =
    "api"


authRoute : String
authRoute =
    "auth"


etablissementRoute : String
etablissementRoute =
    "insee"


fichesRoute : String
fichesRoute =
    "fiches"


contactsRoute : String
contactsRoute =
    "contacts"


echangesRoute : String
echangesRoute =
    "echanges"


cloturesRoute : String
cloturesRoute =
    "demandes"


rappelsRoute : String
rappelsRoute =
    "rappels"


accountRoute : String
accountRoute =
    "compte"


adminRoute : String
adminRoute =
    "admin"


territoiresRoute : String
territoiresRoute =
    "territoires"


statsRoute : String
statsRoute =
    "stats"


locauxRoute : String
locauxRoute =
    "locaux"


epcisRoute : String
epcisRoute =
    "epcis"


apiUrl : List String -> List QueryParameter -> String
apiUrl segments =
    Builder.absolute (apiRoot :: segments)


login : String
login =
    apiUrl [ authRoute, "login" ] []


logout : String
logout =
    apiUrl [ authRoute, "logout" ] []


authStatus : String
authStatus =
    apiUrl [ authRoute, "status" ] []


jwt : String
jwt =
    apiUrl [ authRoute, "jwt" ] []


searchInseeSiret : String -> String
searchInseeSiret search =
    apiUrl [ etablissementRoute, "siret" ]
        [ Builder.string "search" search
        ]


getEtablissement : String -> String
getEtablissement siret =
    apiUrl [ etablissementRoute, siret ] []


getFiches : String -> String
getFiches query =
    apiUrl [ fichesRoute ] [] ++ "?" ++ query


getFiche : EntityId FicheId -> String
getFiche id =
    apiUrl [ fichesRoute, entityIdToString id ] []


getFicheType : EntityId FicheId -> String
getFicheType id =
    apiUrl [ fichesRoute, "type", entityIdToString id ] []


createCreateur : String
createCreateur =
    apiUrl [ fichesRoute, "creer" ] []


createEtablissement : String
createEtablissement =
    apiUrl [ etablissementRoute, "creer" ] []


updateFiche : EntityId FicheId -> String
updateFiche id =
    apiUrl [ fichesRoute, entityIdToString id, "modifier" ] []


updateFiches : String -> String
updateFiches query =
    apiUrl [ fichesRoute, "batch" ] [] ++ "?" ++ query


updateQualificationsEtablissements : String -> String
updateQualificationsEtablissements query =
    apiUrl [ etablissementRoute, "batch" ] [] ++ "?" ++ query


updateEtablissement : String -> String
updateEtablissement siret =
    apiUrl [ etablissementRoute, siret, "modifier" ] []


rechercheAdresse : Maybe String -> Maybe String -> Maybe String -> String -> String
rechercheAdresse type_ cityCode postCode search =
    let
        sanitizedSearch =
            String.replace " " "+" search

        typeQuery =
            type_
                |> Maybe.map (Builder.string "type" >> List.singleton)
                |> Maybe.withDefault []

        cityQuery =
            cityCode
                |> Maybe.map (Builder.string "citycode" >> List.singleton)
                |> Maybe.withDefault []

        postQuery =
            postCode
                |> Maybe.map (Builder.string "postcode" >> List.singleton)
                |> Maybe.withDefault []
    in
    Builder.crossOrigin
        "https://api-adresse.data.gouv.fr"
        [ "search" ]
    <|
        Builder.string "q" sanitizedSearch
            :: (cityQuery ++ postQuery ++ typeQuery)


rechercheActivite : String -> String
rechercheActivite search =
    apiUrl [ "activites" ]
        [ Builder.string "search" search
        ]


rechercheActiviteParticulier : String -> String
rechercheActiviteParticulier search =
    apiUrl [ "activites" ]
        [ Builder.string "search" search
        ]


rechercheLocalisation : String -> String
rechercheLocalisation search =
    apiUrl [ "localisations" ]
        [ Builder.string "search" search
        ]


rechercheMot : String -> String
rechercheMot search =
    apiUrl [ "mots" ]
        [ Builder.string "search" search
        ]


createContact : Maybe String -> Maybe (EntityId FicheId) -> String
createContact siret ficheId =
    apiUrl [ contactsRoute, "creer" ] <|
        List.filterMap identity <|
            [ Maybe.map (Builder.string "ficheId") <|
                Maybe.map entityIdToString <|
                    ficheId
            , Maybe.map (Builder.string "siret") <|
                siret
            ]


getContact : EntityId ContactId -> String
getContact id =
    apiUrl [ contactsRoute, entityIdToString id ] []


editContact : EntityId ContactId -> String
editContact id =
    apiUrl [ contactsRoute, entityIdToString id, "modifier" ] []


createEchange : Maybe String -> Maybe (EntityId FicheId) -> String
createEchange siret ficheId =
    apiUrl [ echangesRoute, "creer" ] <|
        List.filterMap identity <|
            [ Maybe.map (Builder.string "ficheId") <|
                Maybe.map entityIdToString <|
                    ficheId
            , Maybe.map (Builder.string "siret") <|
                siret
            ]


updateEchange : EntityId EchangeId -> String
updateEchange id =
    apiUrl [ echangesRoute, entityIdToString id, "modifier" ] []


lierFiche : EntityId FicheId -> String -> String
lierFiche id siret =
    apiUrl [ fichesRoute, entityIdToString id, "lierEntreprise", siret ] []


supprimerLienFiche : EntityId FicheId -> String
supprimerLienFiche id =
    apiUrl [ fichesRoute, entityIdToString id, "supprimerLienEntreprise" ] []


createCloture : Maybe String -> Maybe (EntityId FicheId) -> String
createCloture siret ficheId =
    apiUrl [ cloturesRoute, "cloturer" ] <|
        List.filterMap identity <|
            [ Maybe.map (Builder.string "ficheId") <|
                Maybe.map entityIdToString <|
                    ficheId
            , Maybe.map (Builder.string "siret") <|
                siret
            ]


getRappels : String
getRappels =
    apiUrl [ rappelsRoute ] []


createRappel : Maybe String -> Maybe (EntityId FicheId) -> String
createRappel siret ficheId =
    apiUrl [ rappelsRoute, "creer" ] <|
        List.filterMap identity <|
            [ Maybe.map (Builder.string "ficheId") <|
                Maybe.map entityIdToString <|
                    ficheId
            , Maybe.map (Builder.string "siret") <|
                siret
            ]


updateRappel : EntityId RappelId -> String
updateRappel id =
    apiUrl [ rappelsRoute, entityIdToString id, "modifier" ] []


updateAccount : String
updateAccount =
    apiUrl [ accountRoute ] []


updatePassword : String
updatePassword =
    apiUrl [ accountRoute, "password" ] []


getCollaborateurs : String
getCollaborateurs =
    apiUrl [ accountRoute, "collaborateurs" ] []


getStats : String
getStats =
    apiUrl [ accountRoute, "stats" ] []


getActiviteStats : String -> String
getActiviteStats categorie =
    apiUrl [ accountRoute, "stats", "activite", categorie ] []


getZonages : String
getZonages =
    apiUrl [ accountRoute, "zonages" ] []


getZonage : EntityId ZonageId -> String
getZonage id =
    apiUrl [ accountRoute, "zonages", entityIdToString id ] []


getAnalysisStats : String -> String
getAnalysisStats query =
    apiUrl [ "stats", "analyse" ] [] ++ "?" ++ query


getFichesFiltersSource : String -> String
getFichesFiltersSource context =
    apiUrl [ fichesRoute, "filtersSource", context ] []


createUtilisateur : String
createUtilisateur =
    apiUrl [ adminRoute, "utilisateurs" ] []


getUtilisateurs : String
getUtilisateurs =
    apiUrl [ adminRoute, "utilisateurs" ] []


getUtilisateursCsv : String
getUtilisateursCsv =
    apiUrl [ adminRoute, "utilisateurs", "csv" ] []


updatePasswordAdmin : String
updatePasswordAdmin =
    apiUrl [ adminRoute, "password" ] []


toggleActifAdmin : String
toggleActifAdmin =
    apiUrl [ adminRoute, "toggleUser" ] []


refreshStats : String
refreshStats =
    apiUrl [ adminRoute, "refreshTerritoriesStats" ] []


newTerritoireSearch : String -> String -> String
newTerritoireSearch typeTerritoire search =
    apiUrl [ territoiresRoute, "search" ]
        [ Builder.string "search" search
        , Builder.string "type" typeTerritoire
        ]


getCommunes : String
getCommunes =
    apiUrl [ territoiresRoute, "communes" ] []


getExportCreateurs : String -> String
getExportCreateurs query =
    apiUrl [ fichesRoute ] [] ++ "?" ++ query ++ "&format=xlsx"


getExportEtablissements : String -> String
getExportEtablissements query =
    apiUrl [ etablissementRoute ] [] ++ "?" ++ query


getStatsData : String -> String
getStatsData query =
    apiUrl [ statsRoute ] [] ++ "?" ++ query


adminTerritoires : String
adminTerritoires =
    apiUrl [ adminRoute, "territoires" ] []


getLocaux : String -> String
getLocaux query =
    apiUrl [ locauxRoute ] [] ++ "?" ++ query


createLocal : String
createLocal =
    apiUrl [ locauxRoute, "creer" ] []


getLocal : EntityId LocalId -> String
getLocal id =
    apiUrl [ locauxRoute, entityIdToString id ] []


updateLocal : EntityId LocalId -> String
updateLocal id =
    apiUrl [ locauxRoute, entityIdToString id, "modifier" ] []


addProprietaire : EntityId LocalId -> String
addProprietaire id =
    apiUrl [ locauxRoute, entityIdToString id, "proprietaire" ] []


deleteProprietaire : EntityId LocalId -> String
deleteProprietaire id =
    apiUrl [ locauxRoute, entityIdToString id, "proprietaire", "remove" ] []


getEtablissements : String -> String
getEtablissements query =
    apiUrl [ etablissementRoute, "siret" ] [] ++ "?" ++ query


searchEtablissements : String -> String
searchEtablissements query =
    apiUrl [ etablissementRoute ] [] ++ "?" ++ query


rechercheCodeNaf : String -> String
rechercheCodeNaf search =
    apiUrl [ "naf" ]
        [ Builder.string "search" search
        ]


geoTokens : String
geoTokens =
    apiUrl [ accountRoute, "geotokens" ] []


getTerritoryTags : String
getTerritoryTags =
    apiUrl [ territoiresRoute, "tags" ] []


getAdminTags : EntityId TerritoireId -> String
getAdminTags territoryId =
    apiUrl [ adminRoute, "territoires", entityIdToString territoryId, "tags" ] []


toggleFavoriteEtablissement : String
toggleFavoriteEtablissement =
    apiUrl [ etablissementRoute, "favoris" ] []


getEpcisForPetr : String
getEpcisForPetr =
    apiUrl [ epcisRoute, "petr" ] []
