module Lib.Variables exposing (contactEmail, hintActivites, hintDonneesPubliques, hintESS, hintMotsCles, hintSuiviPortefeuille, hintSuiviTous, hintZoneGeographique, hrefNouveautes, hrefTutoriel)

import Url.Builder as Builder


contactEmail : String
contactEmail =
    "contact@deveco.incubateur.anct.gouv.fr"


hrefTutoriel : String
hrefTutoriel =
    Builder.crossOrigin "https://deveco.incubateur.anct.gouv.fr" [ "documentation" ] []


hrefNouveautes : String
hrefNouveautes =
    Builder.crossOrigin "https://deveco.incubateur.anct.gouv.fr" [ "documentation", "nouveau" ] []


hintActivites : String
hintActivites =
    "La qualification de ce champ vous permet de filtrer les établissements et créateurs d'entreprise d'une même activité ou filière dans les onglets Établissements et Créateurs d'entreprise\nVotre collectivité peut créer ses propres étiquettes d'activité\n\nExemples\u{00A0}:\nNumérique, ESS, Commerces de proximité, Automobile, Médical, Service aux particuliers, Association, etc."


hintZoneGeographique : String
hintZoneGeographique =
    "La qualification de ce champ vous permet de filtrer les établissements et créateurs d'entreprise d'une même zone géographique dans les onglets Établissements et Créateurs d'entreprise\nVotre collectivité peut créer ses propres étiquettes de zone géographique\n\nExemples\u{00A0}:\nCentre-ville, ZAC Les Peupliers, Rive droite, QPV, Ancien marché, etc."


hintMotsCles : String
hintMotsCles =
    "La qualification de ce champ vous permet de filtrer les établissements et créateurs d'entreprise ayant le même mot-clé dans les onglets Établissements et Créateurs d'entreprise\nVotre collectivité peut créer ses propres étiquettes de mots-clés en fonction de ses spécificités\n\nExemples\u{00A0}:\nFête du pain, Jury de l'industrie, Redevable taxe Y, Charte signée, etc."


hintSuiviTous : String
hintSuiviTous =
    "Cherche parmi tous les établissements enregistrés dans Deveco\u{00A0}:\n- établissements de la base SIRENE du territoire de référence\n- établissements exogènes rajoutés manuellement"


hintSuiviPortefeuille : String
hintSuiviPortefeuille =
    "Cherche parmi les établissements dont la fiche a été enrichie par\u{00A0}:\n- une qualification (étiquette Activité réelle, Zone géographique, Mots-clés) et/ou\n- un contact et/ou\n- un échange (lié ou non à une demande)"


hintDonneesPubliques : String
hintDonneesPubliques =
    "Les données publiques disponibles sont celles de la base SIRENE et d'API Entreprises mises à jour en continu"


hintESS : String
hintESS =
    "L'Économie Sociale et Solidaire regroupe les associations, les fondations, les coopératives et les mutuelles, ainsi que les établissements qui déclarent adhérer aux principes de l'ESS (loi n°2014-856 du 31 juillet 2014)"
