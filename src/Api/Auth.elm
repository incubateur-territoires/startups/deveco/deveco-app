module Api.Auth exposing
    ( checkAuth
    , delete
    , get
    , post
    , request
    )

import Api
import Http
import Json.Decode as Decode exposing (Decoder)
import RemoteData as RD exposing (WebData)
import Shared exposing (Msg, Redirect)
import User


checkAuth : Bool -> Redirect -> Cmd Msg
checkAuth stayOnPage redir =
    let
        resultToMsg res =
            case res of
                Ok id ->
                    Shared.LoggedIn id redir

                Err _ ->
                    if stayOnPage then
                        Shared.NoOp

                    else
                        Shared.LoggedOut redir
    in
    Http.get
        { url = Api.authStatus
        , expect =
            Http.expectJson resultToMsg <|
                Decode.field "user" User.decodeUser
        }


logoutOnUnauthorized : (Msg -> msg) -> (WebData a -> msg) -> WebData a -> msg
logoutOnUnauthorized toShared handler response =
    case response of
        RD.Failure (Http.BadStatus 401) ->
            toShared <| Shared.LoggedOut Nothing

        RD.Failure (Http.BadStatus 403) ->
            toShared <| Shared.LoggedOut Nothing

        _ ->
            handler response


request :
    { method : String
    , headers : List Http.Header
    , url : String
    , body : Http.Body
    , timeout : Maybe Float
    , tracker : Maybe String
    }
    -> (Msg -> msg)
    -> (WebData data -> msg)
    -> Decoder data
    -> Cmd msg
request { method, headers, url, body, timeout, tracker } toShared toMsg decodePayload =
    Http.request
        { method = method
        , headers = headers
        , url = url
        , body = body

        -- TODO expand on this to allow error reporting on request failure
        , expect = Http.expectJson (RD.fromResult >> logoutOnUnauthorized toShared toMsg) decodePayload
        , timeout = timeout
        , tracker = tracker
        }


get :
    { url : String }
    -> (Msg -> msg)
    -> (WebData data -> msg)
    -> Decoder data
    -> Cmd msg
get { url } =
    request
        { method = "GET"
        , headers = []
        , url = url
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Nothing
        }


post :
    { url : String, body : Http.Body }
    -> (Msg -> msg)
    -> (WebData data -> msg)
    -> Decoder data
    -> Cmd msg
post { url, body } =
    request
        { method = "POST"
        , headers = []
        , url = url
        , body = body
        , timeout = Nothing
        , tracker = Nothing
        }


delete :
    { url : String, body : Http.Body }
    -> (Msg -> msg)
    -> (WebData data -> msg)
    -> Decoder data
    -> Cmd msg
delete { url, body } =
    request
        { method = "DELETE"
        , headers = []
        , url = url
        , body = body
        , timeout = Nothing
        , tracker = Nothing
        }
