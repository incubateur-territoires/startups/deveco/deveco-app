exports.config = {
	bootstrap: null,
	gherkin: {
		features: "./features/**/*.feature",
		steps: ["./step_definitions/steps.js"],
	},
	helpers: {
		FileSystem: {},
		Playwright: {
			browser: "chromium",
			show: process.env.CODECEPT_UI ? true : false,
			url: process.env.CODECEPT_BASEURL || "http://127.0.0.1:3000",
		},
		REST: {
			endpoint:
				(process.env.CODECEPT_BASEURL || "http://127.0.0.1:3000") +
				"/api/test/",
			defaultHeaders: {
				"Content-Type": "application/json",
				Accept: "application/json",
			},
		},
	},
	hooks: [],
	mocha: {},
	name: "Deveco",
	output: "./output",
	plugins: {
		pauseOnFail: {},
		retryFailedStep: {
			enabled: true,
		},
		screenshotOnFail: {
			enabled: true,
		},
		tryTo: {
			enabled: true,
		},
	},
	stepTimeout: 0,
	stepTimeoutOverride: [
		{
			pattern: "wait.*",
			timeout: 0,
		},
		{
			pattern: "amOnPage",
			timeout: 0,
		},
	],
	teardown: null,
	tests: "./__tests__/*.ts",
	timeout: null,
	translation: "fr-FR",
};
