# Tests end-to-end

Les tests e2e sont écrits en [gherkin](https://cucumber.io/docs/gherkin/reference/) en [francais](https://cucumber.io/docs/gherkin/reference/#spoken-languages) via la directive `#language: fr` présente dans les tests. Afin que codecept supporte le francais, nous utilisons le fichier `step_definitions/fr.js` pour traduire les instructions gherkin en francais.

```gherkin
#language: fr

@home
Fonctionnalité: Page d'accueil
  Pour pouvoir me renseigner sur deveco
  En tant que visiteur
  Je veux pouvoir consulter la page d'accueil

Scénario: Home deveco
  Soit un utilisateur sur la page d'accueil
  Alors je vois "Deveco"
  Alors je vois "Faciliter l'accès et la gestion"
```

On utilise [codeceptjs](https://codecept.io/) et [playwright](https://codecept.io/playwright/)

## Vocabulaire

La situation initiale commence `Soit`
les actions commencent par `Quand`
les assertions commencent par `Alors`

## Utilisation

Pour lancer les tests:

```sh
cd e2e
yarn && yarn test
```

Par défaut, les tests se lancent en mode headless mais on peut aussi les lancer avec l'interface de codecept en utilisant

```sh
yarn test:ui
```

## Écrire un test

Pour l'exemple, nous allons écrire le test pour un konami code

1. Créer le fichier konami-code.feature
1. Ecrire le test
1. lancer `yarn codeceptjs gherkin:snippets`
1. Implementer les snippets manquants
1. lancer les test.

Les snippets se trouve dans le fichier `steps.js`

## Howto

Lancer un test en mode debug

```sh
yarn test --steps --verbose --grep "@home" -p pauseOnFail
```

on peut aussi mettre une pause dans le test

```gherkin
@mon_test
Fonctionnalité: Mon test

  Scénario:
    Soit un navigateur web sur le site
    Quand je pause le test
    Alors je vois "foo"
```

## Liens utiles

- Aide XPath, qui sert à cibler un élément dans un document XML (comme une page HTML) : [xpath cheatsheet](https://devhints.io/xpath)
