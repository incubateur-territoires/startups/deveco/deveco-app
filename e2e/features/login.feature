#language: fr

@login
Fonctionnalité: Page de connexion
	Pour pouvoir utiliser deveco
	En tant que visiteur
	Je veux pouvoir me connecter

Scénario: Obtention d'un lien magique
	Quand je vais sur la page d'accueil
	Alors je vois "Deveco"
	Alors je vois "Me connecter"
	Quand je clique sur "Me connecter"
	Alors je vois "Se connecter à Dévéco"
	Alors je vois "Courriel *"
	Quand je renseigne "deveco@ccd.fr" dans le champ "Courriel *"
	Quand je clique sur "Recevoir un lien de connexion"
	Alors je vois "Un courriel vous a été envoyé !"
	Alors je ne vois pas "Adresse de courriel"

Scénario: Utilisation du lien magique
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Alors je vois "territoire de référence"
	Alors je vois "Gestion des collaborateurs"
	Quand je clique sur le bouton de déconnexion
	Alors je suis sur la page d'accueil
	Alors je vois "Gagnez en efficacité pour mieux accompagner le développement économique de votre territoire."

Scénario: Connexion par mot de passe
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et le mot de passe "blah"
	Quand je vais sur la page d'accueil
	Alors je vois "Deveco"
	Alors je vois "Me connecter"
	Quand je clique sur "Me connecter"
	Alors je vois "Se connecter à Dévéco"
	Alors je vois "Courriel *"
	Quand je renseigne "machin@test.fr" dans le champ "Courriel *"
	Quand je clique sur "mot de passe"
	Alors je vois "Mot de passe *"
	Quand je clique sur "mot de passe"
	Alors je ne vois pas "Mot de passe *"
	Quand je clique sur "mot de passe"
	Alors l'élément nommé "submit-connexion" est "désactivé"
	Quand je renseigne "faux" dans le champ "Mot de passe *"
	Alors l'élément nommé "submit-connexion" est "activé"
	Quand je clique sur "Se connecter"
	Alors je vois "Connexion échouée"
	Alors je suis sur la page "/connexion"
	Quand je renseigne "blah" dans le champ "Mot de passe *"
	Alors l'élément nommé "submit-connexion" est "activé"
	Quand je clique sur "Se connecter"
	Alors je vois "territoire de référence"
	Alors je suis sur la page d'accueil
