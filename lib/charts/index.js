// Highcharts integration code written by https://github.com/n1k0 and taken from https://github.com/MTES-MCT/ecobalyse/
import Highcharts from "highcharts";
import enableA11y from "highcharts/modules/accessibility";
import BarHorizontal from "./bar-horizontal";
import Pie from "./pie";

// Enable a11y https://www.highcharts.com/docs/accessibility/accessibility-module
enableA11y(Highcharts);

Highcharts.setOptions({
	lang: {
		loading: "Chargement…",
		months: [
			"janvier",
			"février",
			"mars",
			"avril",
			"mai",
			"juin",
			"juillet",
			"août",
			"septembre",
			"octobre",
			"novembre",
			"décembre",
		],
		weekdays: [
			"dimanche",
			"lundi",
			"mardi",
			"mercredi",
			"jeudi",
			"vendredi",
			"samedi",
		],
		shortMonths: [
			"jan",
			"fév",
			"mar",
			"avr",
			"mai",
			"juin",
			"juil",
			"aoû",
			"sep",
			"oct",
			"nov",
			"déc",
		],
		exportButtonTitle: "Exporter",
		printButtonTitle: "Imprimer",
		rangeSelectorFrom: "Du",
		rangeSelectorTo: "au",
		rangeSelectorZoom: "Période",
		downloadPNG: "Télécharger en PNG",
		downloadJPEG: "Télécharger en JPEG",
		downloadPDF: "Télécharger en PDF",
		downloadSVG: "Télécharger en SVG",
		resetZoom: "Réinitialiser le zoom",
		resetZoomTitle: "Réinitialiser le zoom",
		thousandsSep: " ",
		decimalPoint: ",",
	},
});

export default {
	registerElements: function () {
		customElements.define("chart-bar-horizontal", BarHorizontal);
		customElements.define("chart-pie", Pie);
	},
};
