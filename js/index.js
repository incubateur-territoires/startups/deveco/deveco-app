import Charts from "../lib/charts";
import { toggleCrisp } from "./crisp";

if (
	typeof Elm !== "undefined" &&
	Object.prototype.hasOwnProperty.call(Elm, "Main") &&
	Elm.Main &&
	Object.prototype.hasOwnProperty.call(Elm.Main, "init") &&
	Elm.Main.init
) {
	function switchTheme(theme) {
		const options = ["system", "light", "dark"];
		const [html] = document.getElementsByTagName("html");
		if (options.includes(theme)) {
			html.dataset.frScheme = theme;
		}
	}

	function setAverti() {
		localStorage.setItem("averti", "true");
	}

	const currentTheme = localStorage.getItem("scheme") || "system";
	const averti = Boolean(localStorage.getItem("averti"));

	const appUrl = window.location.origin;

	const flags = {
		now: new Date().getTime(),
		timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
		currentTheme,
		appUrl,
		averti,
	};
	const elm = Elm.Main.init({ flags });
	elm.ports.switchTheme.subscribe(switchTheme);
	elm.ports.setAverti.subscribe(setAverti);

	elm.ports.toggleCrisp.subscribe(toggleCrisp);

	elm.ports.scrollIntoView.subscribe((id) => {
		const element = document.getElementById(id);

		if (element) {
			element.scrollIntoView({
				block: "start",
				inline: "nearest",
				behavior: "smooth",
			});
		}
	});

	// Register custom chart elements
	Charts.registerElements();
}
