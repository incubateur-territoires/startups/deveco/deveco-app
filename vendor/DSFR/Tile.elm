module DSFR.Tile exposing (horizontal, isExternal, vertical, view)

import Accessibility exposing (Html, div, h4, p, text)
import DSFR.Typography
import Html exposing (map)
import Html.Attributes
import Html.Extra exposing (viewMaybe)


type TileConfig
    = TileConfig MandatoryTileConfig OptionalTileConfig


type alias MandatoryTileConfig =
    { title : Html Never
    , link : String
    , description : Maybe String
    }


type alias OptionalTileConfig =
    { orientation : Orientation
    , external : Bool
    }


type Orientation
    = Vertical
    | Horizontal


vertical : { title : Html Never, link : String, description : Maybe String } -> TileConfig
vertical mandatory =
    TileConfig mandatory { orientation = Vertical, external = False }


horizontal : { title : Html Never, link : String, description : Maybe String } -> TileConfig
horizontal mandatory =
    TileConfig mandatory { orientation = Horizontal, external = False }


isExternal : TileConfig -> TileConfig
isExternal (TileConfig mandatory optional) =
    TileConfig mandatory { optional | external = True }


view : TileConfig -> Html msg
view (TileConfig { title, description, link } { orientation, external }) =
    map never <|
        div
            [ Html.Attributes.classList
                [ ( "fr-tile fr-enlarge-link", True )
                , ( "fr-tile--horizontal", orientation == Horizontal )
                ]
            ]
            [ div
                [ Html.Attributes.class "fr-tile__body"
                ]
                [ h4
                    [ Html.Attributes.class "fr-tile__title"
                    ]
                    [ (if external then
                        DSFR.Typography.externalLink

                       else
                        DSFR.Typography.link
                      )
                        link
                        [ Html.Attributes.class "fr-tile__link"
                        ]
                        [ title ]
                    ]
                , viewMaybe
                    (\desc ->
                        p
                            [ Html.Attributes.class "fr-tile__desc"
                            ]
                            [ text desc ]
                    )
                    description
                ]
            ]
