module DSFR.Quote exposing (quote)

import Accessibility exposing (Html, text)


quote : String -> Html msg
quote =
    text
