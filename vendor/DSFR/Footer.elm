module DSFR.Footer exposing (..)

import Accessibility exposing (Html, a, br, button, div, footer, h1, h4, img, li, p, span, text, ul)
import DSFR.Radio
import DSFR.Typography exposing (externalLink, link)
import Html exposing (node)
import Html.Attributes as Attr exposing (class)
import Html.Extra exposing (nothing)
import Lib.Variables exposing (contactEmail)
import Route
import Shared exposing (Shared)
import UI.Theme


simple : Shared -> Html Shared.Msg
simple shared =
    footer
        [ class "fr-footer fr-card--white"
        , Attr.attribute "role" "contentinfo"
        , Attr.id "footer"
        ]
        [ node "dialog"
            [ Attr.id "fr-theme-modal"
            , class "fr-modal"
            , Attr.attribute "role" "dialog"
            , Attr.attribute "aria-labelledby" "fr-theme-modal-title"
            ]
            [ div
                [ class "fr-container fr-container--fluid fr-container-md"
                ]
                [ div
                    [ class "fr-grid-row fr-grid-row--center"
                    ]
                    [ div
                        [ class "fr-col-12 fr-col-md-6 fr-col-lg-4"
                        ]
                        [ div
                            [ class "fr-modal__body"
                            ]
                            [ div
                                [ class "fr-modal__header"
                                ]
                                [ button
                                    [ class "fr-link--close fr-link"
                                    , Attr.attribute "aria-controls" "fr-theme-modal"
                                    ]
                                    [ text "Fermer" ]
                                ]
                            , div
                                [ class "fr-modal__content"
                                ]
                                [ h1
                                    [ Attr.id "fr-theme-modal-title"
                                    , class "fr-modal__title"
                                    ]
                                    [ text "Paramètres d’affichage" ]
                                , div
                                    [ Attr.id "fr-display"
                                    , class "fr-form-group fr-display"
                                    ]
                                    [ div
                                        [ class "fr-form-group"
                                        ]
                                        [ DSFR.Radio.group
                                            { id = "radio-theme"
                                            , options = [ UI.Theme.Light, UI.Theme.Dark, UI.Theme.System ]
                                            , current = Just <| shared.theme
                                            , toLabel =
                                                \theme ->
                                                    text <|
                                                        case theme of
                                                            UI.Theme.Light ->
                                                                "Thème clair"

                                                            UI.Theme.Dark ->
                                                                "Thème sombre"

                                                            UI.Theme.System ->
                                                                "Système"
                                            , toId = UI.Theme.themeToString >> (++) "option-"
                                            , legend = text "Choisissez un thème pour personnaliser l’apparence du site."
                                            , msg = Shared.SetTheme
                                            }
                                            |> DSFR.Radio.withHint
                                                (Just <|
                                                    \theme ->
                                                        case theme of
                                                            UI.Theme.Light ->
                                                                nothing

                                                            UI.Theme.Dark ->
                                                                nothing

                                                            UI.Theme.System ->
                                                                text "Utilise les paramètres système."
                                                )
                                            |> DSFR.Radio.stacked
                                            |> DSFR.Radio.viewRich
                                                (\theme ->
                                                    case theme of
                                                        UI.Theme.Light ->
                                                            ( "/assets/light.svg", Just ( 80, 80 ) )

                                                        UI.Theme.Dark ->
                                                            ( "/assets/dark.svg", Just ( 80, 80 ) )

                                                        UI.Theme.System ->
                                                            ( "/assets/system.svg", Just ( 80, 80 ) )
                                                )
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        , div
            [ class "fr-container"
            ]
            [ div
                [ class "fr-footer__body"
                ]
                [ div
                    [ class "fr-footer__brand fr-enlarge-link"
                    ]
                    [ a
                        [ Attr.href "/accueil"
                        , Attr.title "Retour à l’accueil"
                        ]
                        [ p
                            [ class "fr-logo"
                            , Attr.title "république française"
                            ]
                            [ text "Agence"
                            , br []
                            , text "Nationale"
                            , br []
                            , text "de la Cohésion"
                            , br []
                            , text "des Territoires"
                            ]
                        ]
                    ]
                , div
                    [ class "fr-footer__content"
                    ]
                    [ p
                        [ class "fr-footer__content-desc"
                        ]
                        []
                    , ul
                        [ class "fr-footer__content-list"
                        ]
                        [ li
                            [ class "fr-footer__content-item"
                            ]
                            [ a
                                [ class "fr-footer__content-link"
                                , Attr.href "https://legifrance.gouv.fr"
                                ]
                                [ text "legifrance.gouv.fr" ]
                            ]
                        , li
                            [ class "fr-footer__content-item"
                            ]
                            [ a
                                [ class "fr-footer__content-link"
                                , Attr.href "https://gouvernement.fr"
                                ]
                                [ text "gouvernement.fr" ]
                            ]
                        , li
                            [ class "fr-footer__content-item"
                            ]
                            [ a
                                [ class "fr-footer__content-link"
                                , Attr.href "https://service-public.fr"
                                ]
                                [ text "service-public.fr" ]
                            ]
                        , li
                            [ class "fr-footer__content-item"
                            ]
                            [ a
                                [ class "fr-footer__content-link"
                                , Attr.href "https://data.gouv.fr"
                                ]
                                [ text "data.gouv.fr" ]
                            ]
                        ]
                    ]
                ]
            , div
                [ Attr.class "fr-footer__partners"
                ]
                [ h4
                    [ Attr.class "fr-footer__partners-title"
                    ]
                    [ text "Nos partenaires" ]
                , div
                    [ Attr.class "fr-footer__partners-logos"
                    ]
                    [ div
                        [ Attr.class "fr-footer__partners-main"
                        ]
                        []
                    , div
                        [ Attr.class "fr-footer__partners-sub"
                        ]
                        [ ul []
                            [ li []
                                [ a
                                    [ Attr.class "fr-footer__partners-link"
                                    , Attr.href "https://next-generation-eu.europa.eu/index_fr"
                                    ]
                                    [ img "NextGenerationEU"
                                        [ Attr.class "fr-footer__logo"
                                        , Attr.style "height" "5.625rem"
                                        , Attr.src "/assets/next-generation-EU.png"
                                        ]
                                    ]
                                ]
                            , li []
                                [ a
                                    [ Attr.class "fr-footer__partners-link"
                                    , Attr.href "https://www.economie.gouv.fr/plan-de-relance"
                                    ]
                                    [ img "France Relance"
                                        [ Attr.class "fr-footer__logo"
                                        , Attr.style "height" "5.625rem"
                                        , Attr.src "/assets/france-relance.png"
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            , div
                [ class "fr-footer__bottom"
                ]
                [ span
                    [ DSFR.Typography.textXs, class "w-full text-right p-4" ]
                    [ text "Contactez-nous pour toute aide ou suggestion d'amélioration\u{00A0}: "
                    , externalLink
                        ("mailto:" ++ contactEmail)
                        [ class "fr-footer__bottom-link" ]
                        [ text contactEmail ]
                    ]
                , ul
                    [ class "fr-footer__bottom-list"
                    ]
                    [ li
                        [ class "fr-footer__bottom-item"
                        ]
                        [ button
                            [ class "fr-footer__bottom-link fr-fi-theme-fill fr-link--icon-left"
                            , Attr.attribute "aria-controls" "fr-theme-modal"
                            , Attr.attribute "data-fr-opened" "false"
                            ]
                            [ text "Paramètres d'affichage" ]
                        ]
                    , li
                        [ class "fr-footer__bottom-item"
                        ]
                        [ span
                            [ class "fr-footer__bottom-link"
                            ]
                            [ text "Accessibilité: non conforme" ]
                        ]
                    , li
                        [ class "fr-footer__bottom-item"
                        ]
                        [ link
                            (Route.toUrl <| Route.MentionsLegales)
                            [ class "fr-footer__bottom-link" ]
                            [ text "Mentions légales" ]
                        ]
                    , li
                        [ class "fr-footer__bottom-item"
                        ]
                        [ link
                            (Route.toUrl <| Route.PolitiqueConfidentialite)
                            [ class "fr-footer__bottom-link" ]
                            [ text "Politique de confidentialité" ]
                        ]
                    , li
                        [ class "fr-footer__bottom-item"
                        ]
                        [ link
                            (Route.toUrl <| Route.CGU)
                            [ class "fr-footer__bottom-link" ]
                            [ text "Conditions générales d'utilisation" ]
                        ]
                    , li
                        [ class "fr-footer__bottom-item"
                        ]
                        [ lienTutoriel True ]
                    , li
                        [ class "fr-footer__bottom-item"
                        ]
                        [ link
                            (Route.toUrl <| Route.Stats Nothing)
                            [ class "fr-footer__bottom-link" ]
                            [ text "Statistiques" ]
                        ]
                    ]

                -- [ li
                --     [ class "fr-footer__bottom-item"
                --     ]
                --     [ a
                --         [ class "fr-footer__bottom-link"
                --         , Attr.href "#"
                --         ]
                --         [ text "Plan du site" ]
                --     ]
                -- , li
                --     [ class "fr-footer__bottom-item"
                --     ]
                --     [ a
                --         [ class "fr-footer__bottom-link"
                --         , Attr.href "#"
                --         ]
                --         [ text "Accessibilité: non conforme" ]
                --     ]
                -- , li
                --     [ class "fr-footer__bottom-item"
                --     ]
                --     [ a
                --         [ class "fr-footer__bottom-link"
                --         , Attr.href "#"
                --         ]
                --         [ text "Mentions légales" ]
                --     ]
                -- , li
                --     [ class "fr-footer__bottom-item"
                --     ]
                --     [ a
                --         [ class "fr-footer__bottom-link"
                --         , Attr.href "#"
                --         ]
                --         [ text "Données personnelles" ]
                --     ]
                -- , li
                --     [ class "fr-footer__bottom-item"
                --     ]
                --     [ a
                --         [ class "fr-footer__bottom-link"
                --         , Attr.href "#"
                --         ]
                --         [ text "Gestion des cookies" ]
                --     ]
                -- ]
                , div
                    [ class "fr-footer__bottom-copy"
                    ]
                    [ p []
                        [ text "Sauf mention contraire, tous les contenus de ce site sont sous "
                        , a
                            [ Attr.href "https://github.com/etalab/licence-ouverte/blob/master/LO.md"
                            , Attr.target "_blank"
                            ]
                            [ text "licence etalab-2.0" ]
                        ]
                    ]
                ]
            ]
        ]


lienTutoriel : Bool -> Html msg
lienTutoriel bottom =
    externalLink
        Lib.Variables.hrefTutoriel
        [ class <|
            if bottom then
                "fr-footer__bottom-link"

            else
                "fr--link"
        ]
        [ text "Tutoriel" ]
