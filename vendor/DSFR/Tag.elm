module DSFR.Tag exposing (TagConfig, clickable, deletable, medium, oneMedium, oneSmall, selectable, small, unclickable, withAttrs, withIcon, groupWrapper)

import Accessibility exposing (Attribute, Html, a, button, li, p, text, ul)
import Accessibility.Aria exposing (pressed)
import DSFR.Icons exposing (IconName)
import Html
import Html.Attributes exposing (class, href, type_)
import Html.Attributes.Extra exposing (empty)
import Html.Events exposing (onClick)


type TagConfig msg data
    = TagConfig
        ( MandatoryConfig data
        , Tag msg data
        , OptionalConfig
        )


type alias MandatoryConfig data =
    { data : data
    , toString : data -> String
    }


type Tag msg data
    = Unclickable
    | Clickable Href
    | Selectable Checked (data -> msg)
    | Deletable (data -> msg)


type Href
    = Href String


type Checked
    = Checked Bool


type alias OptionalConfig =
    { icon : Icon
    , extraAttrs : List (Attribute Never)
    }


defaultOptions : OptionalConfig
defaultOptions =
    { icon = NoIcon
    , extraAttrs = []
    }


type Size
    = SM
    | MD


type Icon
    = NoIcon
    | LeftIcon IconName
    | RightIcon IconName


unclickable : { data : data, toString : data -> String } -> TagConfig msg data
unclickable config =
    TagConfig ( config, Unclickable, defaultOptions )


clickable : String -> { data : data, toString : data -> String } -> TagConfig msg data
clickable link config =
    TagConfig ( config, Clickable (Href link), defaultOptions )


selectable : (data -> msg) -> Bool -> { data : data, toString : data -> String } -> TagConfig msg data
selectable selectMsg checked label =
    TagConfig ( label, Selectable (Checked checked) selectMsg, defaultOptions )


deletable : (data -> msg) -> { data : data, toString : data -> String } -> TagConfig msg data
deletable deleteMsg label =
    TagConfig ( label, Deletable deleteMsg, defaultOptions )


oneSmall : TagConfig msg data -> Html msg
oneSmall =
    view SM


oneMedium : TagConfig msg data -> Html msg
oneMedium =
    view MD


view : Size -> TagConfig msg data -> Html msg
view size (TagConfig ( { data, toString }, tag, { icon, extraAttrs } )) =
    let
        tagAttrs =
            class "fr-tag"

        sizeAttrs =
            case size of
                SM ->
                    class "fr-tag--sm"

                MD ->
                    empty

        iconAttrs =
            case icon of
                NoIcon ->
                    empty

                LeftIcon iconName ->
                    class <| "fr-tag--icon-left " ++ DSFR.Icons.toClassName iconName

                RightIcon iconName ->
                    class <| "fr-tag--icon-right " ++ DSFR.Icons.toClassName iconName
    in
    case tag of
        Unclickable ->
            p
                (tagAttrs
                    :: sizeAttrs
                    :: iconAttrs
                    :: class "!mb-0"
                    :: extraAttrs
                )
                [ text <| toString data ]

        Clickable (Href "") ->
            Html.map never <|
                button
                    (Html.Attributes.style "cursor" "default"
                        :: tagAttrs
                        :: sizeAttrs
                        :: iconAttrs
                        :: extraAttrs
                    )
                    [ text <| toString data ]

        Clickable (Href link) ->
            a
                (href link
                    :: tagAttrs
                    :: sizeAttrs
                    :: iconAttrs
                    :: extraAttrs
                )
                [ text <| toString data ]

        Selectable (Checked checked) msg ->
            button
                ((onClick <| msg data)
                    :: type_ "button"
                    :: (pressed <| Just checked)
                    :: tagAttrs
                    :: sizeAttrs
                    :: List.map Html.Attributes.Extra.static extraAttrs
                )
                [ text <| toString data ]

        Deletable msg ->
            button
                ((onClick <| msg data)
                    :: type_ "button"
                    :: tagAttrs
                    :: sizeAttrs
                    :: class "fr-tag--dismiss"
                    :: Accessibility.Aria.label ("Retirer " ++ toString data)
                    :: List.map Html.Attributes.Extra.static extraAttrs
                )
                [ text <| toString data ]


small : List (TagConfig msg data) -> Html msg
small =
    group SM


medium : List (TagConfig msg data) -> Html msg
medium =
    group MD


group : Size -> List (TagConfig msg data) -> Html msg
group size configs =
    groupWrapper <|
        List.map (view size) <|
            configs


groupWrapper : List (Html msg) -> Html msg
groupWrapper list =
    ul [ class "fr-tags-group" ] <|
        List.map (List.singleton >> li [ class "!pb-0" ]) <|
            list


withAttrs : List (Attribute Never) -> TagConfig msg data -> TagConfig msg data
withAttrs extraAttrs (TagConfig ( mandatory, tag, optional )) =
    TagConfig ( mandatory, tag, { optional | extraAttrs = extraAttrs } )


withIcon : DSFR.Icons.IconName -> TagConfig msg data -> TagConfig msg data
withIcon iconName (TagConfig ( mandatory, tag, optional )) =
    TagConfig ( mandatory, tag, { optional | icon = LeftIcon iconName } )
